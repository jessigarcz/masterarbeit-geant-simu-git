#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os, sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress
import matplotlib.cm as cm

def fitten(funk, x, y):
	params, covar = curve_fit(funk, x, y)
	uparams = unumpy.uarray(params, np.sqrt(np.diag(covar)))
	return params, covar, uparams

def efunkt(x, a, b, c):
	return a*np.exp(-b*x) + c 

# Messdaten
x, y, z, count = np.loadtxt('simu-build/oraya_spek.out', delimiter=',', unpack=True)

energie = []
for i in range(0, len(count)):
	energie = energie + [i+0.5]

#print(len(energie))
#print(len(count))

plt.plot(energie, count, 'rx')
plt.savefig('spektrum_plot.pdf')
