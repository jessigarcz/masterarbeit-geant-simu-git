#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4SystemOfUnits.hh"

#include "G4SubtractionSolid.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"


DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0)
{ }

DetectorConstruction::~DetectorConstruction()
{ }

G4VPhysicalVolume* DetectorConstruction::Construct()
{  
  G4bool checkOverlaps = true;  
  G4NistManager* nist = G4NistManager::Instance();

  // Materialien
  G4double a,z,density,fracMass;
  G4String name,symbol;
  G4int nel,ncomp;

  G4Element* elH = new G4Element(name="Wasserstoff", symbol="H", z=1., a=1.0079*g/mole);
  G4Element* elB = new G4Element(name="Bor", symbol="B", z=5., a=10.811*g/mole);
  G4Element* elC = new G4Element(name="Kohlenstoff", symbol="C", z=6., a=12.011*g/mole);
  G4Element* elN = new G4Element(name="Stickstoff", symbol="N", z=7., a=14.007*g/mole);
  G4Element* elO = new G4Element(name="Sauerstoff", symbol="O", z=8., a=15.999*g/mole);
  G4Element* elF = new G4Element(name="Fluor", symbol="F", z=9., a=18.988*g/mole);
  G4Element* elNa = new G4Element(name="Natrium", symbol="Na", z=11., a=22.990*g/mole);
  G4Element* elMg = new G4Element(name="Magnesium", symbol="Mg", z=12., a=24.305*g/mole);
  G4Element* elAl = new G4Element(name="Aluminium", symbol="Al", z=13., a=26.982*g/mole);
  G4Element* elSi = new G4Element(name="Silicium", symbol="Si", z=14., a=28.086*g/mole);
  G4Element* elP = new G4Element(name="Phosphor", symbol="P", z=15., a=30.974*g/mole);
  G4Element* elS = new G4Element(name="Schwefel", symbol="S", z=16., a=32.065*g/mole);
  G4Element* elCl = new G4Element(name="Chlor", symbol="Cl", z=17., a=35.453*g/mole);
  G4Element* elCa = new G4Element(name="Calcium", symbol="Ca", z=20., a=40.078*g/mole);
  G4Element* elTi = new G4Element(name="Titan", symbol="Ti", z=22., a=47.867*g/mole);
  G4Element* elBr = new G4Element(name="Brom", symbol="Br", z=35., a=79.904*g/mole);
  
  G4Material* Polystyrene = new G4Material(name="Polystyrene", density=1.060*g/cm3, nel=2);
  Polystyrene->AddElement(elC, fracMass=92.26*perCent);
  Polystyrene->AddElement(elH, fracMass=7.74*perCent);

  G4Material* RW3 = new G4Material ("RW3", density=1.050*g/cm3, ncomp=4);
  RW3->AddElement(elH, fracMass=7.59*perCent);
  RW3->AddElement(elC, fracMass=90.41*perCent); 
  RW3->AddElement(elO, fracMass=0.80*perCent); 
  RW3->AddElement(elTi, fracMass=1.20*perCent); 

  G4Material* A150 = new G4Material ("A150", density=1.127*g/cm3, ncomp=6);
  A150->AddElement(elH, fracMass=10.13*perCent);
  A150->AddElement(elC, fracMass=77.55*perCent); 
  A150->AddElement(elN, fracMass=3.51*perCent); 
  A150->AddElement(elO, fracMass=5.23*perCent);
  A150->AddElement(elF, fracMass=1.74*perCent); 
  A150->AddElement(elCa, fracMass=1.84*perCent); 

  G4Material* PMMA = new G4Material ("PMMA", density=1.190*g/cm3, ncomp=3);
  PMMA->AddElement(elH, fracMass=8.06*perCent);
  PMMA->AddElement(elC, fracMass=59.98*perCent); 
  PMMA->AddElement(elO, fracMass=31.96*perCent); 
  
  G4Material* RMI457 = new G4Material ("RMI457", density=1.030*g/cm3, ncomp=6);
  RMI457->AddElement(elH, fracMass=8.09*perCent);
  RMI457->AddElement(elC, fracMass=67.22*perCent); 
  RMI457->AddElement(elN, fracMass=2.4*perCent);  
  RMI457->AddElement(elO, fracMass=19.84*perCent);
  RMI457->AddElement(elCl, fracMass=0.13*perCent); 
  RMI457->AddElement(elCa, fracMass=2.32*perCent);  

  G4Material* VW = new G4Material ("VW", density=1.030*g/cm3, ncomp=6);
  VW->AddElement(elH, fracMass=7.70*perCent);
  VW->AddElement(elC, fracMass=68.74*perCent); 
  VW->AddElement(elN, fracMass=2.27*perCent);  
  VW->AddElement(elO, fracMass=18.86*perCent);
  VW->AddElement(elCl, fracMass=0.13*perCent); 
  VW->AddElement(elCa, fracMass=2.30*perCent);

  G4Material* PRESAGE = new G4Material ("PRESAGE", density=1.101*g/cm3, ncomp=6);
  PRESAGE->AddElement(elH, fracMass=8.92*perCent);
  PRESAGE->AddElement(elC, fracMass=60.74*perCent); 
  PRESAGE->AddElement(elN, fracMass=4.46*perCent);  
  PRESAGE->AddElement(elO, fracMass=21.72*perCent);
  PRESAGE->AddElement(elCl, fracMass=3.34*perCent); 
  PRESAGE->AddElement(elBr, fracMass=0.82*perCent);

  G4Material* PWDT = new G4Material ("PWDT", density=1.039*g/cm3, ncomp=8);
  PWDT->AddElement(elH, fracMass=7.40*perCent);
  PWDT->AddElement(elB, fracMass=2.26*perCent); 
  PWDT->AddElement(elC, fracMass=46.74*perCent);  
  PWDT->AddElement(elN, fracMass=1.56*perCent);
  PWDT->AddElement(elO, fracMass=33.50*perCent); 
  PWDT->AddElement(elMg, fracMass=6.88*perCent);
  PWDT->AddElement(elAl, fracMass=1.40*perCent);
  PWDT->AddElement(elCl, fracMass=0.24*perCent);

  G4Material* PAGAT = new G4Material ("PAGAT", density=1.026*g/cm3, ncomp=6);
  PAGAT->AddElement(elH, fracMass=10.59*perCent);
  PAGAT->AddElement(elC, fracMass=6.81*perCent); 
  PAGAT->AddElement(elN, fracMass=2.42*perCent);  
  PAGAT->AddElement(elO, fracMass=80.14*perCent);
  PAGAT->AddElement(elP, fracMass=0.02*perCent); 
  PAGAT->AddElement(elCl, fracMass=0.02*perCent);

  G4Material* PW = new G4Material ("PW", density=1.013*g/cm3, ncomp=7);
  PW->AddElement(elH, fracMass=9.25*perCent);
  PW->AddElement(elC, fracMass=62.87*perCent); 
  PW->AddElement(elN, fracMass=1.00*perCent);  
  PW->AddElement(elO, fracMass=17.94*perCent);
  PW->AddElement(elCl, fracMass=0.96*perCent); 
  PW->AddElement(elCa, fracMass=7.95*perCent);
  PW->AddElement(elBr, fracMass=0.03*perCent);

  G4Material* PE = new G4Material ("PE", density=0.930*g/cm3, ncomp=2);
  PE->AddElement(elH, fracMass=14.37*perCent);
  PE->AddElement(elC, fracMass=85.63*perCent); 

  G4Material* BlueW = new G4Material ("BlueW", density=1.090*g/cm3, ncomp=5);
  BlueW->AddElement(elH, fracMass=7.45*perCent);
  BlueW->AddElement(elC, fracMass=86.42*perCent); 
  BlueW->AddElement(elN, fracMass=0.42*perCent);  
  BlueW->AddElement(elO, fracMass=2.86*perCent);
  BlueW->AddElement(elTi, fracMass=2.85*perCent); 

  G4Material* SW = new G4Material ("SW", density=1.043*g/cm3, ncomp=6);
  SW->AddElement(elH, fracMass=8.10*perCent);
  SW->AddElement(elC, fracMass=67.20*perCent); 
  SW->AddElement(elN, fracMass=2.40*perCent);  
  SW->AddElement(elO, fracMass=19.90*perCent);
  SW->AddElement(elCl, fracMass=0.10*perCent); 
  SW->AddElement(elCa, fracMass=2.30*perCent);

  G4Material* HESW = new G4Material ("HESW", density=1.032*g/cm3, ncomp=12);
  HESW->AddElement(elH, fracMass=8.13*perCent);
  HESW->AddElement(elB, fracMass=0.05*perCent); 
  HESW->AddElement(elC, fracMass=65.80*perCent);
  HESW->AddElement(elN, fracMass=2.21*perCent);  
  HESW->AddElement(elO, fracMass=19.37*perCent);
  HESW->AddElement(elNa, fracMass=0.20*perCent);
  HESW->AddElement(elMg, fracMass=1.11*perCent); 
  HESW->AddElement(elAl, fracMass=0.03*perCent);
  HESW->AddElement(elSi, fracMass=1.14*perCent);  
  HESW->AddElement(elS, fracMass=0.03*perCent);  
  HESW->AddElement(elCl, fracMass=0.14*perCent);  
  HESW->AddElement(elCa, fracMass=1.78*perCent);  

  G4Material* PWLR = new G4Material ("PWLR", density=1.029*g/cm3, ncomp=6);
  PWLR->AddElement(elH, fracMass=7.91*perCent);
  PWLR->AddElement(elC, fracMass=53.62*perCent);  
  PWLR->AddElement(elN, fracMass=1.74*perCent);
  PWLR->AddElement(elO, fracMass=27.21*perCent); 
  PWLR->AddElement(elMg, fracMass=9.29*perCent);
  PWLR->AddElement(elCl, fracMass=0.23*perCent);

  G4Material* RW1 = new G4Material ("RW1", density=0.970*g/cm3, ncomp=5);
  RW1->AddElement(elH, fracMass=13.19*perCent);
  RW1->AddElement(elC, fracMass=79.41*perCent); 
  RW1->AddElement(elO, fracMass=3.81*perCent); 
  RW1->AddElement(elMg, fracMass=0.91*perCent); 
  RW1->AddElement(elCa, fracMass=2.68*perCent); 

  // Vacuum
  //G4double atomicNumber = 1, massOfMole = 1.008*g/mole, dens = 1.e-25*g/cm3,
  //  temperature = 2.73*kelvin, pressure = 3.e-18*pascal;
  //G4Material* vac_mat = new G4Material("interGalactic", atomicNumber, massOfMole, dens, kStateGas, temperature, pressure); //G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");

// 1)“at machine exit” This was a cylindrical f4 tally that filled the air within the first (upstream) tungsten collimator (bright green) – from 36 mm to 38.5 mm
// 2)“after filtration and collimation” This was a spherical f4 tally with 1-mm radius placed in the air between the second tungsten collimator (bright green) and the phantom – the center of the sphere at 75 mm
// 3)“at fovea” This was a spherical f4 tally with 1-mm radius placed in the water phantom (red) at 21.8-mm depth (location of fovea, clinical target volume – the center of the sphere at 150 mm

  density = 2.700*g/cm3;
  a = 26.98*g/mole;
  G4Material* al_mat = new G4Material(name="Aluminum", z=13., a, density);

  G4Material* kol_mat = nist->FindOrBuildMaterial("G4_W"); 
  //G4Material* al_mat = nist->FindOrBuildMaterial("G4_ALUMINUM_OXIDE"); 
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR"); 
  //G4Material* glass_mat = nist->FindOrBuildMaterial("RW3");   

  // Colour
  G4VisAttributes* Col1 = new G4VisAttributes(G4Colour(0.1,0.1,1,0.5));
  G4VisAttributes* Col2 = new G4VisAttributes(G4Colour(1,0.1,1,0.5));
  G4VisAttributes* Col3 = new G4VisAttributes(G4Colour(0.1,1,1,0.5));
  G4VisAttributes* Col4 = new G4VisAttributes(G4Colour(0.5,0.5,0.5,0.5));

  // Brass
  G4Element* zincNist = G4NistManager::Instance()->FindOrBuildElement("Zn");
  G4Element* copperNist = G4NistManager::Instance()->FindOrBuildElement("Cu");
  G4double d = 8.40*g/cm3, nComponents = 2;
  G4double fractionmass;
  G4Material* brass_mat = new G4Material("Brass", d, nComponents);  
  brass_mat->AddElement(zincNist, fractionmass = 30 *perCent);
  brass_mat->AddElement(copperNist, fractionmass = 70 *perCent);

  G4double world_sizeXYZ = 100*cm;

  // OBJEKTE
  // World 
  G4Box* solidWorld =    
    new G4Box("World", 0.5*world_sizeXYZ, 0.5*world_sizeXYZ, 0.5*world_sizeXYZ);
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld, world_mat, "World");                                  
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);  

  G4ThreeVector nullpunkt = G4ThreeVector(0, 0, 0);
  G4ThreeVector mittelpunkt = G4ThreeVector(0, 0, 36*mm);

  // Kollimator 1
  G4double kol1_h = 2.5*mm, kol1_d = 2.5*mm;
  G4double platte = 10*cm;
  G4ThreeVector kol1_pos = G4ThreeVector(0, 0, 37.25*mm);

  G4VSolid* solidKol1Platte =   
    new G4Box("Kol1P", 0.5*platte, 0.5*platte, 0.5*kol1_h);  
  G4VSolid* solidKol1Kreis =
    new G4Tubs("Kol1K", 0*mm, kol1_d/2, kol1_h, 0*deg, 360*deg);

  G4SubtractionSolid* solidKol1 = 
    new G4SubtractionSolid("Kol1", solidKol1Platte, solidKol1Kreis, 0, nullpunkt);
  G4LogicalVolume* logicKol1 =                        
    new G4LogicalVolume(solidKol1, kol_mat, "Kol1"); 
    new G4PVPlacement(0, kol1_pos, logicKol1, "Kol1", logicWorld, false, 0, checkOverlaps);   
    logicKol1->SetVisAttributes(Col1);

  // Filter
  G4double fil_al_h = 0.75*mm;
  G4ThreeVector al_pos = G4ThreeVector(0, 0, 38.875*mm);

  G4Box* solidFilAl =    
    new G4Box("Al", 0.5*platte, 0.5*platte, 0.5*fil_al_h);
  G4LogicalVolume* logicFilAl =                         
    new G4LogicalVolume(solidFilAl, al_mat, "Al");
    new G4PVPlacement(0, al_pos, logicFilAl, "Al", logicWorld, false, 0, checkOverlaps);
    logicFilAl->SetVisAttributes(Col2);

  // Glass

  // Kollimator 2
  G4double kol2_h = 2.5*mm, kol2_d = 1*mm;
  G4ThreeVector kol2_pos = G4ThreeVector(0, 0, ((72.5-70)/2+70)*mm);

  G4VSolid* solidKol2Platte =   
    new G4Box("Kol2P", 0.5*platte, 0.5*platte, 0.5*kol2_h);  
  G4VSolid* solidKol2Kreis =
    new G4Tubs("Kol2K", 0*mm, 0.5*kol2_d, 0.5*kol2_h, 0*deg, 360*deg);

  G4SubtractionSolid* solidKol2 = 
    new G4SubtractionSolid("Kol2", solidKol2Platte, solidKol2Kreis, 0, nullpunkt);
  G4LogicalVolume* logicKol2 =                        
    new G4LogicalVolume(solidKol2, kol_mat, "Kol2"); 
    new G4PVPlacement(0, kol2_pos, logicKol2, "Kol2", logicWorld, false, 0, checkOverlaps);
    logicKol2->SetVisAttributes(Col3);

  // Brass Housing
  G4double brass1_d = 10*mm, brass2_d = 9*mm;
  G4double brass_h = (70-39.25)*mm;
  G4ThreeVector brass_pos = G4ThreeVector(0, 0, ((brass_h)/2+39.25)*mm);
 
  G4VSolid* solidBrass =   
    new G4Tubs("Brass1", 0.5*brass2_d, 0.5*brass1_d, 0.5*brass_h, 0*deg, 360*deg);
  G4LogicalVolume* logicBrass =                        
    new G4LogicalVolume(solidBrass, brass_mat, "Brass1"); 
    new G4PVPlacement(0, brass_pos, logicBrass, "Brass1", logicWorld, false, 0, checkOverlaps);
    logicBrass->SetVisAttributes(Col4);


  G4Material* phantom_mat = nist->FindOrBuildMaterial("SW");
  G4Material* phantom_mat_auge = nist->FindOrBuildMaterial("SW");

  // Auge
  G4double eye_rmin = 0, eye_rmax = 12*mm, eye_kuerzer = 5*mm, 
  eye_sphi = 0*degree, eye_dphi = 180*degree, // eye_r_real = eye_rmax - eye_kuerzer,
  eye_stheta = 0*degree, eye_dtheta = 360*degree;
  
// Halbkugel als Auge
  G4Sphere* solidPhantomEye_lang =     
    new G4Sphere("Phantom3", eye_rmin, eye_rmax, eye_sphi, eye_dphi, eye_stheta, eye_dtheta);
  G4Tubs* solidEyeAbschneiden =
    new G4Tubs("EyeAbschneiden", 0*mm, 20*mm, 0.5*eye_kuerzer, eye_stheta, eye_dtheta);
   
  G4RotationMatrix* rot_augabschneiden = new G4RotationMatrix(); 
    rot_augabschneiden->rotateX(90*deg);
  G4ThreeVector eye_abschneid = G4ThreeVector(0, 0.5*eye_kuerzer, 0);
  G4SubtractionSolid* solidPhantomEye = 
    new G4SubtractionSolid("Auge", solidPhantomEye_lang, solidEyeAbschneiden, rot_augabschneiden, eye_abschneid);

  G4LogicalVolume* logicPhantomEye =                        
    new G4LogicalVolume(solidPhantomEye, phantom_mat_auge, "Auge");    
  G4RotationMatrix* rotation_auge = new G4RotationMatrix();
  rotation_auge->rotateX(90*deg); 
  G4ThreeVector eye_pos = G4ThreeVector(0, 0, eye_rmax-7*mm+128.2*mm); 
    new G4PVPlacement(rotation_auge, eye_pos, logicPhantomEye, "Auge", logicWorld, false, 0, checkOverlaps);
    logicPhantomEye->SetVisAttributes(Col1);

  G4double wasser_xyz = 80*mm;
  G4Box* solidBecken =    
    new G4Box("Becken", 0.5*wasser_xyz, 0.5*wasser_xyz, 0.5*wasser_xyz);
  G4LogicalVolume* logicBecken =                         
    new G4LogicalVolume(solidBecken, phantom_mat, "Becken");
  G4ThreeVector becken_pos = G4ThreeVector(0, 0, 128.2*mm+0.5*wasser_xyz); 
    new G4PVPlacement(0, becken_pos, logicBecken, "Becken", logicWorld, false, 0, checkOverlaps);
    logicBecken->SetVisAttributes(Col2);

  fScoringVolume = logicWorld; 
  return physWorld;
}
