#include "EventAction.hh"
#include "RunAction.hh"

#include "G4Event.hh"
#include "G4RunManager.hh"

EventAction::EventAction(RunAction* runAction)
: G4UserEventAction(),
  fRunAction(runAction),
  fEdep(0.)
{} 

EventAction::~EventAction()
{}

void EventAction::BeginOfEventAction(const G4Event*)
{    
  fEdep = 0.;
}

void EventAction::EndOfEventAction(const G4Event*) //evt)
{   
  fRunAction->AddEdep(fEdep);
  //G4HCofThisEvent* HCE = evt->GetHCofThisEvent();
  //if(!HCE) return;
  //G4SDManager* SDMan = G4SDManager::GetSDMpointer();
  //G4int colID = SDMan->GetCollectionID("boxMesh_1/trackLengthGamma");
  //G4THitsMap<G4double>* evtMap = (G4THitsMap<G4double>*)(HCE->GetHC(colID));
  //std::map<G4int,G4double*>::iterator itr = evtMap->GetMap()->begin();
  //for(; itr != evtMap->GetMap()->end(); itr++)
  //{
  //  sum[itr->first] += *(itr->second);
  //  sum2[itr->first] += (*(itr->second))*(*(itr->second));
  //} 
}

