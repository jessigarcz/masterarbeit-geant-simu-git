import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
import matplotlib.cm as cm
import re
import sys

# Function definieren
run_n = 50
pfad = '9_tdk_nach_taddei_solid_water'
name = 'taddei_bild_fit_24_c_gerade_solid'
mac = '.mac'

for run in range(1,run_n+1):

	# sh-Datei
	dok_neu_name = 'sh/'+str(name)+'_'+str(run)+'.sh'
	dok_neu = open(dok_neu_name, 'w')

	dok_neu.write('#!/bin/sh -f \n\n')
	dok_neu.write('module add geant/10.2.1 \n\n')
	dok_neu.write('workingdir=/home/jgarczarczyk/'+str(pfad)+'/'+str(name)+'-build \n\n')
	dok_neu.write('cd  $workingdir || exit 1 \n')
	dok_neu.write('echo "Current working directory: "`pwd` \n')
	dok_neu.write('./oraya run'+str(run)+'.mac')
	

print('Fertig.')

