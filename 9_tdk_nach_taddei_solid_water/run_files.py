import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
import matplotlib.cm as cm
import re
import sys

# Function definieren
run_n = 50
name = 'run'
mac = '.mac'

for run in range(1,run_n+1):
	dok_neu_name = 'run/'+str(name)+str(run)+str(mac)
	dok_neu = open(dok_neu_name, 'w')

	dok_neu.write('/random/setSeeds '+str(2*run-1)+' '+str(2*run)+'\n')
	dok_neu.write('/run/initialize \n/control/verbose 0 \n/run/verbose 0 \n/event/verbose 0 \n/tracking/verbose 0 \n\n')

	dok_neu.write('/score/create/boxMesh box_quer_x \n/score/mesh/boxSize 0.5 0.5 3 mm \n/score/mesh/nBin 1 1 24 \n/score/mesh/translate/xyz 0 0 -22.8 mm \n/score/mesh/rotate/rotateY 90 deg \n/score/quantity/energyDeposit eDep \n/score/quantity/doseDeposit dDep \n/score/quantity/nOfTrack numb01 2 false \n/score/filter/particleWithKineticEnergy kin 0 100 keV gamma \n/score/close \n\n')
	dok_neu.write('/score/create/boxMesh box_quer_y \n/score/mesh/boxSize 0.5 0.5 3 mm \n/score/mesh/nBin 1 1 24 \n/score/mesh/translate/xyz 0 0 -22.8 mm \n/score/mesh/rotate/rotateX 90 deg \n/score/quantity/energyDeposit eDep \n/score/quantity/doseDeposit dDep \n/score/quantity/nOfTrack numb01 2 false \n/score/filter/particleWithKineticEnergy	kin 0 100 keV gamma \n/score/close \n\n')
	dok_neu.write('/score/create/cylinderMesh cyl_tdk \n/score/mesh/cylinderSize 1.25 15 mm \n/score/mesh/nBin 1 15 1 \n/score/mesh/translate/xyz 0 0 -16 mm \n/score/quantity/energyDeposit eDep \n/score/quantity/doseDeposit dDep \n/score/quantity/nOfTrack numb01 2 false \n/score/filter/particleWithKineticEnergy	kin 0 100 keV gamma \n/score/close \n\n')
	dok_neu.write('/run/beamOn 100000000 \n/score/dumpAllQuantitiesToFile cyl_tdk cyl_tdk_nach_taddei_solidwater_'+str(2*run-1)+'-'+str(2*run)+'.out \n/score/dumpAllQuantitiesToFile box_quer_x oraya_box_quer_x_'+str(2*run-1)+'-'+str(2*run)+'.out \n/score/dumpAllQuantitiesToFile box_quer_y oraya_box_quer_y_'+str(2*run-1)+'-'+str(2*run)+'.out')

print('Fertig.')

