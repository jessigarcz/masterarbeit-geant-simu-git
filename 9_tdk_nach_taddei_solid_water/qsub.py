import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
import matplotlib.cm as cm
import re
import sys

# Function definieren
run_n = 50
pfad = '9_tdk_nach_taddei_solid_water'
name = 'taddei_bild_fit_24_c_gerade_solid'
mac = '.mac'

for run in range(1,run_n+1):
	print('qsub -l walltime=23:00:00,vmem=1000mb '+str(pfad)+'/sh/'+str(name)+'_'+str(run)+'.sh &&')

print('Fertig.')

