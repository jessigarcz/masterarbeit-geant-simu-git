#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

//new
#include "G4IonTable.hh"
#include "G4ChargedGeantino.hh"
#include "G4RandomDirection.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0) 
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="gamma");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(0.*keV);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

    G4double myRand = G4UniformRand()*20.800;

  //G4cout
  //   << myRand
  //   << G4endl;
    G4double myKinErg = 0.0;
    if      ( (  myRand >=  0.000    ) && ( myRand <    0.050    ) ) {     myKinErg =   4.0 ;}
    else if ( (  myRand >=  0.050    ) && ( myRand <    0.150    ) ) {     myKinErg =   5.0 ;}
    else if ( (  myRand >=  0.150    ) && ( myRand <    0.350    ) ) {     myKinErg =   6.0 ;}
    else if ( (  myRand >=  0.350    ) && ( myRand <    0.650    ) ) {     myKinErg =   6.5 ;}
    else if ( (  myRand >=  0.650    ) && ( myRand <    1.050    ) ) {     myKinErg =   7.5 ;}
    else if ( (  myRand >=  1.050    ) && ( myRand <    1.550    ) ) {     myKinErg =   7.9 ;}
    else if ( (  myRand >=  1.550    ) && ( myRand <    2.150    ) ) {     myKinErg =   8.0 ;}
    else if ( (  myRand >=  2.150    ) && ( myRand <    2.850    ) ) {     myKinErg =   8.1 ;}
    else if ( (  myRand >=  2.850    ) && ( myRand <    3.650    ) ) {     myKinErg =   8.2 ;}
    else if ( (  myRand >=  3.650    ) && ( myRand <    4.550    ) ) {     myKinErg =   8.7 ;}
    else if ( (  myRand >=  4.550    ) && ( myRand <    6.150    ) ) {     myKinErg =   9.1 ;}
    else if ( (  myRand >=  6.150    ) && ( myRand <    7.250    ) ) {     myKinErg =   9.3 ;}
    else if ( (  myRand >=  7.250    ) && ( myRand <    8.250    ) ) {     myKinErg =   9.5 ;}
    else if ( (  myRand >=  8.250    ) && ( myRand <    8.950    ) ) {     myKinErg =   10.0    ;}
    else if ( (  myRand >=  8.950    ) && ( myRand <    9.550    ) ) {     myKinErg =   10.7    ;}
    else if ( (  myRand >=  9.550    ) && ( myRand <    10.050   ) ) {     myKinErg =   10.9    ;}
    else if ( (  myRand >=  10.050   ) && ( myRand <    10.450   ) ) {     myKinErg =   11.0    ;}
    else if ( (  myRand >=  10.450   ) && ( myRand <    10.750   ) ) {     myKinErg =   11.7    ;}
    else if ( (  myRand >=  10.750   ) && ( myRand <    11.025   ) ) {     myKinErg =   12.0    ;}
    else if ( (  myRand >=  11.025   ) && ( myRand <    11.325   ) ) {     myKinErg =   14.0    ;}
    else if ( (  myRand >=  11.325   ) && ( myRand <    11.700   ) ) {     myKinErg =   16.0    ;}
    else if ( (  myRand >=  11.700   ) && ( myRand <    12.200   ) ) {     myKinErg =   20.0    ;}
    else if ( (  myRand >=  12.200   ) && ( myRand <    12.750   ) ) {     myKinErg =   24.0    ;}
    else if ( (  myRand >=  12.750   ) && ( myRand <    13.310   ) ) {     myKinErg =   26.0    ;}
    else if ( (  myRand >=  13.310   ) && ( myRand <    13.860   ) ) {     myKinErg =   28.0    ;}
    else if ( (  myRand >=  13.860   ) && ( myRand <    14.380   ) ) {     myKinErg =   32.0    ;}
    else if ( (  myRand >=  14.380   ) && ( myRand <    14.880   ) ) {     myKinErg =   34.0    ;}
    else if ( (  myRand >=  14.880   ) && ( myRand <    15.360   ) ) {     myKinErg =   36.0    ;}
    else if ( (  myRand >=  15.360   ) && ( myRand <    15.780   ) ) {     myKinErg =   40.0    ;}
    else if ( (  myRand >=  15.780   ) && ( myRand <    16.160   ) ) {     myKinErg =   44.0    ;}
    else if ( (  myRand >=  16.160   ) && ( myRand <    16.500   ) ) {     myKinErg =   48.0    ;}
    else if ( (  myRand >=  16.500   ) && ( myRand <    16.810   ) ) {     myKinErg =   50.0    ;}
    else if ( (  myRand >=  16.810   ) && ( myRand <    17.090   ) ) {     myKinErg =   54.0    ;}
    else if ( (  myRand >=  17.090   ) && ( myRand <    17.350   ) ) {     myKinErg =   56.0    ;}
    else if ( (  myRand >=  17.350   ) && ( myRand <    17.950   ) ) {     myKinErg =   57.0    ;}
    else if ( (  myRand >=  17.950   ) && ( myRand <    18.225   ) ) {     myKinErg =   58.0    ;}
    else if ( (  myRand >=  18.225   ) && ( myRand <    19.075   ) ) {     myKinErg =   59.0    ;}
    else if ( (  myRand >=  19.075   ) && ( myRand <    19.300   ) ) {     myKinErg =   61.0    ;}
    else if ( (  myRand >=  19.300   ) && ( myRand <    19.500   ) ) {     myKinErg =   64.0    ;}
    else if ( (  myRand >=  19.500   ) && ( myRand <    19.680   ) ) {     myKinErg =   67.0    ;}
    else if ( (  myRand >=  19.680   ) && ( myRand <    20.055   ) ) {     myKinErg =   68.0    ;}
    else if ( (  myRand >=  20.055   ) && ( myRand <    20.215   ) ) {     myKinErg =   69.0    ;}
    else if ( (  myRand >=  20.215   ) && ( myRand <    20.415   ) ) {     myKinErg =   70.0    ;}
    else if ( (  myRand >=  20.415   ) && ( myRand <    20.540   ) ) {     myKinErg =   72.0    ;}
    else if ( (  myRand >=  20.540   ) && ( myRand <    20.650   ) ) {     myKinErg =   76.0    ;}
    else if ( (  myRand >=  20.650   ) && ( myRand <    20.750   ) ) {     myKinErg =   80.0    ;}
    else if ( (  myRand >=  20.750   ) && ( myRand <    20.800   ) ) {     myKinErg =   88.0    ;}
    else if ( myRand > 20.800) { myKinErg = 0.0; G4cout << "ERROR" << G4endl;}

    // Quelle

    myRand = G4UniformRand()*20;
    fParticleGun->SetParticleEnergy((myRand+80)*keV);


    //fParticleGun->SetParticleEnergy(myKinErg*keV);
    G4double x0 = 0*mm, y0 = 0*mm;
    G4double z0 = 0*mm; 

    fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));        
    // Winkel avon 30 Grad 
    G4ThreeVector direction = (G4ThreeVector(std::tan(((G4UniformRand()*2-1))*deg), std::tan(((G4UniformRand()*2-1))*deg), 1));     
    fParticleGun->SetParticleMomentumDirection(direction);    
    fParticleGun->GeneratePrimaryVertex(anEvent);
}




