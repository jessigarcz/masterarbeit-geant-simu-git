#include "DetectorConstruction.hh"

#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Cons.hh"
#include "G4Tubs.hh"
#include "G4Orb.hh"
#include "G4Sphere.hh"
#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4RotationMatrix.hh"
#include "G4SystemOfUnits.hh"

#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "globals.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

DetectorConstruction::DetectorConstruction()
: G4VUserDetectorConstruction(),
  fScoringVolume(0)
{ }

DetectorConstruction::~DetectorConstruction()
{ }




G4VPhysicalVolume* DetectorConstruction::Construct()
{  
  G4bool checkOverlaps = true;  
  G4NistManager* nist = G4NistManager::Instance();

  // Materialien
  G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR"); 
  // Vacuum
  G4double atomicNumber = 1, massOfMole = 1.008*g/mole, density = 1.e-25*g/cm3,
    temperature = 2.73*kelvin, pressure = 3.e-18*pascal;
  G4Material* vac_mat = new G4Material("interGalactic", atomicNumber, massOfMole, 
    density, kStateGas, temperature, pressure); //G4Material* world_mat = nist->FindOrBuildMaterial("G4_AIR");
  G4Material* kolli_mat = nist->FindOrBuildMaterial("G4_W"); 
  G4Material* al_mat = nist->FindOrBuildMaterial("G4_Al"); 
  G4Material* be_mat = nist->FindOrBuildMaterial("G4_Be"); 
  G4Material* phantom_mat = nist->FindOrBuildMaterial("G4_WATER"); 
  
            
  // Größen und Positionen und Drehungen
  // Mittelpunkt ist hier die Teilchenquelle
  G4ThreeVector mittelpunkt = G4ThreeVector(0, 0, 0);
  // Vacuum
  G4double vacuum_xyz = 2*70*mm; 
  // Filter/Kollimatorgrößen
  G4double filkol_xy = vacuum_xyz, filter_z_be = 0.8*mm, filter_z_al = 0.75*mm, 
  kolli_z = 2.5*mm, kolli_d = 1.9334*mm;
  // Weltgroesse
  G4double world_sizeXYZ = vacuum_xyz*10;
  // Filter/Kolliposition
  G4ThreeVector filter_be_pos = G4ThreeVector(0, 0, (18*mm+filter_z_be/2)), 
  filter_al_pos = G4ThreeVector(0, 0, (38.5*mm+filter_z_al/2)),
  kolli_pos = G4ThreeVector(0, 0, (70*mm+kolli_z/2));
  // Phantom
  G4double phantom_quader_xyz = 50*mm, phantom_eye_rmax = 12*mm;
  G4ThreeVector phantom_quader_pos = G4ThreeVector(0, 0, (150*mm-22.6*mm+phantom_eye_rmax+phantom_quader_xyz/2)),
  phantom_eye_pos = G4ThreeVector(0, 0, (150*mm-22.6*mm+phantom_eye_rmax)); 
  // Scoring an der Fovea
  G4double scor_r = 2*mm, scor_h = 1*mm;
  G4ThreeVector scor_pos = G4ThreeVector(0, 0, -phantom_quader_xyz/2-phantom_eye_rmax+22.6*mm); // Mittelpunkt ist die BOX!
  // Rotation
  G4RotationMatrix* rotation = new G4RotationMatrix();
  rotation->rotateX(90*deg);

 
  // OBJEKTE
  // World 
  G4Box* solidWorld =    
    new G4Box("World", 0.5*world_sizeXYZ, 0.5*world_sizeXYZ, 0.5*world_sizeXYZ);
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld, world_mat, "World");                                  
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0, G4ThreeVector(), logicWorld, "World", 0, false, 0, checkOverlaps);  

  // Vacuum 
  G4Box* solidVacuum =    
    new G4Box("Vacuum", 0.5*vacuum_xyz, 0.5*vacuum_xyz, 0.5*vacuum_xyz);
  G4LogicalVolume* logicVacuum =                         
    new G4LogicalVolume(solidVacuum, vac_mat, "Vacuum");                                  
  //G4VPhysicalVolume* physVacuum = 
    new G4PVPlacement(0, G4ThreeVector(), logicVacuum, "Vacuum", logicWorld, false, 0, checkOverlaps);  


  // Be Filter
  G4Box* solidBE =    
    new G4Box("BE", 0.5*filkol_xy, 0.5*filkol_xy, 0.5*filter_z_be);
  G4LogicalVolume* logicBE =                         
    new G4LogicalVolume(solidBE, be_mat, "BE");
  //G4VPhysicalVolume* physBE =                    
    new G4PVPlacement(0, filter_be_pos, logicBE, "BE", logicVacuum, false, 0, checkOverlaps);

  // Al Filter
  G4Box* solidAL =    
    new G4Box("AL", 0.5*filkol_xy, 0.5*filkol_xy, 0.5*filter_z_be);
  G4LogicalVolume* logicAL =                         
    new G4LogicalVolume(solidAL, al_mat, "AL");
  //G4VPhysicalVolume* physAL =                    
    new G4PVPlacement(0, filter_al_pos, logicAL, "AL", logicVacuum, false, 0, checkOverlaps);

  // Kollimator
  G4VSolid* solidKolliPlatte =   
    new G4Box("KolliPlatte", 0.5*filkol_xy, 0.5*filkol_xy, 0.5*kolli_z);  
  G4VSolid* solidKolliKreis =
    new G4Tubs("KolliKreis", 0*mm, kolli_d/2, kolli_z, 0*deg, 360*deg);

  G4SubtractionSolid* solidKolli = 
    new G4SubtractionSolid("Kolli", solidKolliPlatte, solidKolliKreis, 0, mittelpunkt);
  G4LogicalVolume* logicKolli =                        
    new G4LogicalVolume(solidKolli, kolli_mat, "Kolli");
  // G4VPhysicalVolume* physKolli =   
    new G4PVPlacement(0, kolli_pos, logicKolli, "Kolli", logicWorld, false, 0, checkOverlaps);

  // Wasserphantom 
  // Quadratisches Phantom              
  G4Box* solidPhantomQuader =    
    new G4Box("Phantom1", 0.5*phantom_quader_xyz, 0.5*phantom_quader_xyz, 0.5*phantom_quader_xyz);
  G4LogicalVolume* logicPhantomQuader =                         
    new G4LogicalVolume(solidPhantomQuader, phantom_mat, "Phantom1");
  //G4VPhysicalVolume* physPhantomQuader =                    
  new G4PVPlacement(0, phantom_quader_pos, logicPhantomQuader, "Phantom1", logicWorld, false, 0, checkOverlaps);
 // Halbkugel 24 mm als Auge
  G4Sphere* solidPhantomEye =     
    new G4Sphere("Phantom2", 0*mm, phantom_eye_rmax, 0*degree, 180*degree, 0*degree, 360*degree);
  G4LogicalVolume* logicPhantomEye =                        
    new G4LogicalVolume(solidPhantomEye, phantom_mat, "Phantom2");
  // G4VPhysicalVolume* physPhantomEye =   
    new G4PVPlacement(rotation, phantom_eye_pos, logicPhantomEye, "Phantom2", logicWorld, false, 0, checkOverlaps);

  // Scoringvolumen
  G4Tubs* solidScoring =    
    new G4Tubs("Scoring", 0, scor_r, 0.5*scor_h, 0*deg, 360*deg);
  G4LogicalVolume* logicScoring =                         
    new G4LogicalVolume(solidScoring, phantom_mat, "Scoring");
  //G4VPhysicalVolume* physPhantomQuader =                    
  new G4PVPlacement(0, scor_pos, logicScoring, "Scoring", logicPhantomQuader, false, 0, checkOverlaps);
 

  // Farben und Transparenz
  G4VisAttributes* vis_vac = new G4VisAttributes(G4Colour(1,1,1,0.1));
  logicVacuum->SetVisAttributes(vis_vac);
  G4VisAttributes* vis_filter = new G4VisAttributes(G4Colour(0.1,0.1,1,0.3));
  logicBE->SetVisAttributes(vis_filter);
  logicAL->SetVisAttributes(vis_filter);

  G4VisAttributes* phantom_farbe = new G4VisAttributes(G4Colour(0.1,0.1,1,0.99));
  logicPhantomQuader->SetVisAttributes(phantom_farbe);
  logicPhantomEye->SetVisAttributes(phantom_farbe);

  fScoringVolume = logicWorld; 
  return physWorld;
}
