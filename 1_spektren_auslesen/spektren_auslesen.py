import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
import scipy.interpolate as sp
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress
from PIL import Image
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.signal import medfilt
from abtastfunktionen import *

### Alle Werte (x/y) werden bei werte_ausgeben_mit(_f) auf (2/3) Nachkommestellen gerundet
### Die geladenen Daten werden normiert

### Farben der Graphen ### 
blue = [0, 0, 255]
red = [255, 0, 0]
green = [0, 255, 0]


### Graphen ohne Filterung ###
x_oraya_ohne, x_oraya_ohne_f, y_oraya_ohne, y_oraya_ohne_f = graph_aus_bild("daten/graph_oraya.png", red, 0, 100, 0, 1)
x_oraya_s_ohne, x_oraya_s_ohne_f, y_oraya_s_ohne, y_oraya_s_ohne_f = graph_aus_bild("daten/graph_oraya_slide.png", blue, 0, 100, 0, 1)
x_dirk_ohne, y_dirk_ohne = np.loadtxt('daten/spektrum_dirk_ohne.out', unpack=True)
x_calc_ohne, y_calc_ohne = np.loadtxt('daten/spekcalc_ohne_fil.out', unpack=True)
x_taddei_ohne, x_taddei_ohne_f, y_taddei_ohne, y_taddei_ohne_f = graph_aus_bild("daten/graph_taddei.png", blue, 0, 100, 0, 1)
x_taddei_daten_ohne, y_taddei_daten_ohne = np.loadtxt("daten/spektrum_taddei_daten_ohne.out", unpack=True)
x_taddei_daten_ohne_se, y_taddei_daten_ohne_se = np.loadtxt("daten/spektrum_taddei_daten_ohne_se.out", unpack=True)

max_dirk = max(y_dirk_ohne)
y_dirk_ohne = y_dirk_ohne/max_dirk
max_calc = max(y_calc_ohne)
y_calc_ohne = y_calc_ohne/max_calc
max_taddei_daten = max(y_taddei_daten_ohne)
y_taddei_daten_ohne = y_taddei_daten_ohne/max_taddei_daten
max_taddei_daten_se = max(y_taddei_daten_ohne_se)
y_taddei_daten_ohne_se = y_taddei_daten_ohne_se/max_taddei_daten_se

### Graphen mit Filterung ###
x_oraya_mit, x_oraya_mit_f, y_oraya_mit, y_oraya_mit_f = graph_aus_bild("daten/graph_oraya.png", blue, 0, 100, 0, 1)
x_oraya_s_mit, x_oraya_s_mit_f, y_oraya_s_mit, y_oraya_s_mit_f = graph_aus_bild("daten/graph_oraya_slide.png", red, 0, 100, 0, 1)
x_dirk_mit, y_dirk_mit = np.loadtxt('daten/spektrum_dirk_mit_geant.out', unpack=True)
x_calc_mit, y_calc_mit = np.loadtxt('daten/spekcalc_mit_fil.out', unpack=True)
x_taddei_mit, x_taddei_mit_f, y_taddei_mit, y_taddei_mit_f = graph_aus_bild("daten/graph_taddei.png", red, 0, 100, 0, 1)
x_taddei_daten_mit, y_taddei_daten_mit = np.loadtxt("daten/spektrum_taddei_daten_mit.out", unpack=True)
x_taddei_daten_mit_se, y_taddei_daten_mit_se = np.loadtxt("daten/spektrum_taddei_daten_mit_se.out", unpack=True)

y_dirk_mit = y_dirk_mit/max_dirk
y_calc_mit = y_calc_mit/max_calc
y_taddei_daten_mit = y_taddei_daten_mit/max_taddei_daten
y_taddei_daten_mit_se = y_taddei_daten_mit_se/max_taddei_daten_se

### Graphen Macula ####
x_oraya_mac, x_oraya_mac_f, y_oraya_mac, y_oraya_mac_f = graph_aus_bild("daten/graph_oraya.png", green, 0, 100, 0, 1)
x_oraya_s_mac, x_oraya_s_mac_f, y_oraya_s_mac, y_oraya_s_mac_f = graph_aus_bild("daten/graph_oraya_slide.png", green, 0, 100, 0, 1)
x_taddei_mac, x_taddei_mac_f, y_taddei_mac, y_taddei_mac_f = graph_aus_bild("daten/graph_taddei.png", green, 0, 100, 0, 1)
x_taddei_daten_mac, y_taddei_daten_mac = np.loadtxt("daten/spektrum_taddei_daten_mac.out", unpack=True)
x_taddei_daten_mac_se, y_taddei_daten_mac_se = np.loadtxt("daten/spektrum_taddei_daten_mac_se.out", unpack=True)

y_taddei_daten_mac = y_taddei_daten_mac/max_taddei_daten
y_taddei_daten_mac_se = y_taddei_daten_mac_se/max_taddei_daten_se


### Originalwerte ausgeben ###
werte_ausgeben_mit_f("auslesedaten/original/oraya_ohne_f.txt", x_oraya_ohne, x_oraya_ohne_f, y_oraya_ohne, y_oraya_ohne_f)
werte_ausgeben_mit_f("auslesedaten/original/oraya_s_ohne_f.txt", x_oraya_s_ohne, x_oraya_s_ohne_f, y_oraya_s_ohne, y_oraya_s_ohne_f)
werte_ausgeben("auslesedaten/original/dirk_ohne.txt", x_dirk_ohne, y_dirk_ohne)
werte_ausgeben("auslesedaten/original/calc_ohne.txt", x_calc_ohne, y_calc_ohne)
werte_ausgeben_mit_f("auslesedaten/original/taddei_ohne_f.txt", x_taddei_ohne, x_taddei_ohne_f, y_taddei_ohne, y_taddei_ohne_f)
werte_ausgeben("auslesedaten/original/taddei_ohne_daten.txt", x_taddei_daten_ohne, y_taddei_daten_ohne)
werte_ausgeben("auslesedaten/original/taddei_ohne_daten_se.txt", x_taddei_daten_ohne_se, y_taddei_daten_ohne_se)

werte_ausgeben_mit_f("auslesedaten/original/oraya_mit_f.txt", x_oraya_mit, x_oraya_mit_f, y_oraya_mit, y_oraya_mit_f)
werte_ausgeben_mit_f("auslesedaten/original/oraya_s_mit_f.txt", x_oraya_s_mit, x_oraya_s_mit_f, y_oraya_s_mit, y_oraya_s_mit_f)
werte_ausgeben("auslesedaten/original/dirk_mit_geant.txt", x_dirk_mit, y_dirk_mit)
werte_ausgeben("auslesedaten/original/calc_mit.txt", x_calc_mit, y_calc_mit)
werte_ausgeben_mit_f("auslesedaten/original/taddei_mit_f.txt", x_taddei_mit, x_taddei_mit_f, y_taddei_mit, y_taddei_mit_f)
werte_ausgeben("auslesedaten/original/taddei_mit_daten.txt", x_taddei_daten_mit, y_taddei_daten_mit)
werte_ausgeben("auslesedaten/original/taddei_mit_daten_se.txt", x_taddei_daten_mit_se, y_taddei_daten_mit_se)

werte_ausgeben_mit_f("auslesedaten/original/oraya_macula_f.txt", x_oraya_mac, x_oraya_mac_f, y_oraya_mac, y_oraya_mac_f)
werte_ausgeben_mit_f("auslesedaten/original/oraya_s_macula_f.txt", x_oraya_s_mac, x_oraya_s_mac_f, y_oraya_s_mac, y_oraya_s_mac_f)
werte_ausgeben_mit_f("auslesedaten/original/taddei_macula_f.txt", x_taddei_mac, x_taddei_mac_f, y_taddei_mac, y_taddei_mac_f)
werte_ausgeben("auslesedaten/original/taddei_mac_daten.txt", x_taddei_daten_mac, y_taddei_daten_mac)
werte_ausgeben("auslesedaten/original/taddei_mac_daten_se.txt", x_taddei_daten_mac_se, y_taddei_daten_mac_se)


###### Werte neu normieren #########
y_oraya_ohne_n = norm_array(y_oraya_ohne, max_array_zwischen(x_oraya_ohne, y_oraya_ohne, 15, 30))
y_oraya_ohne_f_n = norm_array(y_oraya_ohne_f, max_array_zwischen(x_oraya_ohne, y_oraya_ohne, 15, 30))
y_oraya_s_ohne_n = norm_array(y_oraya_s_ohne, max_array_zwischen(x_oraya_s_ohne, y_oraya_s_ohne, 15, 30))
y_oraya_s_ohne_f_n = norm_array(y_oraya_s_ohne_f, max_array_zwischen(x_oraya_s_ohne, y_oraya_s_ohne, 15, 30))
y_dirk_ohne_n = norm_array(y_dirk_ohne, max_array_zwischen(x_dirk_ohne, y_dirk_ohne, 15, 30))
y_calc_ohne_n = norm_array(y_calc_ohne, max_array_zwischen(x_calc_ohne, y_calc_ohne, 15, 30))
y_taddei_ohne_n = norm_array(y_taddei_ohne, max_array_zwischen(x_taddei_ohne, y_taddei_ohne, 15, 30))
y_taddei_ohne_f_n = norm_array(y_taddei_ohne_f, max_array_zwischen(x_taddei_ohne, y_taddei_ohne, 15, 30))
y_taddei_daten_ohne_n = norm_array(y_taddei_daten_ohne, max_array_zwischen(x_taddei_daten_ohne, y_taddei_daten_ohne, 15, 30))
y_taddei_daten_ohne_se_n = norm_array(y_taddei_daten_ohne_se, max_array_zwischen(x_taddei_daten_ohne_se, y_taddei_daten_ohne_se, 15, 30))

y_oraya_mit_n = norm_array(y_oraya_mit, max_array_zwischen(x_oraya_mit, y_oraya_mit, 20, 45))
y_oraya_mit_f_n = norm_array(y_oraya_mit_f, max_array_zwischen(x_oraya_mit, y_oraya_mit, 20, 45))
y_oraya_s_mit_n = norm_array(y_oraya_s_mit, max_array_zwischen(x_oraya_s_mit, y_oraya_s_mit, 20, 45))
y_oraya_s_mit_f_n = norm_array(y_oraya_s_mit_f, max_array_zwischen(x_oraya_s_mit, y_oraya_s_mit, 20, 45))
y_dirk_mit_n = norm_array(y_dirk_mit, max_array_zwischen(x_dirk_mit, y_dirk_mit, 20, 45))
y_calc_mit_n = norm_array(y_calc_mit, max_array_zwischen(x_calc_mit, y_calc_mit, 20, 45))
y_taddei_mit_n = norm_array(y_taddei_mit, max_array_zwischen(x_taddei_mit, y_taddei_mit, 20, 45))
y_taddei_mit_f_n = norm_array(y_taddei_mit_f, max_array_zwischen(x_taddei_mit, y_taddei_mit, 20, 45))
y_taddei_daten_mit_n = norm_array(y_taddei_daten_mit, max_array_zwischen(x_taddei_daten_mit, y_taddei_daten_mit, 20, 45))
y_taddei_daten_mit_se_n = norm_array(y_taddei_daten_mit_se, max_array_zwischen(x_taddei_daten_mit_se, y_taddei_daten_mit_se, 20, 45))

y_oraya_mac_n = norm_array(y_oraya_mac, max_array_zwischen(x_oraya_mac, y_oraya_mac, 20, 45))
y_oraya_mac_f_n = norm_array(y_oraya_mac_f, max_array_zwischen(x_oraya_mac, y_oraya_mac, 20, 45))
y_oraya_s_mac_n = norm_array(y_oraya_s_mac, max_array_zwischen(x_oraya_s_mac, y_oraya_s_mac, 20, 45))
y_oraya_s_mac_f_n = norm_array(y_oraya_s_mac_f, max_array_zwischen(x_oraya_s_mac, y_oraya_s_mac, 20, 45))
y_taddei_mac_n = norm_array(y_taddei_mac, max_array_zwischen(x_taddei_mac, y_taddei_mac, 20, 45))
y_taddei_mac_f_n = norm_array(y_taddei_mac_f, max_array_zwischen(x_taddei_mac, y_taddei_mac, 20, 45))
y_taddei_daten_mac_n = norm_array(y_taddei_daten_mac, max_array_zwischen(x_taddei_daten_mac, y_taddei_daten_mac, 20, 45))
y_taddei_daten_mac_se_n = norm_array(y_taddei_daten_mac_se, max_array_zwischen(x_taddei_daten_mac_se, y_taddei_daten_mac_se, 20, 45))



### Normierte Werte ausgeben ###
werte_ausgeben_mit_f("auslesedaten/brems/oraya_ohne_f.txt", x_oraya_ohne, x_oraya_ohne_f, y_oraya_ohne_n, y_oraya_ohne_f_n)
werte_ausgeben_mit_f("auslesedaten/brems/oraya_s_ohne_f.txt", x_oraya_s_ohne, x_oraya_s_ohne_f, y_oraya_s_ohne_n, y_oraya_s_ohne_f_n)
werte_ausgeben("auslesedaten/brems/dirk_ohne.txt", x_dirk_ohne, y_dirk_ohne_n)
werte_ausgeben("auslesedaten/brems/calc_ohne.txt", x_calc_ohne, y_calc_ohne_n)
werte_ausgeben_mit_f("auslesedaten/brems/taddei_ohne_f.txt", x_taddei_ohne, x_taddei_ohne_f, y_taddei_ohne_n, y_taddei_ohne_f_n)
werte_ausgeben("auslesedaten/brems/taddei_ohne_daten.txt", x_taddei_daten_ohne, y_taddei_daten_ohne_n)
werte_ausgeben("auslesedaten/brems/taddei_ohne_daten_se.txt", x_taddei_daten_ohne_se, y_taddei_daten_ohne_se_n)

werte_ausgeben_mit_f("auslesedaten/brems/oraya_mit_f.txt", x_oraya_mit, x_oraya_mit_f, y_oraya_mit_n, y_oraya_mit_f_n)
werte_ausgeben_mit_f("auslesedaten/brems/oraya_s_mit_f.txt", x_oraya_s_mit, x_oraya_s_mit_f, y_oraya_s_mit_n, y_oraya_s_mit_f_n)
werte_ausgeben("auslesedaten/brems/dirk_mit_geant.txt", x_dirk_mit, y_dirk_mit_n)
werte_ausgeben("auslesedaten/brems/calc_mit.txt", x_calc_mit, y_calc_mit_n)
werte_ausgeben_mit_f("auslesedaten/brems/taddei_mit_f.txt", x_taddei_mit, x_taddei_mit_f, y_taddei_mit_n, y_taddei_mit_f_n)
werte_ausgeben("auslesedaten/brems/taddei_mit_daten.txt", x_taddei_daten_mit, y_taddei_daten_mit_n)
werte_ausgeben("auslesedaten/brems/taddei_mit_daten_se.txt", x_taddei_daten_mit_se, y_taddei_daten_mit_se_n)

werte_ausgeben_mit_f("auslesedaten/brems/oraya_mac_f.txt", x_oraya_mac, x_oraya_mac_f, y_oraya_mac_n, y_oraya_mac_f_n)
werte_ausgeben_mit_f("auslesedaten/brems/oraya_mac_s_f.txt", x_oraya_s_mac, x_oraya_s_mac_f, y_oraya_s_mac_n, y_oraya_s_mac_f_n)
werte_ausgeben_mit_f("auslesedaten/brems/taddei_mac_f.txt", x_taddei_mac, x_taddei_mac_f, y_taddei_mac_n, y_taddei_mac_f_n)
werte_ausgeben("auslesedaten/brems/taddei_mac_daten.txt", x_taddei_daten_mac, y_taddei_daten_mac_n)
werte_ausgeben("auslesedaten/brems/taddei_mac_daten_se.txt", x_taddei_daten_mac_se, y_taddei_daten_mac_se_n)