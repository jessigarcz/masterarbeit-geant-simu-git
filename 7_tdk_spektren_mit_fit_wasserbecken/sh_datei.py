import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
import matplotlib.cm as cm
import re
import sys

# Function definieren
dateien_n = 4
name = 'spektrum_tdk_'
dateien = np.array(['dirk', 'oraya', 'taddei_bild', 'taddei_data'])  
run_n = 6

# cmake und make: befinde dich in einem -ohnebuild ordner 
for dat in range(0,dateien_n):
	#for run in range(1,run_n+1):
	print('cd ../ && mkdir '+str(name)+str(dateien[dat])+'-build && cd '+str(name)+str(dateien[dat])+' && cmake -DGeant4_DIR=/sl6/sw/geant4/10.02/lib64/Geant4-10.2.1 /home/jgarczarczyk/simu_spektrum_tdk_mit_fit/'+str(name)+str(dateien[dat])+' && make -j4 &&')

for dat in range(0,dateien_n):
	for run in range(1,run_n+1):
		
		# qsub für Phido
		dok_neu_name = 'sh/'+str(name)+str(dateien[dat])+'_'+str(run)+'.sh'
		dok_neu = open(dok_neu_name, 'w')

		dok_neu.write('#!/bin/sh -f')
		dok_neu.write('\n')
		dok_neu.write('\n')

		dok_neu.write('module add geant/10.2.1')
		dok_neu.write('\n')
		dok_neu.write('\n')

		dok_neu.write('workingdir=/home/jgarczarczyk/simu_spektrum_tdk_mit_fit/'+str(name)+str(dateien[dat])+'-build')
		dok_neu.write('\n')
		dok_neu.write('\n')

		dok_neu.write('cd  $workingdir || exit 1')
		dok_neu.write('\n')
		dok_neu.write('echo "Current working directory: "`pwd`')
		dok_neu.write('\n')

		dok_neu.write('./oraya run'+str(run)+'.mac')
		#if(run == 1):
		#	print('cd .. && cd '+str(name)+str(dateien[dat])+'-build && ./oraya run5.mac &&')
			

		print('qsub -l walltime=23:59:99,vmem=1000mb '+str(name)+str(dateien[dat])+'_'+str(run)+'.sh &&')
		print()


print('Fertig.')

