import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from scipy.stats import linregress
import matplotlib.cm as cm
import re
import sys
import os.path

# Function definieren
def get_lines(filename):
    f = open(filename, 'r')
    lines = f.readlines()
    anz = len(lines)
    array = []
    for i in range(0,anz):
    	array = array + [lines[i]]
    f.close
    return array

# Dokumente
simulation = 'spektrum_tdk_dirk'
runs = 6
n_1 = 500000000
datei_ohne = 'oraya_cyl_tdk_'
pfad = 'phido/'+str(simulation)
pfad_build = 'phido/'+str(simulation)+'-build/'
lines_text_1 = 3
lines_text_2 = 2

# Dateien laden
lines = []
for j in range(0,runs):
	line = get_lines(str(pfad_build)+str(datei_ohne)+str(n_1)+'_'+str(j+1)+'.out')
	lines.append(line)

#print(lines[1][2])

# Check ob Dokumente gleiche Box verwenden an Anzahl der Elemente
for j in range(1,runs):
	if len(lines[0]) == len(lines[j]):
		print('Datei in Ordnung.')
	else:
		sys.exit('Scorer fehlerhaft')

# Neues Dokument erstellen
n_ges = runs * n_1

### 1. Hälfte in Gy
dok_neu_name_ohne_pfad = str(datei_ohne)+str(n_ges)+'_dosis.out'
#print(dok_neu_name)
dok_neu_name = os.path.join(pfad, dok_neu_name_ohne_pfad)
dok_neu = open(dok_neu_name, 'w')
print(dok_neu_name)
#print(round(len(lines[0])/2))
for i in range(0, round(len(lines[0])/2)):
	if i < lines_text_1:
		for j in range(1,runs):
			if lines[0][i] == lines[j][i]:
				#print('yes')
				if j == runs-1:
					dok_neu.write(lines[0][i])
					#print('yes')
			else:
				sys.exit('Scorer fehlerhaft')
	else:
		z = []
		phi = []
		r = []
		val = []
		for j in range(0,runs):
			line = lines[j][i].replace(',', ' ')
			line = line.split()
			z = z + [line[0]]
			phi = phi + [line[1]]
			r = r + [line[2]]
			val = val + [float(line[3])]
		value = val[0]
		for j in range(1,runs):
			if z[0] == z[j]:
				if j == runs-1:
					dok_neu.write(z[0]+',')
			else:
				sys.exit('Scorer fehlerhaft')

			if phi[0] == phi[j]:
				if j == runs-1:
					dok_neu.write(phi[0]+',')
			else:
				sys.exit('Scorer fehlerhaft')

			if r[0] == r[j]:
				if j == runs-1:
					dok_neu.write(r[0]+',')
			else:
				sys.exit('Scorer fehlerhaft')

			value = value + val[j]
			if j == runs-1:
				dok_neu.write(str(value))
				dok_neu.write('\n')

dok_neu.close()
print('Fertig zusammengefasst.')