#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os, sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress
import matplotlib.cm as cm
from /Users/jgarczarczyk/geant4-simu/plot_farben import *

# Messdaten
#ordner = 'spektrum_tdk_'
#oraya = ['taddei', 'oraya', 'oraya_slide', 'dirk', 'calc']
#legende = ['Taddei et al', 'Oraya', 'Oraya Slides', 'Pitaske', 'SpekCalc']
#n_ges = '3000000000'
#datei = 'oraya_cyl_tdk_'+str(n_ges)+'_dosis.out'
#anzahl_dat = len(oraya)

# Interesannte Angaben
#zarr = []
#valarr = []

x, y, z, val = np.loadtxt('../5_tdk_spektren_ohne_fit_wasserbecken/spektrum_tdk_taddei_daten-build/oraya_cyl_tdk_10000000_1.out', delimiter=',', unpack=True)

#for i in range(0, anzahl_dat):
#	x, y, z, val = np.loadtxt(str(ordner)+str(oraya[i])+'/'+str(datei), delimiter=',', unpack=True)
#	zarr = zarr + [z]
#	valarr = valarr + [val]

#bins = len(zarr[0]) 


# Plotgroesse
#fig_size = plt.rcParams["figure.figsize"]
#print(fig_size)
#fig_size[0] = 8*0.8
#fig_size[1] = 6*0.8
#plt.rcParams["figure.figsize"] = fig_size

# PLOTS
#col = ['k', 'b', 'c', 'g', 'm', 'r']
#strich = ['-', '--', ':', '-.', '.']

fig = plt.figure()
ax1 = fig.add_subplot(3,1,(1,2))
ax1.grid()

ax1.plot(z, val, 'g-', linewidth=2, label='TDK')


#for i in range(0, anzahl_dat):
#	ax1.plot(zarr[i], valarr[i]/valarr[i][0]*100, str(col[i])+str(strich[i]), linewidth=2, label=str(legende[i]))
#	#semilogy

plt.ylabel('prozentuale Dosis zur Oberfläche /  %')
legend = plt.legend(markerscale = 1.5, fancybox=True)
plt.setp(legend.get_title(),fontsize='12')
plt.setp(plt.gca().get_legend().get_texts(), fontsize='12')
legend.get_frame().set_edgecolor('gray')

ax2 = fig.add_subplot(3,1,3)
ax2.grid()

#for i in range(0, anzahl_dat):
ax2.plot(z, val, 'x-', label='Abweichung')

plt.ylabel('proz. Abw.  /  %')
plt.xlabel('Tiefe des Wasserphantoms  /  mm')
plt.savefig("plot_tdk_neu.pdf")





