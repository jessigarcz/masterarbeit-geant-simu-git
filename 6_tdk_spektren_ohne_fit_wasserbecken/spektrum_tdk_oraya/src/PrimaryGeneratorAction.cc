#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

//new
#include "G4IonTable.hh"
#include "G4ChargedGeantino.hh"
#include "G4RandomDirection.hh"
#include <iostream>


PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0) 
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);
  // Textdatei erzeugen
  //std::ofstream outFile(“output.txt”);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="gamma");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(0.*keV);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  G4double norm = 66.752; 
  G4double myRand = G4UniformRand()*norm; 
  G4double myKinErg = 0.0; 
  if((myRand >= 0) && (myRand < 0.0)){ myKinErg = 0.0;} 
  else if((myRand >= 0.0) && (myRand < -0.018)){ myKinErg = 14.48;} 
  else if((myRand >= -0.018) && (myRand < 0.063)){ myKinErg = 15.43;} 
  else if((myRand >= 0.063) && (myRand < 0.189)){ myKinErg = 16.24;} 
  else if((myRand >= 0.189) && (myRand < 0.324)){ myKinErg = 16.37;} 
  else if((myRand >= 0.324) && (myRand < 0.495)){ myKinErg = 16.91;} 
  else if((myRand >= 0.495) && (myRand < 0.684)){ myKinErg = 17.32;} 
  else if((myRand >= 0.684) && (myRand < 0.909)){ myKinErg = 17.46;} 
  else if((myRand >= 0.909) && (myRand < 1.143)){ myKinErg = 17.73;} 
  else if((myRand >= 1.143) && (myRand < 1.386)){ myKinErg = 18.13;} 
  else if((myRand >= 1.386) && (myRand < 1.71)){ myKinErg = 18.67;} 
  else if((myRand >= 1.71) && (myRand < 2.043)){ myKinErg = 19.08;} 
  else if((myRand >= 2.043) && (myRand < 2.403)){ myKinErg = 19.22;} 
  else if((myRand >= 2.403) && (myRand < 2.844)){ myKinErg = 19.62;} 
  else if((myRand >= 2.844) && (myRand < 3.312)){ myKinErg = 20.3;} 
  else if((myRand >= 3.312) && (myRand < 3.826)){ myKinErg = 21.38;} 
  else if((myRand >= 3.826) && (myRand < 4.43)){ myKinErg = 22.6;} 
  else if((myRand >= 4.43) && (myRand < 5.043)){ myKinErg = 23.27;} 
  else if((myRand >= 5.043) && (myRand < 5.701)){ myKinErg = 23.68;} 
  else if((myRand >= 5.701) && (myRand < 6.422)){ myKinErg = 24.63;} 
  else if((myRand >= 6.422) && (myRand < 7.179)){ myKinErg = 25.44;} 
  else if((myRand >= 7.179) && (myRand < 7.954)){ myKinErg = 25.85;} 
  else if((myRand >= 7.954) && (myRand < 8.774)){ myKinErg = 26.66;} 
  else if((myRand >= 8.774) && (myRand < 9.657)){ myKinErg = 27.6;} 
  else if((myRand >= 9.657) && (myRand < 10.531)){ myKinErg = 28.15;} 
  else if((myRand >= 10.531) && (myRand < 11.459)){ myKinErg = 28.82;} 
  else if((myRand >= 11.459) && (myRand < 12.396)){ myKinErg = 29.23;} 
  else if((myRand >= 12.396) && (myRand < 13.378)){ myKinErg = 29.5;} 
  else if((myRand >= 13.378) && (myRand < 14.378)){ myKinErg = 30.72;} 
  else if((myRand >= 14.378) && (myRand < 15.369)){ myKinErg = 32.21;} 
  else if((myRand >= 15.369) && (myRand < 16.369)){ myKinErg = 32.88;} 
  else if((myRand >= 16.369) && (myRand < 17.351)){ myKinErg = 34.1;} 
  else if((myRand >= 17.351) && (myRand < 18.333)){ myKinErg = 35.32;} 
  else if((myRand >= 18.333) && (myRand < 19.306)){ myKinErg = 36.27;} 
  else if((myRand >= 19.306) && (myRand < 20.252)){ myKinErg = 37.21;} 
  else if((myRand >= 20.252) && (myRand < 21.225)){ myKinErg = 37.35;} 
  else if((myRand >= 21.225) && (myRand < 22.171)){ myKinErg = 38.29;} 
  else if((myRand >= 22.171) && (myRand < 23.135)){ myKinErg = 39.11;} 
  else if((myRand >= 23.135) && (myRand < 24.09)){ myKinErg = 39.24;} 
  else if((myRand >= 24.09) && (myRand < 25.036)){ myKinErg = 39.78;} 
  else if((myRand >= 25.036) && (myRand < 25.964)){ myKinErg = 40.32;} 
  else if((myRand >= 25.964) && (myRand < 26.883)){ myKinErg = 41.27;} 
  else if((myRand >= 26.883) && (myRand < 27.775)){ myKinErg = 42.08;} 
  else if((myRand >= 27.775) && (myRand < 28.676)){ myKinErg = 42.22;} 
  else if((myRand >= 28.676) && (myRand < 29.559)){ myKinErg = 42.35;} 
  else if((myRand >= 29.559) && (myRand < 30.424)){ myKinErg = 42.9;} 
  else if((myRand >= 30.424) && (myRand < 31.271)){ myKinErg = 43.57;} 
  else if((myRand >= 31.271) && (myRand < 32.127)){ myKinErg = 43.84;} 
  else if((myRand >= 32.127) && (myRand < 32.974)){ myKinErg = 44.25;} 
  else if((myRand >= 32.974) && (myRand < 33.812)){ myKinErg = 44.79;} 
  else if((myRand >= 33.812) && (myRand < 34.65)){ myKinErg = 45.2;} 
  else if((myRand >= 34.65) && (myRand < 35.479)){ myKinErg = 45.33;} 
  else if((myRand >= 35.479) && (myRand < 36.299)){ myKinErg = 45.87;} 
  else if((myRand >= 36.299) && (myRand < 37.11)){ myKinErg = 46.28;} 
  else if((myRand >= 37.11) && (myRand < 37.912)){ myKinErg = 46.68;} 
  else if((myRand >= 37.912) && (myRand < 38.714)){ myKinErg = 47.09;} 
  else if((myRand >= 38.714) && (myRand < 39.498)){ myKinErg = 47.77;} 
  else if((myRand >= 39.498) && (myRand < 40.273)){ myKinErg = 48.85;} 
  else if((myRand >= 40.273) && (myRand < 41.039)){ myKinErg = 49.39;} 
  else if((myRand >= 41.039) && (myRand < 41.787)){ myKinErg = 49.93;} 
  else if((myRand >= 41.787) && (myRand < 42.517)){ myKinErg = 50.34;} 
  else if((myRand >= 42.517) && (myRand < 43.238)){ myKinErg = 50.88;} 
  else if((myRand >= 43.238) && (myRand < 43.941)){ myKinErg = 51.42;} 
  else if((myRand >= 43.941) && (myRand < 44.626)){ myKinErg = 51.96;} 
  else if((myRand >= 44.626) && (myRand < 45.302)){ myKinErg = 52.91;} 
  else if((myRand >= 45.302) && (myRand < 45.978)){ myKinErg = 53.32;} 
  else if((myRand >= 45.978) && (myRand < 46.645)){ myKinErg = 53.99;} 
  else if((myRand >= 46.645) && (myRand < 47.294)){ myKinErg = 54.8;} 
  else if((myRand >= 47.294) && (myRand < 47.934)){ myKinErg = 55.35;} 
  else if((myRand >= 47.934) && (myRand < 48.565)){ myKinErg = 55.75;} 
  else if((myRand >= 48.565) && (myRand < 49.187)){ myKinErg = 56.16;} 
  else if((myRand >= 49.187) && (myRand < 49.773)){ myKinErg = 56.56;} 
  else if((myRand >= 49.773) && (myRand < 50.359)){ myKinErg = 56.97;} 
  else if((myRand >= 50.359) && (myRand < 50.954)){ myKinErg = 57.24;} 
  else if((myRand >= 50.954) && (myRand < 52.341)){ myKinErg = 57.78;} 
  else if((myRand >= 52.341) && (myRand < 53.737)){ myKinErg = 58.19;} 
  else if((myRand >= 53.737) && (myRand < 54.287)){ myKinErg = 58.59;} 
  else if((myRand >= 54.287) && (myRand < 54.846)){ myKinErg = 59.0;} 
  else if((myRand >= 54.846) && (myRand < 56.819)){ myKinErg = 59.54;} 
  else if((myRand >= 56.819) && (myRand < 58.801)){ myKinErg = 59.95;} 
  else if((myRand >= 58.801) && (myRand < 59.324)){ myKinErg = 60.89;} 
  else if((myRand >= 59.324) && (myRand < 59.829)){ myKinErg = 61.84;} 
  else if((myRand >= 59.829) && (myRand < 60.315)){ myKinErg = 62.92;} 
  else if((myRand >= 60.315) && (myRand < 60.774)){ myKinErg = 63.46;} 
  else if((myRand >= 60.774) && (myRand < 61.242)){ myKinErg = 64.14;} 
  else if((myRand >= 61.242) && (myRand < 61.683)){ myKinErg = 65.49;} 
  else if((myRand >= 61.683) && (myRand < 62.124)){ myKinErg = 65.9;} 
  else if((myRand >= 62.124) && (myRand < 62.547)){ myKinErg = 66.71;} 
  else if((myRand >= 62.547) && (myRand < 63.042)){ myKinErg = 67.12;} 
  else if((myRand >= 63.042) && (myRand < 63.61)){ myKinErg = 67.25;} 
  else if((myRand >= 63.61) && (myRand < 64.529)){ myKinErg = 67.79;} 
  else if((myRand >= 64.529) && (myRand < 64.916)){ myKinErg = 68.74;} 
  else if((myRand >= 64.916) && (myRand < 65.294)){ myKinErg = 69.01;} 
  else if((myRand >= 65.294) && (myRand < 65.753)){ myKinErg = 69.82;} 
  else if((myRand >= 65.753) && (myRand < 66.068)){ myKinErg = 70.77;} 
  else if((myRand >= 66.068) && (myRand < 66.374)){ myKinErg = 70.91;} 
  else if((myRand >= 66.374) && (myRand < 66.581)){ myKinErg = 79.7;} 
  else if((myRand >= 66.581) && (myRand < 66.653)){ myKinErg = 93.5;} 
  else if((myRand >= 66.653) && (myRand < 66.734)){ myKinErg = 93.91;} 
  else if((myRand >= 66.734) && (myRand < 66.752)){ myKinErg = 97.97;} 
  else if((myRand >= 66.752) && (myRand < 66.752)){ myKinErg = 100.0;} 
  else if(myRand >= 66.752){ myKinErg = 0.0;} 

  // Quelle
  fParticleGun->SetParticleEnergy(myKinErg*keV);
  
  // Quadrat, welches dann Überprüft, ob Zahl innerhalb Kreisscheibe ist
  //G4double r = 0.5, x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r);
  //while((x0*x0)+(y0*y0)>r){ x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r); }
  G4double z0 = -(75*mm-22.8*mm); 

  // Zufallszahl mit Phi und R
  G4double radius_quelle = 0.5*mm;
  G4double r_position = radius_quelle *sqrt(G4UniformRand());
  G4double phi = CLHEP::twopi*G4UniformRand();
  G4double x0 = (r_position * sin(phi))  ;
  G4double y0 = (r_position * cos(phi)) ;

  G4double b = 1.146*deg, a = 2.294, alphax = (a*x0)+b, alphay = (a*y0)+b;
  fParticleGun->SetParticlePosition(G4ThreeVector(x0*mm,y0*mm,z0*mm));   
  G4ThreeVector direction = (G4ThreeVector(std::tan(alphax*deg), std::tan(alphay*deg), 1));  
  //G4double alpx = G4UniformRand()*2*alphax-alphax, alpy = G4UniformRand()*2*alphay-alphay
  //G4ThreeVector direction = (G4ThreeVector(std::tan(alpx*deg), std::tan(alpy*deg), 1));     
  fParticleGun->SetParticleMomentumDirection(direction);    
  fParticleGun->GeneratePrimaryVertex(anEvent);
}




