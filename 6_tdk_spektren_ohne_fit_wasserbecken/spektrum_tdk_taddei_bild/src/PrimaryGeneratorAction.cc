#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

//new
#include "G4IonTable.hh"
#include "G4ChargedGeantino.hh"
#include "G4RandomDirection.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0) 
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="gamma");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(0.*keV);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  G4double norm = 90.595; 
  G4double myRand = G4UniformRand()*norm; 
  G4double myKinErg = 0.0; 
  if((myRand >= 0) && (myRand < 0.0)){ myKinErg = 0.0;} 
  else if((myRand >= 0.0) && (myRand < 0.017)){ myKinErg = 12.8;} 
  else if((myRand >= 0.017) && (myRand < 0.059)){ myKinErg = 13.7;} 
  else if((myRand >= 0.059) && (myRand < 0.16)){ myKinErg = 14.76;} 
  else if((myRand >= 0.16) && (myRand < 0.311)){ myKinErg = 15.51;} 
  else if((myRand >= 0.311) && (myRand < 0.487)){ myKinErg = 15.96;} 
  else if((myRand >= 0.487) && (myRand < 0.756)){ myKinErg = 17.17;} 
  else if((myRand >= 0.756) && (myRand < 1.168)){ myKinErg = 19.13;} 
  else if((myRand >= 1.168) && (myRand < 1.639)){ myKinErg = 19.88;} 
  else if((myRand >= 1.639) && (myRand < 2.143)){ myKinErg = 20.18;} 
  else if((myRand >= 2.143) && (myRand < 2.681)){ myKinErg = 20.48;} 
  else if((myRand >= 2.681) && (myRand < 3.244)){ myKinErg = 20.93;} 
  else if((myRand >= 3.244) && (myRand < 3.857)){ myKinErg = 21.54;} 
  else if((myRand >= 3.857) && (myRand < 4.605)){ myKinErg = 23.19;} 
  else if((myRand >= 4.605) && (myRand < 5.445)){ myKinErg = 25.0;} 
  else if((myRand >= 5.445) && (myRand < 6.336)){ myKinErg = 26.2;} 
  else if((myRand >= 6.336) && (myRand < 7.252)){ myKinErg = 27.26;} 
  else if((myRand >= 7.252) && (myRand < 8.168)){ myKinErg = 28.16;} 
  else if((myRand >= 8.168) && (myRand < 9.134)){ myKinErg = 29.52;} 
  else if((myRand >= 9.134) && (myRand < 10.126)){ myKinErg = 30.27;} 
  else if((myRand >= 10.126) && (myRand < 11.118)){ myKinErg = 30.42;} 
  else if((myRand >= 11.118) && (myRand < 12.118)){ myKinErg = 31.17;} 
  else if((myRand >= 12.118) && (myRand < 13.118)){ myKinErg = 31.78;} 
  else if((myRand >= 13.118) && (myRand < 14.101)){ myKinErg = 31.93;} 
  else if((myRand >= 14.101) && (myRand < 15.076)){ myKinErg = 32.08;} 
  else if((myRand >= 15.076) && (myRand < 16.017)){ myKinErg = 32.53;} 
  else if((myRand >= 16.017) && (myRand < 16.958)){ myKinErg = 33.58;} 
  else if((myRand >= 16.958) && (myRand < 17.891)){ myKinErg = 34.64;} 
  else if((myRand >= 17.891) && (myRand < 18.841)){ myKinErg = 35.09;} 
  else if((myRand >= 18.841) && (myRand < 19.799)){ myKinErg = 35.54;} 
  else if((myRand >= 19.799) && (myRand < 20.749)){ myKinErg = 35.84;} 
  else if((myRand >= 20.749) && (myRand < 21.69)){ myKinErg = 35.99;} 
  else if((myRand >= 21.69) && (myRand < 22.623)){ myKinErg = 36.14;} 
  else if((myRand >= 22.623) && (myRand < 23.539)){ myKinErg = 36.45;} 
  else if((myRand >= 23.539) && (myRand < 24.438)){ myKinErg = 36.9;} 
  else if((myRand >= 24.438) && (myRand < 25.32)){ myKinErg = 37.35;} 
  else if((myRand >= 25.32) && (myRand < 26.194)){ myKinErg = 38.1;} 
  else if((myRand >= 26.194) && (myRand < 27.06)){ myKinErg = 38.86;} 
  else if((myRand >= 27.06) && (myRand < 27.917)){ myKinErg = 39.01;} 
  else if((myRand >= 27.917) && (myRand < 28.749)){ myKinErg = 39.46;} 
  else if((myRand >= 28.749) && (myRand < 29.598)){ myKinErg = 40.21;} 
  else if((myRand >= 29.598) && (myRand < 30.447)){ myKinErg = 40.66;} 
  else if((myRand >= 30.447) && (myRand < 31.287)){ myKinErg = 40.96;} 
  else if((myRand >= 31.287) && (myRand < 32.119)){ myKinErg = 41.27;} 
  else if((myRand >= 32.119) && (myRand < 32.943)){ myKinErg = 41.42;} 
  else if((myRand >= 32.943) && (myRand < 33.75)){ myKinErg = 41.87;} 
  else if((myRand >= 33.75) && (myRand < 34.54)){ myKinErg = 42.32;} 
  else if((myRand >= 34.54) && (myRand < 35.33)){ myKinErg = 43.07;} 
  else if((myRand >= 35.33) && (myRand < 36.128)){ myKinErg = 43.52;} 
  else if((myRand >= 36.128) && (myRand < 36.918)){ myKinErg = 43.83;} 
  else if((myRand >= 36.918) && (myRand < 37.691)){ myKinErg = 44.13;} 
  else if((myRand >= 37.691) && (myRand < 38.447)){ myKinErg = 44.88;} 
  else if((myRand >= 38.447) && (myRand < 39.212)){ myKinErg = 45.78;} 
  else if((myRand >= 39.212) && (myRand < 39.96)){ myKinErg = 46.08;} 
  else if((myRand >= 39.96) && (myRand < 40.699)){ myKinErg = 46.39;} 
  else if((myRand >= 40.699) && (myRand < 41.422)){ myKinErg = 46.69;} 
  else if((myRand >= 41.422) && (myRand < 42.136)){ myKinErg = 46.99;} 
  else if((myRand >= 42.136) && (myRand < 42.842)){ myKinErg = 47.44;} 
  else if((myRand >= 42.842) && (myRand < 43.539)){ myKinErg = 47.59;} 
  else if((myRand >= 43.539) && (myRand < 44.22)){ myKinErg = 48.04;} 
  else if((myRand >= 44.22) && (myRand < 44.884)){ myKinErg = 48.49;} 
  else if((myRand >= 44.884) && (myRand < 45.539)){ myKinErg = 48.64;} 
  else if((myRand >= 45.539) && (myRand < 46.178)){ myKinErg = 48.95;} 
  else if((myRand >= 46.178) && (myRand < 46.791)){ myKinErg = 49.4;} 
  else if((myRand >= 46.791) && (myRand < 47.396)){ myKinErg = 50.15;} 
  else if((myRand >= 47.396) && (myRand < 48.009)){ myKinErg = 51.81;} 
  else if((myRand >= 48.009) && (myRand < 48.622)){ myKinErg = 52.86;} 
  else if((myRand >= 48.622) && (myRand < 49.244)){ myKinErg = 53.31;} 
  else if((myRand >= 49.244) && (myRand < 49.841)){ myKinErg = 53.92;} 
  else if((myRand >= 49.841) && (myRand < 50.412)){ myKinErg = 54.22;} 
  else if((myRand >= 50.412) && (myRand < 50.975)){ myKinErg = 54.37;} 
  else if((myRand >= 50.975) && (myRand < 51.521)){ myKinErg = 54.82;} 
  else if((myRand >= 51.521) && (myRand < 52.05)){ myKinErg = 55.27;} 
  else if((myRand >= 52.05) && (myRand < 52.579)){ myKinErg = 56.02;} 
  else if((myRand >= 52.579) && (myRand < 53.226)){ myKinErg = 56.63;} 
  else if((myRand >= 53.226) && (myRand < 53.949)){ myKinErg = 56.78;} 
  else if((myRand >= 53.949) && (myRand < 54.731)){ myKinErg = 56.93;} 
  else if((myRand >= 54.731) && (myRand < 55.622)){ myKinErg = 57.08;} 
  else if((myRand >= 55.622) && (myRand < 56.672)){ myKinErg = 57.38;} 
  else if((myRand >= 56.672) && (myRand < 57.638)){ myKinErg = 57.83;} 
  else if((myRand >= 57.638) && (myRand < 58.403)){ myKinErg = 57.98;} 
  else if((myRand >= 58.403) && (myRand < 58.974)){ myKinErg = 58.28;} 
  else if((myRand >= 58.974) && (myRand < 59.571)){ myKinErg = 58.43;} 
  else if((myRand >= 59.571) && (myRand < 60.344)){ myKinErg = 58.73;} 
  else if((myRand >= 60.344) && (myRand < 61.378)){ myKinErg = 58.89;} 
  else if((myRand >= 61.378) && (myRand < 62.655)){ myKinErg = 59.19;} 
  else if((myRand >= 62.655) && (myRand < 64.151)){ myKinErg = 59.49;} 
  else if((myRand >= 64.151) && (myRand < 65.622)){ myKinErg = 59.64;} 
  else if((myRand >= 65.622) && (myRand < 66.782)){ myKinErg = 59.79;} 
  else if((myRand >= 66.782) && (myRand < 67.883)){ myKinErg = 59.94;} 
  else if((myRand >= 67.883) && (myRand < 68.791)){ myKinErg = 60.09;} 
  else if((myRand >= 68.791) && (myRand < 69.606)){ myKinErg = 60.24;} 
  else if((myRand >= 69.606) && (myRand < 70.144)){ myKinErg = 60.54;} 
  else if((myRand >= 70.144) && (myRand < 70.589)){ myKinErg = 60.99;} 
  else if((myRand >= 70.589) && (myRand < 71.051)){ myKinErg = 61.6;} 
  else if((myRand >= 71.051) && (myRand < 71.496)){ myKinErg = 61.9;} 
  else if((myRand >= 71.496) && (myRand < 71.925)){ myKinErg = 62.2;} 
  else if((myRand >= 71.925) && (myRand < 72.345)){ myKinErg = 62.35;} 
  else if((myRand >= 72.345) && (myRand < 72.757)){ myKinErg = 62.65;} 
  else if((myRand >= 72.757) && (myRand < 73.152)){ myKinErg = 63.1;} 
  else if((myRand >= 73.152) && (myRand < 73.547)){ myKinErg = 63.4;} 
  else if((myRand >= 73.547) && (myRand < 73.934)){ myKinErg = 63.7;} 
  else if((myRand >= 73.934) && (myRand < 74.295)){ myKinErg = 64.16;} 
  else if((myRand >= 74.295) && (myRand < 74.648)){ myKinErg = 65.21;} 
  else if((myRand >= 74.648) && (myRand < 75.018)){ myKinErg = 66.11;} 
  else if((myRand >= 75.018) && (myRand < 75.447)){ myKinErg = 66.57;} 
  else if((myRand >= 75.447) && (myRand < 75.968)){ myKinErg = 66.87;} 
  else if((myRand >= 75.968) && (myRand < 76.556)){ myKinErg = 67.02;} 
  else if((myRand >= 76.556) && (myRand < 77.186)){ myKinErg = 67.17;} 
  else if((myRand >= 77.186) && (myRand < 77.858)){ myKinErg = 67.32;} 
  else if((myRand >= 77.858) && (myRand < 78.589)){ myKinErg = 67.47;} 
  else if((myRand >= 78.589) && (myRand < 79.278)){ myKinErg = 67.62;} 
  else if((myRand >= 79.278) && (myRand < 79.883)){ myKinErg = 67.77;} 
  else if((myRand >= 79.883) && (myRand < 80.438)){ myKinErg = 67.92;} 
  else if((myRand >= 80.438) && (myRand < 80.967)){ myKinErg = 68.07;} 
  else if((myRand >= 80.967) && (myRand < 81.404)){ myKinErg = 68.22;} 
  else if((myRand >= 81.404) && (myRand < 81.807)){ myKinErg = 68.37;} 
  else if((myRand >= 81.807) && (myRand < 82.16)){ myKinErg = 68.83;} 
  else if((myRand >= 82.16) && (myRand < 82.513)){ myKinErg = 68.98;} 
  else if((myRand >= 82.513) && (myRand < 82.891)){ myKinErg = 69.28;} 
  else if((myRand >= 82.891) && (myRand < 83.269)){ myKinErg = 69.58;} 
  else if((myRand >= 83.269) && (myRand < 83.63)){ myKinErg = 69.73;} 
  else if((myRand >= 83.63) && (myRand < 83.975)){ myKinErg = 69.88;} 
  else if((myRand >= 83.975) && (myRand < 84.294)){ myKinErg = 70.03;} 
  else if((myRand >= 84.294) && (myRand < 84.605)){ myKinErg = 70.18;} 
  else if((myRand >= 84.605) && (myRand < 84.899)){ myKinErg = 70.33;} 
  else if((myRand >= 84.899) && (myRand < 85.176)){ myKinErg = 70.48;} 
  else if((myRand >= 85.176) && (myRand < 85.445)){ myKinErg = 70.78;} 
  else if((myRand >= 85.445) && (myRand < 85.706)){ myKinErg = 71.23;} 
  else if((myRand >= 85.706) && (myRand < 85.958)){ myKinErg = 71.54;} 
  else if((myRand >= 85.958) && (myRand < 86.202)){ myKinErg = 71.84;} 
  else if((myRand >= 86.202) && (myRand < 86.437)){ myKinErg = 71.99;} 
  else if((myRand >= 86.437) && (myRand < 86.655)){ myKinErg = 72.44;} 
  else if((myRand >= 86.655) && (myRand < 86.882)){ myKinErg = 73.19;} 
  else if((myRand >= 86.882) && (myRand < 87.1)){ myKinErg = 73.95;} 
  else if((myRand >= 87.1) && (myRand < 87.31)){ myKinErg = 74.4;} 
  else if((myRand >= 87.31) && (myRand < 87.512)){ myKinErg = 75.75;} 
  else if((myRand >= 87.512) && (myRand < 87.714)){ myKinErg = 77.41;} 
  else if((myRand >= 87.714) && (myRand < 87.916)){ myKinErg = 78.01;} 
  else if((myRand >= 87.916) && (myRand < 88.109)){ myKinErg = 78.77;} 
  else if((myRand >= 88.109) && (myRand < 88.294)){ myKinErg = 79.07;} 
  else if((myRand >= 88.294) && (myRand < 88.47)){ myKinErg = 79.52;} 
  else if((myRand >= 88.47) && (myRand < 88.638)){ myKinErg = 79.82;} 
  else if((myRand >= 88.638) && (myRand < 88.798)){ myKinErg = 80.27;} 
  else if((myRand >= 88.798) && (myRand < 88.949)){ myKinErg = 81.93;} 
  else if((myRand >= 88.949) && (myRand < 89.1)){ myKinErg = 83.58;} 
  else if((myRand >= 89.1) && (myRand < 89.243)){ myKinErg = 83.73;} 
  else if((myRand >= 89.243) && (myRand < 89.377)){ myKinErg = 84.04;} 
  else if((myRand >= 89.377) && (myRand < 89.503)){ myKinErg = 84.34;} 
  else if((myRand >= 89.503) && (myRand < 89.621)){ myKinErg = 85.09;} 
  else if((myRand >= 89.621) && (myRand < 89.73)){ myKinErg = 86.14;} 
  else if((myRand >= 89.73) && (myRand < 89.839)){ myKinErg = 87.35;} 
  else if((myRand >= 89.839) && (myRand < 89.94)){ myKinErg = 88.25;} 
  else if((myRand >= 89.94) && (myRand < 90.032)){ myKinErg = 89.16;} 
  else if((myRand >= 90.032) && (myRand < 90.116)){ myKinErg = 90.21;} 
  else if((myRand >= 90.116) && (myRand < 90.183)){ myKinErg = 91.27;} 
  else if((myRand >= 90.183) && (myRand < 90.259)){ myKinErg = 92.17;} 
  else if((myRand >= 90.259) && (myRand < 90.326)){ myKinErg = 92.92;} 
  else if((myRand >= 90.326) && (myRand < 90.385)){ myKinErg = 93.37;} 
  else if((myRand >= 90.385) && (myRand < 90.435)){ myKinErg = 93.98;} 
  else if((myRand >= 90.435) && (myRand < 90.477)){ myKinErg = 94.73;} 
  else if((myRand >= 90.477) && (myRand < 90.511)){ myKinErg = 95.18;} 
  else if((myRand >= 90.511) && (myRand < 90.545)){ myKinErg = 95.93;} 
  else if((myRand >= 90.545) && (myRand < 90.57)){ myKinErg = 96.08;} 
  else if((myRand >= 90.57) && (myRand < 90.587)){ myKinErg = 97.59;} 
  else if((myRand >= 90.587) && (myRand < 90.595)){ myKinErg = 98.49;} 
  else if((myRand >= 90.595) && (myRand < 90.595)){ myKinErg = 100.0;} 
  else if(myRand >= 90.595){ myKinErg = 0.0;} 


  // Quelle
  fParticleGun->SetParticleEnergy(myKinErg*keV);
  
  // Quadrat, welches dann Überprüft, ob Zahl innerhalb Kreisscheibe ist
  //G4double r = 0.5, x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r);
  //while((x0*x0)+(y0*y0)>r){ x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r); }
  G4double z0 = -(75*mm-22.6*mm); 

  // Zufallszahl mit Phi und R
  G4double radius_quelle = 0.5*mm;
  G4double r_position = radius_quelle *sqrt(G4UniformRand());
  G4double phi = CLHEP::twopi*G4UniformRand();
  G4double x0 = (r_position * sin(phi))  ;
  G4double y0 = (r_position * cos(phi)) ;

  G4double b = 1.146*deg, a = 2.294, alphax = (a*x0)+b, alphay = (a*y0)+b;
  fParticleGun->SetParticlePosition(G4ThreeVector(x0*mm,y0*mm,z0*mm));   
  G4ThreeVector direction = (G4ThreeVector(std::tan(alphax*deg), std::tan(alphay*deg), 1));  
  //G4double alpx = G4UniformRand()*2*alphax-alphax, alpy = G4UniformRand()*2*alphay-alphay
  //G4ThreeVector direction = (G4ThreeVector(std::tan(alpx*deg), std::tan(alpy*deg), 1));     
  fParticleGun->SetParticleMomentumDirection(direction);    
  fParticleGun->GeneratePrimaryVertex(anEvent);
}




