#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

//new
#include "G4IonTable.hh"
#include "G4ChargedGeantino.hh"
#include "G4RandomDirection.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0) 
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="gamma");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(0.*keV);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  G4double norm = 43.423; 
  G4double myRand = G4UniformRand()*norm; 
  G4double myKinErg = 0.0; 
  if((myRand >= 0) && (myRand < 0.012)){ myKinErg = 10.0;} 
  else if((myRand >= 0.012) && (myRand < 0.034)){ myKinErg = 11.0;} 
  else if((myRand >= 0.034) && (myRand < 0.079)){ myKinErg = 12.0;} 
  else if((myRand >= 0.079) && (myRand < 0.161)){ myKinErg = 13.0;} 
  else if((myRand >= 0.161) && (myRand < 0.304)){ myKinErg = 14.0;} 
  else if((myRand >= 0.304) && (myRand < 0.522)){ myKinErg = 15.0;} 
  else if((myRand >= 0.522) && (myRand < 0.829)){ myKinErg = 16.0;} 
  else if((myRand >= 0.829) && (myRand < 1.23)){ myKinErg = 17.0;} 
  else if((myRand >= 1.23) && (myRand < 1.724)){ myKinErg = 18.0;} 
  else if((myRand >= 1.724) && (myRand < 2.308)){ myKinErg = 19.0;} 
  else if((myRand >= 2.308) && (myRand < 2.976)){ myKinErg = 20.0;} 
  else if((myRand >= 2.976) && (myRand < 3.72)){ myKinErg = 21.0;} 
  else if((myRand >= 3.72) && (myRand < 4.53)){ myKinErg = 22.0;} 
  else if((myRand >= 4.53) && (myRand < 5.395)){ myKinErg = 23.0;} 
  else if((myRand >= 5.395) && (myRand < 6.304)){ myKinErg = 24.0;} 
  else if((myRand >= 6.304) && (myRand < 7.246)){ myKinErg = 25.0;} 
  else if((myRand >= 7.246) && (myRand < 8.214)){ myKinErg = 26.0;} 
  else if((myRand >= 8.214) && (myRand < 9.2)){ myKinErg = 27.0;} 
  else if((myRand >= 9.2) && (myRand < 10.196)){ myKinErg = 28.0;} 
  else if((myRand >= 10.196) && (myRand < 11.196)){ myKinErg = 29.0;} 
  else if((myRand >= 11.196) && (myRand < 12.194)){ myKinErg = 30.0;} 
  else if((myRand >= 12.194) && (myRand < 13.186)){ myKinErg = 31.0;} 
  else if((myRand >= 13.186) && (myRand < 14.168)){ myKinErg = 32.0;} 
  else if((myRand >= 14.168) && (myRand < 15.137)){ myKinErg = 33.0;} 
  else if((myRand >= 15.137) && (myRand < 16.091)){ myKinErg = 34.0;} 
  else if((myRand >= 16.091) && (myRand < 17.028)){ myKinErg = 35.0;} 
  else if((myRand >= 17.028) && (myRand < 17.945)){ myKinErg = 36.0;} 
  else if((myRand >= 17.945) && (myRand < 18.842)){ myKinErg = 37.0;} 
  else if((myRand >= 18.842) && (myRand < 19.718)){ myKinErg = 38.0;} 
  else if((myRand >= 19.718) && (myRand < 20.572)){ myKinErg = 39.0;} 
  else if((myRand >= 20.572) && (myRand < 21.403)){ myKinErg = 40.0;} 
  else if((myRand >= 21.403) && (myRand < 22.212)){ myKinErg = 41.0;} 
  else if((myRand >= 22.212) && (myRand < 22.998)){ myKinErg = 42.0;} 
  else if((myRand >= 22.998) && (myRand < 23.761)){ myKinErg = 43.0;} 
  else if((myRand >= 23.761) && (myRand < 24.502)){ myKinErg = 44.0;} 
  else if((myRand >= 24.502) && (myRand < 25.221)){ myKinErg = 45.0;} 
  else if((myRand >= 25.221) && (myRand < 25.918)){ myKinErg = 46.0;} 
  else if((myRand >= 25.918) && (myRand < 26.593)){ myKinErg = 47.0;} 
  else if((myRand >= 26.593) && (myRand < 27.247)){ myKinErg = 48.0;} 
  else if((myRand >= 27.247) && (myRand < 27.88)){ myKinErg = 49.0;} 
  else if((myRand >= 27.88) && (myRand < 28.493)){ myKinErg = 50.0;} 
  else if((myRand >= 28.493) && (myRand < 29.086)){ myKinErg = 51.0;} 
  else if((myRand >= 29.086) && (myRand < 29.66)){ myKinErg = 52.0;} 
  else if((myRand >= 29.66) && (myRand < 30.215)){ myKinErg = 53.0;} 
  else if((myRand >= 30.215) && (myRand < 30.751)){ myKinErg = 54.0;} 
  else if((myRand >= 30.751) && (myRand < 31.269)){ myKinErg = 55.0;} 
  else if((myRand >= 31.269) && (myRand < 31.77)){ myKinErg = 56.0;} 
  else if((myRand >= 31.77) && (myRand < 32.254)){ myKinErg = 57.0;} 
  else if((myRand >= 32.254) && (myRand < 33.357)){ myKinErg = 58.0;} 
  else if((myRand >= 33.357) && (myRand < 34.913)){ myKinErg = 59.0;} 
  else if((myRand >= 34.913) && (myRand < 35.348)){ myKinErg = 60.0;} 
  else if((myRand >= 35.348) && (myRand < 35.768)){ myKinErg = 61.0;} 
  else if((myRand >= 35.768) && (myRand < 36.173)){ myKinErg = 62.0;} 
  else if((myRand >= 36.173) && (myRand < 36.563)){ myKinErg = 63.0;} 
  else if((myRand >= 36.563) && (myRand < 36.939)){ myKinErg = 64.0;} 
  else if((myRand >= 36.939) && (myRand < 37.302)){ myKinErg = 65.0;} 
  else if((myRand >= 37.302) && (myRand < 37.651)){ myKinErg = 66.0;} 
  else if((myRand >= 37.651) && (myRand < 38.345)){ myKinErg = 67.0;} 
  else if((myRand >= 38.345) && (myRand < 38.669)){ myKinErg = 68.0;} 
  else if((myRand >= 38.669) && (myRand < 39.073)){ myKinErg = 69.0;} 
  else if((myRand >= 39.073) && (myRand < 39.341)){ myKinErg = 70.0;} 
  else if((myRand >= 39.341) && (myRand < 39.599)){ myKinErg = 71.0;} 
  else if((myRand >= 39.599) && (myRand < 39.848)){ myKinErg = 72.0;} 
  else if((myRand >= 39.848) && (myRand < 40.088)){ myKinErg = 73.0;} 
  else if((myRand >= 40.088) && (myRand < 40.32)){ myKinErg = 74.0;} 
  else if((myRand >= 40.32) && (myRand < 40.543)){ myKinErg = 75.0;} 
  else if((myRand >= 40.543) && (myRand < 40.757)){ myKinErg = 76.0;} 
  else if((myRand >= 40.757) && (myRand < 40.963)){ myKinErg = 77.0;} 
  else if((myRand >= 40.963) && (myRand < 41.16)){ myKinErg = 78.0;} 
  else if((myRand >= 41.16) && (myRand < 41.349)){ myKinErg = 79.0;} 
  else if((myRand >= 41.349) && (myRand < 41.529)){ myKinErg = 80.0;} 
  else if((myRand >= 41.529) && (myRand < 41.701)){ myKinErg = 81.0;} 
  else if((myRand >= 41.701) && (myRand < 41.865)){ myKinErg = 82.0;} 
  else if((myRand >= 41.865) && (myRand < 42.021)){ myKinErg = 83.0;} 
  else if((myRand >= 42.021) && (myRand < 42.169)){ myKinErg = 84.0;} 
  else if((myRand >= 42.169) && (myRand < 42.309)){ myKinErg = 85.0;} 
  else if((myRand >= 42.309) && (myRand < 42.441)){ myKinErg = 86.0;} 
  else if((myRand >= 42.441) && (myRand < 42.565)){ myKinErg = 87.0;} 
  else if((myRand >= 42.565) && (myRand < 42.682)){ myKinErg = 88.0;} 
  else if((myRand >= 42.682) && (myRand < 42.791)){ myKinErg = 89.0;} 
  else if((myRand >= 42.791) && (myRand < 42.892)){ myKinErg = 90.0;} 
  else if((myRand >= 42.892) && (myRand < 42.985)){ myKinErg = 91.0;} 
  else if((myRand >= 42.985) && (myRand < 43.07)){ myKinErg = 92.0;} 
  else if((myRand >= 43.07) && (myRand < 43.147)){ myKinErg = 93.0;} 
  else if((myRand >= 43.147) && (myRand < 43.216)){ myKinErg = 94.0;} 
  else if((myRand >= 43.216) && (myRand < 43.276)){ myKinErg = 95.0;} 
  else if((myRand >= 43.276) && (myRand < 43.328)){ myKinErg = 96.0;} 
  else if((myRand >= 43.328) && (myRand < 43.371)){ myKinErg = 97.0;} 
  else if((myRand >= 43.371) && (myRand < 43.406)){ myKinErg = 98.0;} 
  else if((myRand >= 43.406) && (myRand < 43.423)){ myKinErg = 99.0;} 
  else if((myRand >= 43.423) && (myRand < 43.423)){ myKinErg = 100.0;} 
  else if(myRand >= 43.423){ myKinErg = 0.0;} 


  // Quelle
  fParticleGun->SetParticleEnergy(myKinErg*keV);
  
  // Quadrat, welches dann Überprüft, ob Zahl innerhalb Kreisscheibe ist
  //G4double r = 0.5, x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r);
  //while((x0*x0)+(y0*y0)>r){ x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r); }
  G4double z0 = -(75*mm-22.6*mm); 

  // Zufallszahl mit Phi und R
  G4double radius_quelle = 0.5*mm;
  G4double r_position = radius_quelle *sqrt(G4UniformRand());
  G4double phi = CLHEP::twopi*G4UniformRand();
  G4double x0 = (r_position * sin(phi))  ;
  G4double y0 = (r_position * cos(phi)) ;

  G4double b = 1.146*deg, a = 2.294, alphax = (a*x0)+b, alphay = (a*y0)+b;
  fParticleGun->SetParticlePosition(G4ThreeVector(x0*mm,y0*mm,z0*mm));   
  G4ThreeVector direction = (G4ThreeVector(std::tan(alphax*deg), std::tan(alphay*deg), 1));  
  //G4double alpx = G4UniformRand()*2*alphax-alphax, alpy = G4UniformRand()*2*alphay-alphay
  //G4ThreeVector direction = (G4ThreeVector(std::tan(alpx*deg), std::tan(alpy*deg), 1));     
  fParticleGun->SetParticleMomentumDirection(direction);    
  fParticleGun->GeneratePrimaryVertex(anEvent);
}




