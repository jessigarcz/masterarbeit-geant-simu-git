#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

//new
#include "G4IonTable.hh"
#include "G4ChargedGeantino.hh"
#include "G4RandomDirection.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0) 
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="gamma");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(0.*keV);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  G4double norm = 42.227; 
  G4double myRand = G4UniformRand()*norm; 
  G4double myKinErg = 0.0; 
  if((myRand >= 0) && (myRand < 0.0)){ myKinErg = 0.5;} 
  else if((myRand >= 0.0) && (myRand < 0.0)){ myKinErg = 1.5;} 
  else if((myRand >= 0.0) && (myRand < 0.0)){ myKinErg = 2.5;} 
  else if((myRand >= 0.0) && (myRand < 0.0)){ myKinErg = 3.5;} 
  else if((myRand >= 0.0) && (myRand < 0.0)){ myKinErg = 4.5;} 
  else if((myRand >= 0.0) && (myRand < 0.0)){ myKinErg = 5.5;} 
  else if((myRand >= 0.0) && (myRand < 0.0)){ myKinErg = 6.5;} 
  else if((myRand >= 0.0) && (myRand < 0.0)){ myKinErg = 7.5;} 
  else if((myRand >= 0.0) && (myRand < 0.0)){ myKinErg = 8.5;} 
  else if((myRand >= 0.0) && (myRand < 0.015)){ myKinErg = 9.5;} 
  else if((myRand >= 0.015) && (myRand < 0.019)){ myKinErg = 10.5;} 
  else if((myRand >= 0.019) && (myRand < 0.031)){ myKinErg = 11.5;} 
  else if((myRand >= 0.031) && (myRand < 0.059)){ myKinErg = 12.5;} 
  else if((myRand >= 0.059) && (myRand < 0.114)){ myKinErg = 13.5;} 
  else if((myRand >= 0.114) && (myRand < 0.215)){ myKinErg = 14.5;} 
  else if((myRand >= 0.215) && (myRand < 0.375)){ myKinErg = 15.5;} 
  else if((myRand >= 0.375) && (myRand < 0.591)){ myKinErg = 16.5;} 
  else if((myRand >= 0.591) && (myRand < 0.898)){ myKinErg = 17.5;} 
  else if((myRand >= 0.898) && (myRand < 1.277)){ myKinErg = 18.5;} 
  else if((myRand >= 1.277) && (myRand < 1.724)){ myKinErg = 19.5;} 
  else if((myRand >= 1.724) && (myRand < 2.266)){ myKinErg = 20.5;} 
  else if((myRand >= 2.266) && (myRand < 2.871)){ myKinErg = 21.5;} 
  else if((myRand >= 2.871) && (myRand < 3.58)){ myKinErg = 22.5;} 
  else if((myRand >= 3.58) && (myRand < 4.342)){ myKinErg = 23.5;} 
  else if((myRand >= 4.342) && (myRand < 5.127)){ myKinErg = 24.5;} 
  else if((myRand >= 5.127) && (myRand < 5.999)){ myKinErg = 25.5;} 
  else if((myRand >= 5.999) && (myRand < 6.894)){ myKinErg = 26.5;} 
  else if((myRand >= 6.894) && (myRand < 7.807)){ myKinErg = 27.5;} 
  else if((myRand >= 7.807) && (myRand < 8.714)){ myKinErg = 28.5;} 
  else if((myRand >= 8.714) && (myRand < 9.667)){ myKinErg = 29.5;} 
  else if((myRand >= 9.667) && (myRand < 10.655)){ myKinErg = 30.5;} 
  else if((myRand >= 10.655) && (myRand < 11.655)){ myKinErg = 31.5;} 
  else if((myRand >= 11.655) && (myRand < 12.597)){ myKinErg = 32.5;} 
  else if((myRand >= 12.597) && (myRand < 13.533)){ myKinErg = 33.5;} 
  else if((myRand >= 13.533) && (myRand < 14.457)){ myKinErg = 34.5;} 
  else if((myRand >= 14.457) && (myRand < 15.416)){ myKinErg = 35.5;} 
  else if((myRand >= 15.416) && (myRand < 16.323)){ myKinErg = 36.5;} 
  else if((myRand >= 16.323) && (myRand < 17.189)){ myKinErg = 37.5;} 
  else if((myRand >= 17.189) && (myRand < 18.061)){ myKinErg = 38.5;} 
  else if((myRand >= 18.061) && (myRand < 18.887)){ myKinErg = 39.5;} 
  else if((myRand >= 18.887) && (myRand < 19.736)){ myKinErg = 40.5;} 
  else if((myRand >= 19.736) && (myRand < 20.556)){ myKinErg = 41.5;} 
  else if((myRand >= 20.556) && (myRand < 21.341)){ myKinErg = 42.5;} 
  else if((myRand >= 21.341) && (myRand < 22.138)){ myKinErg = 43.5;} 
  else if((myRand >= 22.138) && (myRand < 22.888)){ myKinErg = 44.5;} 
  else if((myRand >= 22.888) && (myRand < 23.655)){ myKinErg = 45.5;} 
  else if((myRand >= 23.655) && (myRand < 24.388)){ myKinErg = 46.5;} 
  else if((myRand >= 24.388) && (myRand < 25.086)){ myKinErg = 47.5;} 
  else if((myRand >= 25.086) && (myRand < 25.749)){ myKinErg = 48.5;} 
  else if((myRand >= 25.749) && (myRand < 26.359)){ myKinErg = 49.5;} 
  else if((myRand >= 26.359) && (myRand < 26.975)){ myKinErg = 50.5;} 
  else if((myRand >= 26.975) && (myRand < 27.591)){ myKinErg = 51.5;} 
  else if((myRand >= 27.591) && (myRand < 28.201)){ myKinErg = 52.5;} 
  else if((myRand >= 28.201) && (myRand < 28.829)){ myKinErg = 53.5;} 
  else if((myRand >= 28.829) && (myRand < 29.387)){ myKinErg = 54.5;} 
  else if((myRand >= 29.387) && (myRand < 29.92)){ myKinErg = 55.5;} 
  else if((myRand >= 29.92) && (myRand < 30.45)){ myKinErg = 56.5;} 
  else if((myRand >= 30.45) && (myRand < 31.624)){ myKinErg = 57.5;} 
  else if((myRand >= 31.624) && (myRand < 32.073)){ myKinErg = 58.5;} 
  else if((myRand >= 32.073) && (myRand < 33.707)){ myKinErg = 59.5;} 
  else if((myRand >= 33.707) && (myRand < 34.133)){ myKinErg = 60.5;} 
  else if((myRand >= 34.133) && (myRand < 34.605)){ myKinErg = 61.5;} 
  else if((myRand >= 34.605) && (myRand < 35.021)){ myKinErg = 62.5;} 
  else if((myRand >= 35.021) && (myRand < 35.423)){ myKinErg = 63.5;} 
  else if((myRand >= 35.423) && (myRand < 35.783)){ myKinErg = 64.5;} 
  else if((myRand >= 35.783) && (myRand < 36.146)){ myKinErg = 65.5;} 
  else if((myRand >= 36.146) && (myRand < 36.537)){ myKinErg = 66.5;} 
  else if((myRand >= 36.537) && (myRand < 37.316)){ myKinErg = 67.5;} 
  else if((myRand >= 37.316) && (myRand < 37.656)){ myKinErg = 68.5;} 
  else if((myRand >= 37.656) && (myRand < 38.049)){ myKinErg = 69.5;} 
  else if((myRand >= 38.049) && (myRand < 38.335)){ myKinErg = 70.5;} 
  else if((myRand >= 38.335) && (myRand < 38.6)){ myKinErg = 71.5;} 
  else if((myRand >= 38.6) && (myRand < 38.828)){ myKinErg = 72.5;} 
  else if((myRand >= 38.828) && (myRand < 39.072)){ myKinErg = 73.5;} 
  else if((myRand >= 39.072) && (myRand < 39.289)){ myKinErg = 74.5;} 
  else if((myRand >= 39.289) && (myRand < 39.508)){ myKinErg = 75.5;} 
  else if((myRand >= 39.508) && (myRand < 39.714)){ myKinErg = 76.5;} 
  else if((myRand >= 39.714) && (myRand < 39.934)){ myKinErg = 77.5;} 
  else if((myRand >= 39.934) && (myRand < 40.146)){ myKinErg = 78.5;} 
  else if((myRand >= 40.146) && (myRand < 40.335)){ myKinErg = 79.5;} 
  else if((myRand >= 40.335) && (myRand < 40.504)){ myKinErg = 80.5;} 
  else if((myRand >= 40.504) && (myRand < 40.669)){ myKinErg = 81.5;} 
  else if((myRand >= 40.669) && (myRand < 40.832)){ myKinErg = 82.5;} 
  else if((myRand >= 40.832) && (myRand < 41.004)){ myKinErg = 83.5;} 
  else if((myRand >= 41.004) && (myRand < 41.135)){ myKinErg = 84.5;} 
  else if((myRand >= 41.135) && (myRand < 41.266)){ myKinErg = 85.5;} 
  else if((myRand >= 41.266) && (myRand < 41.381)){ myKinErg = 86.5;} 
  else if((myRand >= 41.381) && (myRand < 41.503)){ myKinErg = 87.5;} 
  else if((myRand >= 41.503) && (myRand < 41.612)){ myKinErg = 88.5;} 
  else if((myRand >= 41.612) && (myRand < 41.72)){ myKinErg = 89.5;} 
  else if((myRand >= 41.72) && (myRand < 41.818)){ myKinErg = 90.5;} 
  else if((myRand >= 41.818) && (myRand < 41.905)){ myKinErg = 91.5;} 
  else if((myRand >= 41.905) && (myRand < 41.987)){ myKinErg = 92.5;} 
  else if((myRand >= 41.987) && (myRand < 42.049)){ myKinErg = 93.5;} 
  else if((myRand >= 42.049) && (myRand < 42.102)){ myKinErg = 94.5;} 
  else if((myRand >= 42.102) && (myRand < 42.146)){ myKinErg = 95.5;} 
  else if((myRand >= 42.146) && (myRand < 42.179)){ myKinErg = 96.5;} 
  else if((myRand >= 42.179) && (myRand < 42.203)){ myKinErg = 97.5;} 
  else if((myRand >= 42.203) && (myRand < 42.22)){ myKinErg = 98.5;} 
  else if((myRand >= 42.22) && (myRand < 42.227)){ myKinErg = 99.5;} 
  else if(myRand >= 42.227){ myKinErg = 0.0;} 



  // Quelle
  fParticleGun->SetParticleEnergy(myKinErg*keV);
  
  // Quadrat, welches dann Überprüft, ob Zahl innerhalb Kreisscheibe ist
  //G4double r = 0.5, x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r);
  //while((x0*x0)+(y0*y0)>r){ x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r); }
  G4double z0 = -(75*mm-22.6*mm); 

  // Zufallszahl mit Phi und R
  G4double radius_quelle = 0.5*mm;
  G4double r_position = radius_quelle *sqrt(G4UniformRand());
  G4double phi = CLHEP::twopi*G4UniformRand();
  G4double x0 = (r_position * sin(phi))  ;
  G4double y0 = (r_position * cos(phi)) ;

  G4double b = 1.146*deg, a = 2.294, alphax = (a*x0)+b, alphay = (a*y0)+b;
  fParticleGun->SetParticlePosition(G4ThreeVector(x0*mm,y0*mm,z0*mm));   
  G4ThreeVector direction = (G4ThreeVector(std::tan(alphax*deg), std::tan(alphay*deg), 1));  
  //G4double alpx = G4UniformRand()*2*alphax-alphax, alpy = G4UniformRand()*2*alphay-alphay
  //G4ThreeVector direction = (G4ThreeVector(std::tan(alpx*deg), std::tan(alpy*deg), 1));     
  fParticleGun->SetParticleMomentumDirection(direction);    
  fParticleGun->GeneratePrimaryVertex(anEvent);
}




