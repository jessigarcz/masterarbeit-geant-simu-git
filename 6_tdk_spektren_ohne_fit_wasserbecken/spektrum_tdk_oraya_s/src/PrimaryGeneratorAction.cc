#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

//new
#include "G4IonTable.hh"
#include "G4ChargedGeantino.hh"
#include "G4RandomDirection.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0) 
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="gamma");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(0.*keV);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  G4double norm = 34.43; 
  G4double myRand = G4UniformRand()*norm; 
  G4double myKinErg = 0.0; 
  if((myRand >= 0) && (myRand < 0.0)){ myKinErg = 0.0;} 
  else if((myRand >= 0.0) && (myRand < 0.057)){ myKinErg = 15.38;} 
  else if((myRand >= 0.057) && (myRand < 0.314)){ myKinErg = 18.46;} 
  else if((myRand >= 0.314) && (myRand < 0.771)){ myKinErg = 20.38;} 
  else if((myRand >= 0.771) && (myRand < 1.285)){ myKinErg = 21.92;} 
  else if((myRand >= 1.285) && (myRand < 1.828)){ myKinErg = 23.08;} 
  else if((myRand >= 1.828) && (myRand < 2.514)){ myKinErg = 25.0;} 
  else if((myRand >= 2.514) && (myRand < 3.371)){ myKinErg = 27.69;} 
  else if((myRand >= 3.371) && (myRand < 4.371)){ myKinErg = 29.23;} 
  else if((myRand >= 4.371) && (myRand < 5.371)){ myKinErg = 31.92;} 
  else if((myRand >= 5.371) && (myRand < 6.342)){ myKinErg = 35.77;} 
  else if((myRand >= 6.342) && (myRand < 7.313)){ myKinErg = 38.85;} 
  else if((myRand >= 7.313) && (myRand < 8.256)){ myKinErg = 40.38;} 
  else if((myRand >= 8.256) && (myRand < 9.17)){ myKinErg = 40.77;} 
  else if((myRand >= 9.17) && (myRand < 10.056)){ myKinErg = 41.92;} 
  else if((myRand >= 10.056) && (myRand < 10.913)){ myKinErg = 43.46;} 
  else if((myRand >= 10.913) && (myRand < 11.713)){ myKinErg = 45.38;} 
  else if((myRand >= 11.713) && (myRand < 12.456)){ myKinErg = 47.31;} 
  else if((myRand >= 12.456) && (myRand < 13.199)){ myKinErg = 49.23;} 
  else if((myRand >= 13.199) && (myRand < 13.913)){ myKinErg = 50.38;} 
  else if((myRand >= 13.913) && (myRand < 14.599)){ myKinErg = 51.15;} 
  else if((myRand >= 14.599) && (myRand < 15.228)){ myKinErg = 52.69;} 
  else if((myRand >= 15.228) && (myRand < 15.828)){ myKinErg = 54.62;} 
  else if((myRand >= 15.828) && (myRand < 16.399)){ myKinErg = 56.15;} 
  else if((myRand >= 16.399) && (myRand < 16.942)){ myKinErg = 56.54;} 
  else if((myRand >= 16.942) && (myRand < 17.571)){ myKinErg = 56.92;} 
  else if((myRand >= 17.571) && (myRand < 18.714)){ myKinErg = 57.69;} 
  else if((myRand >= 18.714) && (myRand < 20.028)){ myKinErg = 58.08;} 
  else if((myRand >= 20.028) && (myRand < 20.771)){ myKinErg = 58.85;} 
  else if((myRand >= 20.771) && (myRand < 21.828)){ myKinErg = 59.23;} 
  else if((myRand >= 21.828) && (myRand < 23.714)){ myKinErg = 59.62;} 
  else if((myRand >= 23.714) && (myRand < 25.543)){ myKinErg = 60.0;} 
  else if((myRand >= 25.543) && (myRand < 26.829)){ myKinErg = 60.38;} 
  else if((myRand >= 26.829) && (myRand < 27.658)){ myKinErg = 60.77;} 
  else if((myRand >= 27.658) && (myRand < 28.144)){ myKinErg = 61.15;} 
  else if((myRand >= 28.144) && (myRand < 28.601)){ myKinErg = 62.31;} 
  else if((myRand >= 28.601) && (myRand < 29.03)){ myKinErg = 64.23;} 
  else if((myRand >= 29.03) && (myRand < 29.401)){ myKinErg = 66.15;} 
  else if((myRand >= 29.401) && (myRand < 30.201)){ myKinErg = 67.31;} 
  else if((myRand >= 30.201) && (myRand < 31.058)){ myKinErg = 68.08;} 
  else if((myRand >= 31.058) && (myRand < 31.658)){ myKinErg = 68.46;} 
  else if((myRand >= 31.658) && (myRand < 32.029)){ myKinErg = 69.23;} 
  else if((myRand >= 32.029) && (myRand < 32.429)){ myKinErg = 69.62;} 
  else if((myRand >= 32.429) && (myRand < 32.858)){ myKinErg = 70.0;} 
  else if((myRand >= 32.858) && (myRand < 33.144)){ myKinErg = 70.38;} 
  else if((myRand >= 33.144) && (myRand < 33.401)){ myKinErg = 71.15;} 
  else if((myRand >= 33.401) && (myRand < 33.63)){ myKinErg = 73.08;} 
  else if((myRand >= 33.63) && (myRand < 33.83)){ myKinErg = 76.15;} 
  else if((myRand >= 33.83) && (myRand < 34.001)){ myKinErg = 78.85;} 
  else if((myRand >= 34.001) && (myRand < 34.144)){ myKinErg = 81.54;} 
  else if((myRand >= 34.144) && (myRand < 34.258)){ myKinErg = 84.23;} 
  else if((myRand >= 34.258) && (myRand < 34.344)){ myKinErg = 87.69;} 
  else if((myRand >= 34.344) && (myRand < 34.401)){ myKinErg = 91.54;} 
  else if((myRand >= 34.401) && (myRand < 34.43)){ myKinErg = 94.23;} 
  else if((myRand >= 34.43) && (myRand < 34.43)){ myKinErg = 100.0;} 
  else if(myRand >= 34.43){ myKinErg = 0.0;} 


 // Quelle
  fParticleGun->SetParticleEnergy(myKinErg*keV);
  
  // Quadrat, welches dann Überprüft, ob Zahl innerhalb Kreisscheibe ist
  //G4double r = 0.5, x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r);
  //while((x0*x0)+(y0*y0)>r){ x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r); }
  G4double z0 = -(75*mm-22.6*mm); 

  // Zufallszahl mit Phi und R
  G4double radius_quelle = 0.5*mm;
  G4double r_position = radius_quelle *sqrt(G4UniformRand());
  G4double phi = CLHEP::twopi*G4UniformRand();
  G4double x0 = (r_position * sin(phi))  ;
  G4double y0 = (r_position * cos(phi)) ;

  G4double b = 1.146*deg, a = 2.294, alphax = (a*x0)+b, alphay = (a*y0)+b;
  fParticleGun->SetParticlePosition(G4ThreeVector(x0*mm,y0*mm,z0*mm));   
  G4ThreeVector direction = (G4ThreeVector(std::tan(alphax*deg), std::tan(alphay*deg), 1));  
  //G4double alpx = G4UniformRand()*2*alphax-alphax, alpy = G4UniformRand()*2*alphay-alphay
  //G4ThreeVector direction = (G4ThreeVector(std::tan(alpx*deg), std::tan(alpy*deg), 1));     
  fParticleGun->SetParticleMomentumDirection(direction);    
  fParticleGun->GeneratePrimaryVertex(anEvent);
}




