#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

//new
#include "G4IonTable.hh"
#include "G4ChargedGeantino.hh"
#include "G4RandomDirection.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0) 
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="gamma");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(0.*keV);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  G4double norm = 56.545; 
  G4double myRand = G4UniformRand()*norm; 
  G4double myKinErg = 0.0; 
  if((myRand >= 0) && (myRand < 0.0)){ myKinErg = 0.5;} 
  else if((myRand >= 0.0) && (myRand < 0.001)){ myKinErg = 1.5;} 
  else if((myRand >= 0.001) && (myRand < 0.007)){ myKinErg = 2.5;} 
  else if((myRand >= 0.007) && (myRand < 0.007)){ myKinErg = 3.5;} 
  else if((myRand >= 0.007) && (myRand < 0.007)){ myKinErg = 4.5;} 
  else if((myRand >= 0.007) && (myRand < 0.012)){ myKinErg = 5.5;} 
  else if((myRand >= 0.012) && (myRand < 0.012)){ myKinErg = 6.5;} 
  else if((myRand >= 0.012) && (myRand < 0.012)){ myKinErg = 7.5;} 
  else if((myRand >= 0.012) && (myRand < 0.012)){ myKinErg = 8.5;} 
  else if((myRand >= 0.012) && (myRand < 0.061)){ myKinErg = 9.5;} 
  else if((myRand >= 0.061) && (myRand < 0.086)){ myKinErg = 10.5;} 
  else if((myRand >= 0.086) && (myRand < 0.13)){ myKinErg = 11.5;} 
  else if((myRand >= 0.13) && (myRand < 0.206)){ myKinErg = 12.5;} 
  else if((myRand >= 0.206) && (myRand < 0.322)){ myKinErg = 13.5;} 
  else if((myRand >= 0.322) && (myRand < 0.493)){ myKinErg = 14.5;} 
  else if((myRand >= 0.493) && (myRand < 0.732)){ myKinErg = 15.5;} 
  else if((myRand >= 0.732) && (myRand < 1.033)){ myKinErg = 16.5;} 
  else if((myRand >= 1.033) && (myRand < 1.418)){ myKinErg = 17.5;} 
  else if((myRand >= 1.418) && (myRand < 1.865)){ myKinErg = 18.5;} 
  else if((myRand >= 1.865) && (myRand < 2.381)){ myKinErg = 19.5;} 
  else if((myRand >= 2.381) && (myRand < 2.973)){ myKinErg = 20.5;} 
  else if((myRand >= 2.973) && (myRand < 3.625)){ myKinErg = 21.5;} 
  else if((myRand >= 3.625) && (myRand < 4.357)){ myKinErg = 22.5;} 
  else if((myRand >= 4.357) && (myRand < 5.134)){ myKinErg = 23.5;} 
  else if((myRand >= 5.134) && (myRand < 5.95)){ myKinErg = 24.5;} 
  else if((myRand >= 5.95) && (myRand < 6.815)){ myKinErg = 25.5;} 
  else if((myRand >= 6.815) && (myRand < 7.71)){ myKinErg = 26.5;} 
  else if((myRand >= 7.71) && (myRand < 8.64)){ myKinErg = 27.5;} 
  else if((myRand >= 8.64) && (myRand < 9.57)){ myKinErg = 28.5;} 
  else if((myRand >= 9.57) && (myRand < 10.526)){ myKinErg = 29.5;} 
  else if((myRand >= 10.526) && (myRand < 11.508)){ myKinErg = 30.5;} 
  else if((myRand >= 11.508) && (myRand < 12.508)){ myKinErg = 31.5;} 
  else if((myRand >= 12.508) && (myRand < 13.482)){ myKinErg = 32.5;} 
  else if((myRand >= 13.482) && (myRand < 14.464)){ myKinErg = 33.5;} 
  else if((myRand >= 14.464) && (myRand < 15.438)){ myKinErg = 34.5;} 
  else if((myRand >= 15.438) && (myRand < 16.438)){ myKinErg = 35.5;} 
  else if((myRand >= 16.438) && (myRand < 17.42)){ myKinErg = 36.5;} 
  else if((myRand >= 17.42) && (myRand < 18.376)){ myKinErg = 37.5;} 
  else if((myRand >= 18.376) && (myRand < 19.341)){ myKinErg = 38.5;} 
  else if((myRand >= 19.341) && (myRand < 20.28)){ myKinErg = 39.5;} 
  else if((myRand >= 20.28) && (myRand < 21.236)){ myKinErg = 40.5;} 
  else if((myRand >= 21.236) && (myRand < 22.183)){ myKinErg = 41.5;} 
  else if((myRand >= 22.183) && (myRand < 23.113)){ myKinErg = 42.5;} 
  else if((myRand >= 23.113) && (myRand < 24.052)){ myKinErg = 43.5;} 
  else if((myRand >= 24.052) && (myRand < 24.964)){ myKinErg = 44.5;} 
  else if((myRand >= 24.964) && (myRand < 25.885)){ myKinErg = 45.5;} 
  else if((myRand >= 25.885) && (myRand < 26.789)){ myKinErg = 46.5;} 
  else if((myRand >= 26.789) && (myRand < 27.666)){ myKinErg = 47.5;} 
  else if((myRand >= 27.666) && (myRand < 28.524)){ myKinErg = 48.5;} 
  else if((myRand >= 28.524) && (myRand < 29.348)){ myKinErg = 49.5;} 
  else if((myRand >= 29.348) && (myRand < 30.176)){ myKinErg = 50.5;} 
  else if((myRand >= 30.176) && (myRand < 31.01)){ myKinErg = 51.5;} 
  else if((myRand >= 31.01) && (myRand < 31.834)){ myKinErg = 52.5;} 
  else if((myRand >= 31.834) && (myRand < 32.675)){ myKinErg = 53.5;} 
  else if((myRand >= 32.675) && (myRand < 33.461)){ myKinErg = 54.5;} 
  else if((myRand >= 33.461) && (myRand < 34.232)){ myKinErg = 55.5;} 
  else if((myRand >= 34.232) && (myRand < 35.007)){ myKinErg = 56.5;} 
  else if((myRand >= 35.007) && (myRand < 36.165)){ myKinErg = 57.5;} 
  else if((myRand >= 36.165) && (myRand < 36.871)){ myKinErg = 58.5;} 
  else if((myRand >= 36.871) && (myRand < 38.248)){ myKinErg = 59.5;} 
  else if((myRand >= 38.248) && (myRand < 38.939)){ myKinErg = 60.5;} 
  else if((myRand >= 38.939) && (myRand < 39.669)){ myKinErg = 61.5;} 
  else if((myRand >= 39.669) && (myRand < 40.358)){ myKinErg = 62.5;} 
  else if((myRand >= 40.358) && (myRand < 41.034)){ myKinErg = 63.5;} 
  else if((myRand >= 41.034) && (myRand < 41.668)){ myKinErg = 64.5;} 
  else if((myRand >= 41.668) && (myRand < 42.301)){ myKinErg = 65.5;} 
  else if((myRand >= 42.301) && (myRand < 42.967)){ myKinErg = 66.5;} 
  else if((myRand >= 42.967) && (myRand < 43.906)){ myKinErg = 67.5;} 
  else if((myRand >= 43.906) && (myRand < 44.524)){ myKinErg = 68.5;} 
  else if((myRand >= 44.524) && (myRand < 45.19)){ myKinErg = 69.5;} 
  else if((myRand >= 45.19) && (myRand < 45.765)){ myKinErg = 70.5;} 
  else if((myRand >= 45.765) && (myRand < 46.313)){ myKinErg = 71.5;} 
  else if((myRand >= 46.313) && (myRand < 46.824)){ myKinErg = 72.5;} 
  else if((myRand >= 46.824) && (myRand < 47.351)){ myKinErg = 73.5;} 
  else if((myRand >= 47.351) && (myRand < 47.848)){ myKinErg = 74.5;} 
  else if((myRand >= 47.848) && (myRand < 48.35)){ myKinErg = 75.5;} 
  else if((myRand >= 48.35) && (myRand < 48.833)){ myKinErg = 76.5;} 
  else if((myRand >= 48.833) && (myRand < 49.335)){ myKinErg = 77.5;} 
  else if((myRand >= 49.335) && (myRand < 49.827)){ myKinErg = 78.5;} 
  else if((myRand >= 49.827) && (myRand < 50.291)){ myKinErg = 79.5;} 
  else if((myRand >= 50.291) && (myRand < 50.726)){ myKinErg = 80.5;} 
  else if((myRand >= 50.726) && (myRand < 51.154)){ myKinErg = 81.5;} 
  else if((myRand >= 51.154) && (myRand < 51.592)){ myKinErg = 82.5;} 
  else if((myRand >= 51.592) && (myRand < 52.038)){ myKinErg = 83.5;} 
  else if((myRand >= 52.038) && (myRand < 52.423)){ myKinErg = 84.5;} 
  else if((myRand >= 52.423) && (myRand < 52.807)){ myKinErg = 85.5;} 
  else if((myRand >= 52.807) && (myRand < 53.171)){ myKinErg = 86.5;} 
  else if((myRand >= 53.171) && (myRand < 53.542)){ myKinErg = 87.5;} 
  else if((myRand >= 53.542) && (myRand < 53.894)){ myKinErg = 88.5;} 
  else if((myRand >= 53.894) && (myRand < 54.246)){ myKinErg = 89.5;} 
  else if((myRand >= 54.246) && (myRand < 54.583)){ myKinErg = 90.5;} 
  else if((myRand >= 54.583) && (myRand < 54.901)){ myKinErg = 91.5;} 
  else if((myRand >= 54.901) && (myRand < 55.214)){ myKinErg = 92.5;} 
  else if((myRand >= 55.214) && (myRand < 55.482)){ myKinErg = 93.5;} 
  else if((myRand >= 55.482) && (myRand < 55.73)){ myKinErg = 94.5;} 
  else if((myRand >= 55.73) && (myRand < 55.95)){ myKinErg = 95.5;} 
  else if((myRand >= 55.95) && (myRand < 56.142)){ myKinErg = 96.5;} 
  else if((myRand >= 56.142) && (myRand < 56.309)){ myKinErg = 97.5;} 
  else if((myRand >= 56.309) && (myRand < 56.453)){ myKinErg = 98.5;} 
  else if((myRand >= 56.453) && (myRand < 56.545)){ myKinErg = 99.5;} 
  else if(myRand >= 56.545){ myKinErg = 0.0;} 



  // Quelle
  fParticleGun->SetParticleEnergy(myKinErg*keV);
  
  // Quadrat, welches dann Überprüft, ob Zahl innerhalb Kreisscheibe ist
  //G4double r = 0.5, x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r);
  //while((x0*x0)+(y0*y0)>r){ x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r); }
  G4double z0 = -(75*mm-22.6*mm); 

  // Zufallszahl mit Phi und R
  G4double radius_quelle = 0.5*mm;
  G4double r_position = radius_quelle *sqrt(G4UniformRand());
  G4double phi = CLHEP::twopi*G4UniformRand();
  G4double x0 = (r_position * sin(phi))  ;
  G4double y0 = (r_position * cos(phi)) ;

  G4double b = 1.146*deg, a = 2.294, alphax = (a*x0)+b, alphay = (a*y0)+b;
  fParticleGun->SetParticlePosition(G4ThreeVector(x0*mm,y0*mm,z0*mm));   
  G4ThreeVector direction = (G4ThreeVector(std::tan(alphax*deg), std::tan(alphay*deg), 1));  
  //G4double alpx = G4UniformRand()*2*alphax-alphax, alpy = G4UniformRand()*2*alphay-alphay
  //G4ThreeVector direction = (G4ThreeVector(std::tan(alpx*deg), std::tan(alpy*deg), 1));     
  fParticleGun->SetParticleMomentumDirection(direction);    
  fParticleGun->GeneratePrimaryVertex(anEvent);
}




