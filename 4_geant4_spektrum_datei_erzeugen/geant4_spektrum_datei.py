import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
import scipy.interpolate as sp
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress
from PIL import Image
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.signal import medfilt


def spektrum_geant4_txt(name, spek_x, spek_y):
	kum_wert = 0
	y_kum = [0]
	for i in range(0,len(spek_x)): 
		kum_wert = kum_wert + spek_y[i]
		y_kum = y_kum + [kum_wert]

	max_wert = y_kum[len(y_kum)-1]
	fobj_out = open(name, "w")

	# Ersten Zeilen
	fobj_out.write("G4double norm = "+str(max_wert)+"; \n")
	fobj_out.write("G4double myRand = G4UniformRand()*norm;	\n")
	fobj_out.write("G4double myKinErg = 0.0; \n")

	# Erste if-Schleife
	fobj_out.write("if((myRand >= "+str(y_kum[0])+") && (myRand < "+str(y_kum[1])+")){ myKinErg = "+str(spek_x[0])+";} \n")

	# Andere if-Werte
	for i in range(1,len(spek_x)):
		fobj_out.write("else if((myRand >= "+str(y_kum[i])+") && (myRand < "+str(y_kum[i+1])+")){ myKinErg = "+str(spek_x[i])+";} \n")

	# Letzte 
	fobj_out.write("else if(myRand >= "+str(y_kum[len(spek_x)])+"){ myKinErg = 0.0;} \n")
	fobj_out.close()
	#print('Werte ausgegeben.')

### Spektren ###
#spek_x, spek_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/calc_mit.txt', unpack=True)
#name = 'geant4_spektren_spekcalc_mit_ohne_fit.txt'
#spek_x, fx, spek_y, fy = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/oraya_mit_f.txt', unpack=True)
#name = 'geant4_spektren_oraya_mit_ohne_fit.txt'
#spek_x, fx, spek_y, fy = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/oraya_s_mit_f.txt', unpack=True)
#name = 'geant4_spektren_oraya_s_mit_ohne_fit.txt'
#spek_x, fx, spek_y, fy = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_mit_f.txt', unpack=True)
#name = 'geant4_spektren_taddei_mit_bild_ohne_fit.txt'
#spek_x, spek_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_mit_daten.txt', unpack=True)
#name = 'geant4_spektren_taddei_mit_daten_ohne_fit.txt'
#spek_x, spek_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_mit_daten_se.txt', unpack=True)
#name = 'geant4_spektren_taddei_mit_daten_se_ohne_fit.txt'

### Fit ###
#spek_x, spek_y = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_oraya_mit_f.txt', unpack=True)
#name = 'geant4_spektren_oraya_mit_f.txt'
#spek_x, spek_y = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_oraya_s_mit_f.txt', unpack=True)
#name = 'geant4_spektren_oraya_s_mit_f.txt'
#spek_x, spek_y = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_spekcalc_mit.txt', unpack=True)
#name = 'geant4_spektren_spekcalc_mit.txt'
#spek_x, spek_y = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mit_bild_f.txt', unpack=True)
#name = 'geant4_spektren_taddei_mit_bild.txt'
#spek_x, spek_y = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mit_bild_unnorm.txt', unpack=True)
#name = 'geant4_spektren_taddei_mit_bild.txt'
#spek_x, spek_y = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mit_daten_se_unnorm.txt', unpack=True)
#name = 'geant4_spektren_taddei_mit_daten_se.txt'
#spek_x, spek_y = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mit_daten.txt', unpack=True)
#name = 'geant4_spektren_taddei_mit_daten.txt'

spektrum_geant4_txt(name, spek_x, spek_y)


