import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
import scipy.interpolate as sp
from matplotlib import colors
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress
from PIL import Image
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.signal import medfilt
import random
import os, sys
from matplotlib import gridspec


from daten_fitten_def import *

datei_ordner = 'spektra_fitten'
data = 18

if(data == 1):
	data_x, data_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_mit_daten.txt', unpack=True)
	peaks = [57, 60, 67.5, 69.5]
	bremsbereich = [[10,55], [58.5,58.5], [62,65], [68.5,68.5], [73,100]]
	
	fehler = 0
	x_min = 10
	x_max = 100
	#plot_max = 2.2
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'taddei_mit_daten'
	label = 'Taddei et al. (ori. Daten)'

if(data == 2):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_mit_f.txt', unpack=True)
	peaks = [57, 60, 67.5, 69.5]
	bremsbereich = [[10,55], [58.5,58.5], [62,65], [68.5,68.5], [73,100]]
	
	fehler = 1 #1: ja; 0: nein
	x_min = 10
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'taddei_mit_bild_f'
	label = 'Taddei et al.'

if(data == 3):
	data_x, data_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_mit_daten_se.txt', unpack=True)
	peaks = [57, 59, 68, 69.5]
	bremsbereich = [[10,56.5], [58,58], [60,64], [68,68.5], [70,95]]
	
	fehler = 0 #1: ja; 0: nein
	x_min = 10
	x_max = 95
	#plot_max = 2.2
	funktion = lognormalpolydrei #lognormal, lognormalpolydrei, tina
	x = np.arange(x_min, x_max, 0.1)
	name = 'taddei_mit_daten_se_unnorm'
	label = 'Taddei et al. (se, ori. Daten)'

if(data == 4):
	data_x, data_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/calc_mit.txt', unpack=True)
	peaks = [57, 60, 67.5, 69.5]
	bremsbereich = [[10,55], [58.5,58.5], [62,65], [68.5,68.5], [73,100]]
	
	fehler = 0
	x_min = 10
	x_max = 100
	#plot_max = 2.2
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'spekcalc_mit'
	label = 'SpekCalc'

if(data == 5):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_mit_f.txt', unpack=True)
	peaks = [57, 60, 67.5, 69.5]
	bremsbereich = [[10,55], [58.5,58.5], [62,65], [68.5,68.5], [73,100]]
	
	fehler = 1
	x_min = 10
	x_max = 100
	#plot_max = 2.2
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'taddei_mit_bild_unnorm'
	label = 'Taddei et al.'
	
if(data == 6):
	data_x, data_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_mit_daten.txt', unpack=True)
	peaks = [57, 60, 67.5, 69.5]
	bremsbereich = [[10,55], [58.5,58.5], [62,65], [68.5,68.5], [73,100]]
	
	fehler = 0
	x_min = 10
	x_max = 100
	#plot_max = 2.2
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'taddei_mit_daten_unnorm'
	label = 'Taddei et al. (ori. Daten)'

if(data == 7):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/oraya_mit_f.txt', unpack=True)
	peaks = [57, 60, 67.5, 69.5]
	bremsbereich = [[10,55], [59.2,58.5], [62,65], [69,68.5], [73,100]]
	
	fehler = 1
	x_min = 10
	x_max = 100
	#plot_max = 2.2
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'oraya_mit_f'
	label = 'Oraya Patent'

if(data == 8):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/oraya_s_mit_f.txt', unpack=True)
	peaks = [57, 60, 67.5, 69.5]
	bremsbereich = [[10,55], [58.5,58.5], [64,61], [68.5,69.3], [71,100]]
	
	fehler = 1
	x_min = 10
	x_max = 100
	#plot_max = 2.2
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'oraya_s_mit_f'
	label = 'Oraya Slide'

	
### OHNE
# Ausnahme, anderer Verlauf SpekCalc data_x, data_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/calc_ohne.txt', unpack=True)

if(data == 9):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/oraya_ohne_f.txt', unpack=True)
	peaks = [9.5, 57, 60, 67.5, 69]
	bremsbereich = [[0,6], [14,55], [59,58.5], [62,64], [68.8,68.85], [71,100]]

	fehler = 1
	x_min = 0
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min,x_max,0.1)
	name = 'oraya_ohne_f'
	label = 'Oraya Patent'

if(data == 10):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_ohne_f.txt', unpack=True)
	peaks = [9.5, 57, 60, 67.5, 69]
	bremsbereich = [[0,4], [12.5,55], [59,58.5], [62,64], [68.8,68.85], [71,100]]

	fehler = 1
	x_min = 0
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'taddei_ohne_f'
	label = 'Taddei et al.'

if(data == 11):
	data_x, data_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_ohne_daten.txt', unpack=True)
	peaks = [9.5, 57, 60, 67.5, 69]
	bremsbereich = [[0,4], [12.5,55], [59,58.5], [62,64], [68.8,68], [73,100]]

	fehler = 0
	x_min = 0
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'taddei_ohne_daten'
	label = 'Taddei et al. (ori. Daten)'

if(data == 12):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/oraya_s_ohne_f.txt', unpack=True)
	peaks = [10, 57, 60, 67.5, 69]
	bremsbereich = [[0,5], [12.5,55], [59,58.5], [62,66], [68.5,68.5], [72,100]]

	fehler = 1
	x_min = 0
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'oraya_s_ohne_f'
	label = 'Oraya Slide'

if(data == 13):
	data_x, data_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_ohne_daten_se.txt', unpack=True)
	peaks = [10, 57, 60, 67.5, 69]
	bremsbereich = [[0,5], [12.5,55], [59,58.5], [62,66], [68.5,68.5], [72,100]]

	fehler = 0
	x_min = 0
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min, x_max, 0.1)
	name = 'taddei_ohne_daten_se'
	label = 'Taddei et al. (se, ori. Daten)'


### MAC
if(data == 14):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/oraya_macula_f.txt', unpack=True)
	#x_oraya_s_mac_ori, x_oraya_s_mac_f_ori, y_oraya_s_mac_ori, y_oraya_s_mac_f_ori = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/oraya_s_macula_f.txt', unpack=True)
	#x_taddei_mac, x_taddei_mac_f, y_taddei_mac, y_taddei_mac_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_mac_f.txt', unpack=True)
	#x_taddei_mac_ori, x_taddei_mac_f_ori, y_taddei_mac_ori, y_taddei_mac_f_ori = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_macula_f.txt', unpack=True)
	#x_taddei_mac_se_ori, y_taddei_mac_se_ori = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_mac_daten_se.txt', unpack=True)
	peaks = [57.5, 60, 67.5]
	bremsbereich = [[18,55], [59.5,59.1], [61.5,66], [72,100]]

	fehler = 1
	x_min = 18
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min,x_max,0.1)
	name = 'oraya_mac'
	label = 'Oraya Patent'

if(data == 15):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/oraya_s_macula_f.txt', unpack=True)
	#x_taddei_mac, x_taddei_mac_f, y_taddei_mac, y_taddei_mac_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_mac_f.txt', unpack=True)
	#x_taddei_mac_ori, x_taddei_mac_f_ori, y_taddei_mac_ori, y_taddei_mac_f_ori = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_macula_f.txt', unpack=True)
	#x_taddei_mac_se_ori, y_taddei_mac_se_ori = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_mac_daten_se.txt', unpack=True)
	peaks = [57.5, 60, 67.5, 69.5]
	bremsbereich = [[18,53], [59.7,59], [64,65.2], [69.4,68.5], [72,100]]

	fehler = 1
	x_min = 18
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min,x_max,0.1)
	name = 'oraya_s_mac_unnorm'
	label = 'Oraya Slide'

if(data == 16):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/brems/taddei_mac_f.txt', unpack=True)
	#x_taddei_mac_ori, x_taddei_mac_f_ori, y_taddei_mac_ori, y_taddei_mac_f_ori = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_macula_f.txt', unpack=True)
	#x_taddei_mac_se_ori, y_taddei_mac_se_ori = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_mac_daten_se.txt', unpack=True)
	peaks = [57.5, 60, 67.5, 69.5]
	bremsbereich = [[18,55], [58.8,58.1], [64,65.2], [69,68.5], [71,100]]

	fehler = 1
	x_min = 18
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min,x_max,0.1)
	name = 'taddei_mac_bild_f'
	label = 'Taddei et al.'

if(data == 17):
	data_x, data_x_f, data_y, data_y_f = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_macula_f.txt', unpack=True)
	#x_taddei_mac_se_ori, y_taddei_mac_se_ori = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_mac_daten_se.txt', unpack=True)
	peaks = [57.5, 60, 67.5, 69.5]
	bremsbereich = [[18,55], [58.8,58.1], [64,65.2], [69,68.5], [71,100]]

	fehler = 1
	x_min = 18
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min,x_max,0.1)
	name = 'taddei_mac_bild_f_unnorm'
	label = 'Taddei et al.'

if(data == 18):
	data_x, data_y = np.loadtxt('../1_spektren_auslesen/auslesedaten/original/taddei_mac_daten_se.txt', unpack=True)
	peaks = [57.5, 60, 67.5, 69.5]
	bremsbereich = [[18,55], [58.8,58.1], [64,65.2], [69,68.5], [71,100]]

	fehler = 0
	x_min = 18
	x_max = 100
	funktion = lognormalpolydrei
	x = np.arange(x_min,x_max,0.1)
	name = 'taddei_mac_daten_se_unnorm'
	label = 'Taddei et al. (se, ori. Daten)'



print()
print(name)

# Daten auf xmin und xmax reduzieren
x = []
y = []
for j in range(0,len(data_x)):
	if(data_x[j] >= x_min and data_x[j] <= x_max):
		x = x + [data_x[j]]
		y = y + [data_y[j]]

### Plot ohne Abweichung
#fig = plt.figure()
#ax1 = fig.add_subplot(3,1,(1,2), sharex=True)

fig, (ax1, ax2) = plt.subplots(2, sharex=True, gridspec_kw = {'height_ratios':[2, 1]})
#gs = gridspec.GridSpec(2, 1, width_ratios=[2, 1])

#ax1.grid()
if fehler==1:
	ax1.errorbar(data_x, data_y, xerr=data_x_f, yerr=data_y_f, fmt='rx', markersize=8, markeredgewidth=1)
else:
	ax1.plot(data_x, data_y, 'rx', markersize=8, markeredgewidth=1)

### Bremsspektrum	
x_brems, y_brems = filter_brems_bereich(x, y, bremsbereich)
#ax1.plot(x_brems, y_brems, 'bx', linewidth=2) ###
f_brems = fitten(x_brems, y_brems, funktion)
x_brems = np.linspace(min(x_brems), max(x_brems), num=100)
#print(normabwmesswerte(x_brems, y_brems, f_brems),"%")
#ax1.plot(x_brems, f_brems(x_brems), 'b-', linewidth=2) ###

### Gesamtfunktion
f_ges = f_brems
x_ges = x_brems

### Peakspektrum
print("Peakspektrum:")
for p in range(0,len(peaks)):
	x_peak = []
	y_peak = []
	x_peak, y_peak = filter_peak_bereich(x, y, bremsbereich, peaks[p])
	#ax1.plot(x_peak, y_peak, 'gx', linewidth=2) ###
	x_peak_oB, y_peak_oB, x_verschub, peakhohe = peaknormieren(x_peak, y_peak, f_brems)
	#print(peakhohe)
	x_peak_neu, f_peak = fittenverschieben(x_peak_oB, y_peak_oB, normalvert_mu, f_brems, x_verschub, peakhohe)
	#x_peak_neu = np.linspace(min(x_peak)-1, max(x_peak)+1, num=50)

	#ax1.plot(x_peak_neu, f_peak(x_peak_neu), 'b-', linewidth=2) ###
	x_ges, f_ges = funktionenzusammen(x_ges, f_ges, x_peak_neu, f_peak)

ax1.plot(x_ges, f_ges(x_ges), 'g-', linewidth=2, label=str(label)) ###
ax1.tick_params(axis='both', right='on', labelleft='off', labelright='on')
ax1.yaxis.set_label_position("right")
ax1.set_ylabel('Intensität   /   Photonen/cm$^2$')
ax1.set_xlim(x_min, x_max)
#ax1.legend(loc=2)
ax1.set_ylim(0, )

ax1.set_title(str(label))

ax2.grid()
x_abw = []
abw = []
	
for j in range(0,len(y)-1):
	if(y[j] != 0):
		x_abw = x_abw + [x[j]]
		feh = (y[j]-f_ges(x[j]))
		abw = abw + [feh]
ax2.plot(x_abw, abw, 'rx', markersize=8, markeredgewidth=1.5)

limit = round(max(y)/10,2)
ax2.set_ylim(-limit,limit)
ax2.set_xlim(x_min, x_max)
plt.xlabel('Energie   /   keV')
plt.ylabel('absolute Abweichung')

fig.subplots_adjust(hspace=0)
plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)

plt.savefig(str(datei_ordner)+'/spektren_'+str(name)+'.pdf')

werte_ausgeben(str(datei_ordner)+'/spektren_'+str(name)+'.txt', x_ges, f_ges(x_ges))

