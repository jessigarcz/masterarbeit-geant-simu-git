import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
import scipy.interpolate as sp
from matplotlib import colors
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress
from PIL import Image
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.signal import medfilt
import random

farbe = ['royalblue', 'mediumpurple', 'seagreen', 'orchid', 'firebrick', 'darkorange' ]
strich = ['-', '--', '-.', ':']
marker= ['o', 'v', 's', '^', 'p', '<', 'd', '>']

def werte_ausgeben(name, xwerte, ywerte):
	fobj_out = open(name, "w")
	for i in range(0,len(xwerte)):
		fobj_out.write(str(xwerte[i]) + "	" + str(ywerte[i]) + "\n")
	fobj_out.close()
	#print('Werte ausgegeben.')

def max_array_zwischen(array_x, array_y, mini, maxi):
	wert_max = 0
	x_wert = 0
	for i in range(0, len(array_y)):
		#print()
		#print('Maxarray:')
		#print(str(i)+': '+str(array_x[i])+' mit max:'+str(maxi)+' und min:'+str(mini))
		if((array_x[i] <= maxi) and (array_x[i] >= mini)):
			#print('zwischen min/max')
			if(array_y[i] > wert_max):
				#print('yes')
				wert_max = array_y[i]
				x_wert = array_x[i]
				ind = i
	return x_wert, wert_max, ind

def steigung(x1, x2, y1, y2):
	return (y2-y1)/(x2-x1)

def filter_brems_bereich(arrx, arry, bremsber_x):
	arrx_brems = []
	arry_brems = []

	for be in range(0,len(bremsber_x)):
		for x in range(0,len(arrx)):
			if(arrx[x] >= bremsber_x[be][0] and arrx[x] <= bremsber_x[be][1]):
				arrx_brems = arrx_brems + [arrx[x]]
				arry_brems = arry_brems + [arry[x]]

	#werte_ausgeben('../1_spektren_auslesen/auslesedaten/brems/bremsspektrum_werte/'+str(name[i])+'.txt', arrx_brems, arry_brems)
	#print(arrx_brems)
	return arrx_brems, arry_brems

def filter_peak_bereich(arrx, arry, bremsber_x, peak):
	arrx_peaks = []
	arry_peaks = []
	arrx_peak = []
	arry_peak = []
	abw = 1
	#print()
	#print('Peak: ')

	for be in range(0,len(bremsber_x)-1):
		if(peak >= bremsber_x[be][1] and peak <= bremsber_x[be+1][0]):
			x_min = bremsber_x[be][1]
			x_max = bremsber_x[be+1][0]

			# Alle möglichen Werte aus dem Bereich filtern
			for x in range(0,len(arrx)):
				if(arrx[x] >= x_min and arrx[x] <= x_max):
					arrx_peaks = arrx_peaks + [arrx[x]]
					arry_peaks = arry_peaks + [arry[x]]

			# Alle Auf- und absteigenden Flanken filtern
	#		ind_aufst = []
	#		ind_abst = []
	#		for ind in range(0, len(arrx_peaks)-1):
	#			if(steigung(arrx_peaks[ind],arrx_peaks[ind+1],arry_peaks[ind],arry_peaks[ind+1]) >= 0 and arrx_peaks[ind] <= peak+abw):
	#				if(len(ind_aufst) == 0):
	#					ind_aufst = [ind] + ind_aufst
	#				ind_aufst = [ind+1] + ind_aufst

	#			if(steigung(arrx_peaks[ind],arrx_peaks[ind+1],arry_peaks[ind],arry_peaks[ind+1]) <= 0 and arrx_peaks[ind] >= peak-abw):
	#				#print(ind_abst[len(ind_abst)-1])
	#				#print(ind)
	#				if(len(ind_abst) == 0):
	#					ind_abst = ind_abst + [ind]
	#				ind_abst = ind_abst + [ind+1]
	#				#print(ind_abst)
				
	#		# Indizes filtern
	#		ind_aufst_hilf = [ind_aufst[0]]
	#		ind_abst_hilf = [ind_abst[0]]
	#		for ind in range(0, len(ind_aufst)-1):
	#			if((ind_aufst_hilf[ind]-1) == ind_aufst[ind+1]):
	#				ind_aufst_hilf = ind_aufst_hilf + [ind_aufst[ind+1]]
	#			else:
	#				ind_aufst = []
	#				ind_aufst = ind_aufst_hilf
	#				break
	#		for ind in range(0, len(ind_abst)-1):
	#			if((ind_abst_hilf[ind]+1) == ind_abst[ind+1]):
	#				ind_abst_hilf = ind_abst_hilf + [ind_abst[ind+1]]
	#			else:
	#				ind_abst = []
	#				ind_abst = ind_abst_hilf
	#				break
	#		#print(ind_abst)

	#		for ind in range(0, len(ind_aufst)):
	#			#print(ind)
	#			#print(arrx_peaks[ind_aufst[ind]])
	#			arrx_peak = [arrx_peaks[ind_aufst[ind]]] + arrx_peak
	#			arry_peak = [arry_peaks[ind_aufst[ind]]] + arry_peak

	#		for ind in range(0, len(ind_abst)):
	#			#print(ind)
	#			#print(arrx_peaks[ind_abst[ind]])
	#			arrx_peak = arrx_peak + [arrx_peaks[ind_abst[ind]]]
	#			arry_peak = arry_peak + [arry_peaks[ind_abst[ind]]]
	#		#print(arrx_peak)
	#		#print(arry_peak)

	#werte_ausgeben('../1_spektren_auslesen/auslesedaten/brems/peaks_werte/'+str(name[i])+'.txt', arrx_peak, arry_peak)
	return arrx_peaks, arry_peaks

def normalvert(x, yskal, sigma, mu):
	pi = 3.14159265359
	return (sigma**2*2*pi)**(-1/2)*np.exp(-((x-mu)**2)/(2*sigma**2))*yskal
	
def expo(x, xskal, yskal, mu):
	return (np.exp(mu*(x+xskal))*yskal)

def linear(x, m, b):
	return (m*x+b)
	
def quadrat(x, a, b, c):
	return (a*x**2+b*x+c)
	
def crystalball(x, xmit, sigma, alpha, n):
	pi = 3.14159265359
	C = (n/abs(alpha))*1/(n-1)*np.exp(-(abs(alpha)**2)/2)
	D = np.sqrt(pi/2)*(1+scipy.special.erf(abs(alpha)/np.sqrt(2)))
	N = 1/(sigma*(C+D))
	if(((x-xmit)/sigma) > -alpha):
		f = N*np.exp(-(x-xmit)**2/2*sigma**2)
	else:
		A = (n/abs(alpha))**n*np.exp(-(abs(alpha)**2)/2)
		B = n/abs(alpha)-abs(alpha)	
		f = N*A*(B*(x-xmit)/sigma)**(-n)
	return f
	
# Guassian with background
def gaus_and_background(x,N,mu,sigma,a,b,c):
	return N*exp(-0.5 * ((x-mu)/2*sigma)**2 ) + a*x**2 + b*x + c
	
#def fitten(funk, x, y):
#	params, covar = curve_fit(funk, x, y)
#	uparams = unumpy.uarray(params, np.sqrt(np.diag(covar)))
#	return params, covar, uparams

def fitten(xarr, yarr, func):
	params, covar = curve_fit(func, xarr, yarr)
	fehler = np.sqrt(np.diag(covar))
	fehler = fehler/params*100
	uparams = unumpy.uarray(params, np.sqrt(np.diag(covar)))
	x = np.linspace(min(xarr), max(xarr))
	y = []
	
	print("Bremsspktrum - Parameter:")
	print(uparams)
	print("Fehler in Prozent: ")
	print(fehler)
	print()
	
	for i in range(0, len(x)):
		y = y + [abs(func(x[i], *params))]

	f = interp1d(x, y)
	return f

def fittenverschieben(xarr, yarr, func, f_brems, x_verschub, y_max):
	params, covar = curve_fit(func, xarr, yarr)
	fehler = np.sqrt(np.diag(covar))
	fehler = fehler/params*100
	uparams = unumpy.uarray(params, np.sqrt(np.diag(covar)))
	x = np.linspace(min(xarr)+x_verschub, max(xarr)+x_verschub)
	y = []
	
	#print("Parameter: ")
	#print(uparams)
	#print("Fehler in Prozent: ")
	#print(fehler)

	for i in range(0, len(x)):
		y = y + [(func(x[i]-x_verschub, *params) + f_brems(x[i]))] 
	skal = (y_max/max(y))
	#print(y_max)
	#print(max(y))

	for i in range(0, len(x)):
		y[i] = y[i]*skal 

	f = interp1d(x, y)
	return x, f


def polydrei(x, a, b, c, d):
	return a*(x-b)**3+c*(x-b)**2+d*(x-b)

def polyvier(x, a, b, c, d, e):
	return a*(x-b)**4+c*(x-b)**3+d*(x-b)**2+e*(x-b)


def lognormal(x, yskal, sigma, mu):
	pi = 3.14159265359
	return ((1/(x*sigma*(2*pi)**(1/2)))*np.exp(-((np.log(x)-mu)**2)/(2*sigma**2))*yskal)

def lognormalpolyfunf(x, yskal, sigma, mu, a, b):
	pi = 3.14159265359
	return a*(x-b)**5*(np.exp(-((np.log(x)-mu)**2)/(2*sigma**2)))+(yskal*x)

def lognormalpolydrei(x, yskal, sigma, mu, a, b):
	pi = 3.14159265359
	return a*(x-b)**3*(np.exp(-((np.log(x)-mu)**2)/(2*sigma**2)))+(yskal*x)

def tina(x, a, b, c, d, f):
	return a*(x-b)**2*(np.exp((x-c)**2/d)-1)**(-1)+f
	

def normabwmesswerte(xarr, yarr, func):
	abw = 0
	for i in range(0,len(xarr)):
		wert = (yarr[i] - func(xarr[i]))
		abw = abw + wert
	return (abw/len(xarr))*100


def peaknormieren(x_peak, y_peak, f_brems):
	x_neu = []
	y_neu = []
	for i in range(0, len(y_peak)):
		y_neu = y_neu + [y_peak[i] - f_brems(x_peak[i])]
	#print(x_peak)
	x_maxpeak, y_maxpeak, ind_maxpeak = max_array_zwischen(x_peak, y_peak, min(x_peak), max(x_peak))
	for i in range(0, len(x_peak)):
		x_neu = x_neu + [x_peak[i] - x_maxpeak]

	# Mehr Werte im Nullbereich hinzufugen
	#anz_werte = 1
	#for i in range(0, anz_werte):
	#	x_neu = [x_neu[0]-2] + x_neu
	#	x_neu = x_neu + [x_neu[len(x_neu)-1]+2]
	#	y_neu = [0] + y_neu
	#	y_neu = y_neu + [0]

	return x_neu, y_neu, x_maxpeak, y_maxpeak




def normalvert_mu(x, yskal, sigma):
	pi = 3.14159265359
	mu = 0
	return (pi*sigma**2)**(-1/2)*np.exp(-((x-mu)**2)/(sigma**2))*yskal


def funktionenzusammen(x_1, f_1, x_2, f_2):
	x2min = min(x_2)
	x2max = max(x_2)

	y_neu = []
	x = np.arange(min(x_1), max(x_1), step=0.1)
	
	for i in range(0,len(x)):
		if((x[i] >= x2min) and (x[i] <= x2max)):
			if(f_2(x[i]) >= f_1(x[i])):
				y_neu = y_neu + [f_2(x[i])]
			else:
				y_neu = y_neu + [f_1(x[i])]
		else:
			y_neu = y_neu + [f_1(x[i])]
		
	f = interp1d(x, y_neu)
	return x, f

def kumulativ(x, f):
	print('Kumulativ')
	min_x = int(round(min(x))+1)
	max_x = int(round(max(x))-1)
	x_kum = []
	y_kum = []
	f_akt = 0
	for i in range(min_x, max_x):
		x_kum = x_kum + [i]
		f_akt = f_akt + f(i)
		y_kum = y_kum + [f_akt]

		x_kum = x_kum + [i+0.5]
		f_akt = f_akt + f(i+0.5)
		y_kum = y_kum + [f_akt]
	return x_kum, y_kum
