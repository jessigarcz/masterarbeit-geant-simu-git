#include "PrimaryGeneratorAction.hh"

#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "G4SystemOfUnits.hh"
#include "Randomize.hh"

//new
#include "G4IonTable.hh"
#include "G4ChargedGeantino.hh"
#include "G4RandomDirection.hh"

PrimaryGeneratorAction::PrimaryGeneratorAction()
: G4VUserPrimaryGeneratorAction(),
  fParticleGun(0) 
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="gamma");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(0.*keV);
}

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  G4double norm = 109.477729983; 
  G4double myRand = G4UniformRand()*norm; 
  G4double myKinErg = 0.0; 
  if((myRand >= 0) && (myRand < 0.00166796603976)){ myKinErg = 12.8;} 
  else if((myRand >= 0.00166796603976) && (myRand < 0.00418348799485)){ myKinErg = 12.9;} 
  else if((myRand >= 0.00418348799485) && (myRand < 0.00754656586528)){ myKinErg = 13.0;} 
  else if((myRand >= 0.00754656586528) && (myRand < 0.011757199651)){ myKinErg = 13.1;} 
  else if((myRand >= 0.011757199651) && (myRand < 0.0168153893521)){ myKinErg = 13.2;} 
  else if((myRand >= 0.0168153893521) && (myRand < 0.0227211349686)){ myKinErg = 13.3;} 
  else if((myRand >= 0.0227211349686) && (myRand < 0.0294744365003)){ myKinErg = 13.4;} 
  else if((myRand >= 0.0294744365003) && (myRand < 0.0370752939474)){ myKinErg = 13.5;} 
  else if((myRand >= 0.0370752939474) && (myRand < 0.0455237073098)){ myKinErg = 13.6;} 
  else if((myRand >= 0.0455237073098) && (myRand < 0.0548196765876)){ myKinErg = 13.7;} 
  else if((myRand >= 0.0548196765876) && (myRand < 0.0649632017807)){ myKinErg = 13.8;} 
  else if((myRand >= 0.0649632017807) && (myRand < 0.0759542828891)){ myKinErg = 13.9;} 
  else if((myRand >= 0.0759542828891) && (myRand < 0.0877929199129)){ myKinErg = 14.0;} 
  else if((myRand >= 0.0877929199129) && (myRand < 0.100479112852)){ myKinErg = 14.1;} 
  else if((myRand >= 0.100479112852) && (myRand < 0.114012861706)){ myKinErg = 14.2;} 
  else if((myRand >= 0.114012861706) && (myRand < 0.128394166476)){ myKinErg = 14.3;} 
  else if((myRand >= 0.128394166476) && (myRand < 0.143623027161)){ myKinErg = 14.4;} 
  else if((myRand >= 0.143623027161) && (myRand < 0.159699443762)){ myKinErg = 14.5;} 
  else if((myRand >= 0.159699443762) && (myRand < 0.176839378481)){ myKinErg = 14.6;} 
  else if((myRand >= 0.176839378481) && (myRand < 0.19588508391)){ myKinErg = 14.7;} 
  else if((myRand >= 0.19588508391) && (myRand < 0.21683656005)){ myKinErg = 14.8;} 
  else if((myRand >= 0.21683656005) && (myRand < 0.239693806901)){ myKinErg = 14.9;} 
  else if((myRand >= 0.239693806901) && (myRand < 0.264456824462)){ myKinErg = 15.0;} 
  else if((myRand >= 0.264456824462) && (myRand < 0.291125612734)){ myKinErg = 15.1;} 
  else if((myRand >= 0.291125612734) && (myRand < 0.319700171717)){ myKinErg = 15.2;} 
  else if((myRand >= 0.319700171717) && (myRand < 0.35018050141)){ myKinErg = 15.3;} 
  else if((myRand >= 0.35018050141) && (myRand < 0.382566601813)){ myKinErg = 15.4;} 
  else if((myRand >= 0.382566601813) && (myRand < 0.416858472928)){ myKinErg = 15.5;} 
  else if((myRand >= 0.416858472928) && (myRand < 0.453056114753)){ myKinErg = 15.6;} 
  else if((myRand >= 0.453056114753) && (myRand < 0.491159527288)){ myKinErg = 15.7;} 
  else if((myRand >= 0.491159527288) && (myRand < 0.531168710534)){ myKinErg = 15.8;} 
  else if((myRand >= 0.531168710534) && (myRand < 0.573083664491)){ myKinErg = 15.9;} 
  else if((myRand >= 0.573083664491) && (myRand < 0.616904389158)){ myKinErg = 16.0;} 
  else if((myRand >= 0.616904389158) && (myRand < 0.662630884536)){ myKinErg = 16.1;} 
  else if((myRand >= 0.662630884536) && (myRand < 0.710263150625)){ myKinErg = 16.2;} 
  else if((myRand >= 0.710263150625) && (myRand < 0.759801187424)){ myKinErg = 16.3;} 
  else if((myRand >= 0.759801187424) && (myRand < 0.811401307137)){ myKinErg = 16.4;} 
  else if((myRand >= 0.811401307137) && (myRand < 0.865290162461)){ myKinErg = 16.5;} 
  else if((myRand >= 0.865290162461) && (myRand < 0.921467753394)){ myKinErg = 16.6;} 
  else if((myRand >= 0.921467753394) && (myRand < 0.979934079937)){ myKinErg = 16.7;} 
  else if((myRand >= 0.979934079937) && (myRand < 1.04068914209)){ myKinErg = 16.8;} 
  else if((myRand >= 1.04068914209) && (myRand < 1.10373293985)){ myKinErg = 16.9;} 
  else if((myRand >= 1.10373293985) && (myRand < 1.16906547323)){ myKinErg = 17.0;} 
  else if((myRand >= 1.16906547323) && (myRand < 1.23668674221)){ myKinErg = 17.1;} 
  else if((myRand >= 1.23668674221) && (myRand < 1.3065967468)){ myKinErg = 17.2;} 
  else if((myRand >= 1.3065967468) && (myRand < 1.378795487)){ myKinErg = 17.3;} 
  else if((myRand >= 1.378795487) && (myRand < 1.45328296282)){ myKinErg = 17.4;} 
  else if((myRand >= 1.45328296282) && (myRand < 1.53005917424)){ myKinErg = 17.5;} 
  else if((myRand >= 1.53005917424) && (myRand < 1.60912412127)){ myKinErg = 17.6;} 
  else if((myRand >= 1.60912412127) && (myRand < 1.69047780392)){ myKinErg = 17.7;} 
  else if((myRand >= 1.69047780392) && (myRand < 1.77412022217)){ myKinErg = 17.8;} 
  else if((myRand >= 1.77412022217) && (myRand < 1.86005137603)){ myKinErg = 17.9;} 
  else if((myRand >= 1.86005137603) && (myRand < 1.9482712655)){ myKinErg = 18.0;} 
  else if((myRand >= 1.9482712655) && (myRand < 2.03877989058)){ myKinErg = 18.1;} 
  else if((myRand >= 2.03877989058) && (myRand < 2.13156600538)){ myKinErg = 18.2;} 
  else if((myRand >= 2.13156600538) && (myRand < 2.22662248749)){ myKinErg = 18.3;} 
  else if((myRand >= 2.22662248749) && (myRand < 2.32394933691)){ myKinErg = 18.4;} 
  else if((myRand >= 2.32394933691) && (myRand < 2.42354655364)){ myKinErg = 18.5;} 
  else if((myRand >= 2.42354655364) && (myRand < 2.52541413769)){ myKinErg = 18.6;} 
  else if((myRand >= 2.52541413769) && (myRand < 2.62955208904)){ myKinErg = 18.7;} 
  else if((myRand >= 2.62955208904) && (myRand < 2.73596040771)){ myKinErg = 18.8;} 
  else if((myRand >= 2.73596040771) && (myRand < 2.8446390937)){ myKinErg = 18.9;} 
  else if((myRand >= 2.8446390937) && (myRand < 2.95558814699)){ myKinErg = 19.0;} 
  else if((myRand >= 2.95558814699) && (myRand < 3.0688075676)){ myKinErg = 19.1;} 
  else if((myRand >= 3.0688075676) && (myRand < 3.18429735552)){ myKinErg = 19.2;} 
  else if((myRand >= 3.18429735552) && (myRand < 3.30205751075)){ myKinErg = 19.3;} 
  else if((myRand >= 3.30205751075) && (myRand < 3.42208803329)){ myKinErg = 19.4;} 
  else if((myRand >= 3.42208803329) && (myRand < 3.54438892315)){ myKinErg = 19.5;} 
  else if((myRand >= 3.54438892315) && (myRand < 3.66896018032)){ myKinErg = 19.6;} 
  else if((myRand >= 3.66896018032) && (myRand < 3.7958018048)){ myKinErg = 19.7;} 
  else if((myRand >= 3.7958018048) && (myRand < 3.9249137966)){ myKinErg = 19.8;} 
  else if((myRand >= 3.9249137966) && (myRand < 4.0562961557)){ myKinErg = 19.9;} 
  else if((myRand >= 4.0562961557) && (myRand < 4.1897393948)){ myKinErg = 20.0;} 
  else if((myRand >= 4.1897393948) && (myRand < 4.32519637923)){ myKinErg = 20.1;} 
  else if((myRand >= 4.32519637923) && (myRand < 4.46266710901)){ myKinErg = 20.2;} 
  else if((myRand >= 4.46266710901) && (myRand < 4.60215158412)){ myKinErg = 20.3;} 
  else if((myRand >= 4.60215158412) && (myRand < 4.74364980458)){ myKinErg = 20.4;} 
  else if((myRand >= 4.74364980458) && (myRand < 4.88716177038)){ myKinErg = 20.5;} 
  else if((myRand >= 4.88716177038) && (myRand < 5.03268748151)){ myKinErg = 20.6;} 
  else if((myRand >= 5.03268748151) && (myRand < 5.18022693799)){ myKinErg = 20.7;} 
  else if((myRand >= 5.18022693799) && (myRand < 5.3297801398)){ myKinErg = 20.8;} 
  else if((myRand >= 5.3297801398) && (myRand < 5.48134708696)){ myKinErg = 20.9;} 
  else if((myRand >= 5.48134708696) && (myRand < 5.63492777946)){ myKinErg = 21.0;} 
  else if((myRand >= 5.63492777946) && (myRand < 5.79052221729)){ myKinErg = 21.1;} 
  else if((myRand >= 5.79052221729) && (myRand < 5.94813040047)){ myKinErg = 21.2;} 
  else if((myRand >= 5.94813040047) && (myRand < 6.10775232899)){ myKinErg = 21.3;} 
  else if((myRand >= 6.10775232899) && (myRand < 6.26938800284)){ myKinErg = 21.4;} 
  else if((myRand >= 6.26938800284) && (myRand < 6.43303742204)){ myKinErg = 21.5;} 
  else if((myRand >= 6.43303742204) && (myRand < 6.59870058658)){ myKinErg = 21.6;} 
  else if((myRand >= 6.59870058658) && (myRand < 6.76637003417)){ myKinErg = 21.7;} 
  else if((myRand >= 6.76637003417) && (myRand < 6.93568757506)){ myKinErg = 21.8;} 
  else if((myRand >= 6.93568757506) && (myRand < 7.10665320926)){ myKinErg = 21.9;} 
  else if((myRand >= 7.10665320926) && (myRand < 7.27926693675)){ myKinErg = 22.0;} 
  else if((myRand >= 7.27926693675) && (myRand < 7.45352875754)){ myKinErg = 22.1;} 
  else if((myRand >= 7.45352875754) && (myRand < 7.62943867163)){ myKinErg = 22.2;} 
  else if((myRand >= 7.62943867163) && (myRand < 7.80699667903)){ myKinErg = 22.3;} 
  else if((myRand >= 7.80699667903) && (myRand < 7.98620277972)){ myKinErg = 22.4;} 
  else if((myRand >= 7.98620277972) && (myRand < 8.16705697371)){ myKinErg = 22.5;} 
  else if((myRand >= 8.16705697371) && (myRand < 8.349559261)){ myKinErg = 22.6;} 
  else if((myRand >= 8.349559261) && (myRand < 8.5337096416)){ myKinErg = 22.7;} 
  else if((myRand >= 8.5337096416) && (myRand < 8.71950811549)){ myKinErg = 22.8;} 
  else if((myRand >= 8.71950811549) && (myRand < 8.90695468269)){ myKinErg = 22.9;} 
  else if((myRand >= 8.90695468269) && (myRand < 9.09604934318)){ myKinErg = 23.0;} 
  else if((myRand >= 9.09604934318) && (myRand < 9.28679209698)){ myKinErg = 23.1;} 
  else if((myRand >= 9.28679209698) && (myRand < 9.47918294407)){ myKinErg = 23.2;} 
  else if((myRand >= 9.47918294407) && (myRand < 9.67322188447)){ myKinErg = 23.3;} 
  else if((myRand >= 9.67322188447) && (myRand < 9.86890891816)){ myKinErg = 23.4;} 
  else if((myRand >= 9.86890891816) && (myRand < 10.0661560214)){ myKinErg = 23.5;} 
  else if((myRand >= 10.0661560214) && (myRand < 10.2646591123)){ myKinErg = 23.6;} 
  else if((myRand >= 10.2646591123) && (myRand < 10.4644181907)){ myKinErg = 23.7;} 
  else if((myRand >= 10.4644181907) && (myRand < 10.6654332567)){ myKinErg = 23.8;} 
  else if((myRand >= 10.6654332567) && (myRand < 10.8677043103)){ myKinErg = 23.9;} 
  else if((myRand >= 10.8677043103) && (myRand < 11.0712313515)){ myKinErg = 24.0;} 
  else if((myRand >= 11.0712313515) && (myRand < 11.2760143803)){ myKinErg = 24.1;} 
  else if((myRand >= 11.2760143803) && (myRand < 11.4820533966)){ myKinErg = 24.2;} 
  else if((myRand >= 11.4820533966) && (myRand < 11.6893484005)){ myKinErg = 24.3;} 
  else if((myRand >= 11.6893484005) && (myRand < 11.8978993921)){ myKinErg = 24.4;} 
  else if((myRand >= 11.8978993921) && (myRand < 12.1077063712)){ myKinErg = 24.5;} 
  else if((myRand >= 12.1077063712) && (myRand < 12.3187693379)){ myKinErg = 24.6;} 
  else if((myRand >= 12.3187693379) && (myRand < 12.5310882921)){ myKinErg = 24.7;} 
  else if((myRand >= 12.5310882921) && (myRand < 12.744663234)){ myKinErg = 24.8;} 
  else if((myRand >= 12.744663234) && (myRand < 12.9594941634)){ myKinErg = 24.9;} 
  else if((myRand >= 12.9594941634) && (myRand < 13.1755810804)){ myKinErg = 25.0;} 
  else if((myRand >= 13.1755810804) && (myRand < 13.392923985)){ myKinErg = 25.1;} 
  else if((myRand >= 13.392923985) && (myRand < 13.6115228772)){ myKinErg = 25.2;} 
  else if((myRand >= 13.6115228772) && (myRand < 13.8312182216)){ myKinErg = 25.3;} 
  else if((myRand >= 13.8312182216) && (myRand < 14.0517973045)){ myKinErg = 25.4;} 
  else if((myRand >= 14.0517973045) && (myRand < 14.2732601257)){ myKinErg = 25.5;} 
  else if((myRand >= 14.2732601257) && (myRand < 14.4956066853)){ myKinErg = 25.6;} 
  else if((myRand >= 14.4956066853) && (myRand < 14.7188369834)){ myKinErg = 25.7;} 
  else if((myRand >= 14.7188369834) && (myRand < 14.9429510198)){ myKinErg = 25.8;} 
  else if((myRand >= 14.9429510198) && (myRand < 15.1679487947)){ myKinErg = 25.9;} 
  else if((myRand >= 15.1679487947) && (myRand < 15.3938303079)){ myKinErg = 26.0;} 
  else if((myRand >= 15.3938303079) && (myRand < 15.6205955596)){ myKinErg = 26.1;} 
  else if((myRand >= 15.6205955596) && (myRand < 15.8482445497)){ myKinErg = 26.2;} 
  else if((myRand >= 15.8482445497) && (myRand < 16.0767772782)){ myKinErg = 26.3;} 
  else if((myRand >= 16.0767772782) && (myRand < 16.3061937451)){ myKinErg = 26.4;} 
  else if((myRand >= 16.3061937451) && (myRand < 16.5364939504)){ myKinErg = 26.5;} 
  else if((myRand >= 16.5364939504) && (myRand < 16.7676778941)){ myKinErg = 26.6;} 
  else if((myRand >= 16.7676778941) && (myRand < 16.9997455762)){ myKinErg = 26.7;} 
  else if((myRand >= 16.9997455762) && (myRand < 17.2326969967)){ myKinErg = 26.8;} 
  else if((myRand >= 17.2326969967) && (myRand < 17.4665321557)){ myKinErg = 26.9;} 
  else if((myRand >= 17.4665321557) && (myRand < 17.701251053)){ myKinErg = 27.0;} 
  else if((myRand >= 17.701251053) && (myRand < 17.9366449456)){ myKinErg = 27.1;} 
  else if((myRand >= 17.9366449456) && (myRand < 18.1725926276)){ myKinErg = 27.2;} 
  else if((myRand >= 18.1725926276) && (myRand < 18.409094099)){ myKinErg = 27.3;} 
  else if((myRand >= 18.409094099) && (myRand < 18.64614936)){ myKinErg = 27.4;} 
  else if((myRand >= 18.64614936) && (myRand < 18.8837584104)){ myKinErg = 27.5;} 
  else if((myRand >= 18.8837584104) && (myRand < 19.1219212502)){ myKinErg = 27.6;} 
  else if((myRand >= 19.1219212502) && (myRand < 19.3606378796)){ myKinErg = 27.7;} 
  else if((myRand >= 19.3606378796) && (myRand < 19.5999082983)){ myKinErg = 27.8;} 
  else if((myRand >= 19.5999082983) && (myRand < 19.8397325066)){ myKinErg = 27.9;} 
  else if((myRand >= 19.8397325066) && (myRand < 20.0801105043)){ myKinErg = 28.0;} 
  else if((myRand >= 20.0801105043) && (myRand < 20.3210422915)){ myKinErg = 28.1;} 
  else if((myRand >= 20.3210422915) && (myRand < 20.5625278682)){ myKinErg = 28.2;} 
  else if((myRand >= 20.5625278682) && (myRand < 20.8045672343)){ myKinErg = 28.3;} 
  else if((myRand >= 20.8045672343) && (myRand < 21.0471603899)){ myKinErg = 28.4;} 
  else if((myRand >= 21.0471603899) && (myRand < 21.2903073349)){ myKinErg = 28.5;} 
  else if((myRand >= 21.2903073349) && (myRand < 21.5340080694)){ myKinErg = 28.6;} 
  else if((myRand >= 21.5340080694) && (myRand < 21.7782625934)){ myKinErg = 28.7;} 
  else if((myRand >= 21.7782625934) && (myRand < 22.0230709068)){ myKinErg = 28.8;} 
  else if((myRand >= 22.0230709068) && (myRand < 22.2681990952)){ myKinErg = 28.9;} 
  else if((myRand >= 22.2681990952) && (myRand < 22.5136015165)){ myKinErg = 29.0;} 
  else if((myRand >= 22.5136015165) && (myRand < 22.759278171)){ myKinErg = 29.1;} 
  else if((myRand >= 22.759278171) && (myRand < 23.0052290584)){ myKinErg = 29.2;} 
  else if((myRand >= 23.0052290584) && (myRand < 23.2514541789)){ myKinErg = 29.3;} 
  else if((myRand >= 23.2514541789) && (myRand < 23.4979535325)){ myKinErg = 29.4;} 
  else if((myRand >= 23.4979535325) && (myRand < 23.7447271191)){ myKinErg = 29.5;} 
  else if((myRand >= 23.7447271191) && (myRand < 23.9917749387)){ myKinErg = 29.6;} 
  else if((myRand >= 23.9917749387) && (myRand < 24.2390969914)){ myKinErg = 29.7;} 
  else if((myRand >= 24.2390969914) && (myRand < 24.4866932771)){ myKinErg = 29.8;} 
  else if((myRand >= 24.4866932771) && (myRand < 24.7345637958)){ myKinErg = 29.9;} 
  else if((myRand >= 24.7345637958) && (myRand < 24.9827085476)){ myKinErg = 30.0;} 
  else if((myRand >= 24.9827085476) && (myRand < 25.2311275325)){ myKinErg = 30.1;} 
  else if((myRand >= 25.2311275325) && (myRand < 25.4798207503)){ myKinErg = 30.2;} 
  else if((myRand >= 25.4798207503) && (myRand < 25.7287882012)){ myKinErg = 30.3;} 
  else if((myRand >= 25.7287882012) && (myRand < 25.9780298852)){ myKinErg = 30.4;} 
  else if((myRand >= 25.9780298852) && (myRand < 26.2275458022)){ myKinErg = 30.5;} 
  else if((myRand >= 26.2275458022) && (myRand < 26.4773266022)){ myKinErg = 30.6;} 
  else if((myRand >= 26.4773266022) && (myRand < 26.7271525595)){ myKinErg = 30.7;} 
  else if((myRand >= 26.7271525595) && (myRand < 26.977023674)){ myKinErg = 30.8;} 
  else if((myRand >= 26.977023674) && (myRand < 27.2269399458)){ myKinErg = 30.9;} 
  else if((myRand >= 27.2269399458) && (myRand < 27.4769013748)){ myKinErg = 31.0;} 
  else if((myRand >= 27.4769013748) && (myRand < 27.7269079611)){ myKinErg = 31.1;} 
  else if((myRand >= 27.7269079611) && (myRand < 27.9769597046)){ myKinErg = 31.2;} 
  else if((myRand >= 27.9769597046) && (myRand < 28.2270566054)){ myKinErg = 31.3;} 
  else if((myRand >= 28.2270566054) && (myRand < 28.4771986635)){ myKinErg = 31.4;} 
  else if((myRand >= 28.4771986635) && (myRand < 28.7273858788)){ myKinErg = 31.5;} 
  else if((myRand >= 28.7273858788) && (myRand < 28.9776182514)){ myKinErg = 31.6;} 
  else if((myRand >= 28.9776182514) && (myRand < 29.2278957812)){ myKinErg = 31.7;} 
  else if((myRand >= 29.2278957812) && (myRand < 29.4782184683)){ myKinErg = 31.8;} 
  else if((myRand >= 29.4782184683) && (myRand < 29.7285863126)){ myKinErg = 31.9;} 
  else if((myRand >= 29.7285863126) && (myRand < 29.9789993142)){ myKinErg = 32.0;} 
  else if((myRand >= 29.9789993142) && (myRand < 30.2294574731)){ myKinErg = 32.1;} 
  else if((myRand >= 30.2294574731) && (myRand < 30.4799607892)){ myKinErg = 32.2;} 
  else if((myRand >= 30.4799607892) && (myRand < 30.7305092626)){ myKinErg = 32.3;} 
  else if((myRand >= 30.7305092626) && (myRand < 30.9810581822)){ myKinErg = 32.4;} 
  else if((myRand >= 30.9810581822) && (myRand < 31.2314696893)){ myKinErg = 32.5;} 
  else if((myRand >= 31.2314696893) && (myRand < 31.4817437839)){ myKinErg = 32.6;} 
  else if((myRand >= 31.4817437839) && (myRand < 31.7318804659)){ myKinErg = 32.7;} 
  else if((myRand >= 31.7318804659) && (myRand < 31.9818797354)){ myKinErg = 32.8;} 
  else if((myRand >= 31.9818797354) && (myRand < 32.2317415924)){ myKinErg = 32.9;} 
  else if((myRand >= 32.2317415924) && (myRand < 32.4814660368)){ myKinErg = 33.0;} 
  else if((myRand >= 32.4814660368) && (myRand < 32.7310530687)){ myKinErg = 33.1;} 
  else if((myRand >= 32.7310530687) && (myRand < 32.9805026881)){ myKinErg = 33.2;} 
  else if((myRand >= 32.9805026881) && (myRand < 33.2298148949)){ myKinErg = 33.3;} 
  else if((myRand >= 33.2298148949) && (myRand < 33.4789896892)){ myKinErg = 33.4;} 
  else if((myRand >= 33.4789896892) && (myRand < 33.7280270709)){ myKinErg = 33.5;} 
  else if((myRand >= 33.7280270709) && (myRand < 33.9769270401)){ myKinErg = 33.6;} 
  else if((myRand >= 33.9769270401) && (myRand < 34.2256895968)){ myKinErg = 33.7;} 
  else if((myRand >= 34.2256895968) && (myRand < 34.4743147409)){ myKinErg = 33.8;} 
  else if((myRand >= 34.4743147409) && (myRand < 34.7228024725)){ myKinErg = 33.9;} 
  else if((myRand >= 34.7228024725) && (myRand < 34.9711527915)){ myKinErg = 34.0;} 
  else if((myRand >= 34.9711527915) && (myRand < 35.2193656981)){ myKinErg = 34.1;} 
  else if((myRand >= 35.2193656981) && (myRand < 35.467377538)){ myKinErg = 34.2;} 
  else if((myRand >= 35.467377538) && (myRand < 35.7151101906)){ myKinErg = 34.3;} 
  else if((myRand >= 35.7151101906) && (myRand < 35.9625636558)){ myKinErg = 34.4;} 
  else if((myRand >= 35.9625636558) && (myRand < 36.2097379337)){ myKinErg = 34.5;} 
  else if((myRand >= 36.2097379337) && (myRand < 36.4566330241)){ myKinErg = 34.6;} 
  else if((myRand >= 36.4566330241) && (myRand < 36.7032489272)){ myKinErg = 34.7;} 
  else if((myRand >= 36.7032489272) && (myRand < 36.9495856429)){ myKinErg = 34.8;} 
  else if((myRand >= 36.9495856429) && (myRand < 37.1956431712)){ myKinErg = 34.9;} 
  else if((myRand >= 37.1956431712) && (myRand < 37.4414215121)){ myKinErg = 35.0;} 
  else if((myRand >= 37.4414215121) && (myRand < 37.6869206657)){ myKinErg = 35.1;} 
  else if((myRand >= 37.6869206657) && (myRand < 37.9321406318)){ myKinErg = 35.2;} 
  else if((myRand >= 37.9321406318) && (myRand < 38.1770814106)){ myKinErg = 35.3;} 
  else if((myRand >= 38.1770814106) && (myRand < 38.421743002)){ myKinErg = 35.4;} 
  else if((myRand >= 38.421743002) && (myRand < 38.6661254061)){ myKinErg = 35.5;} 
  else if((myRand >= 38.6661254061) && (myRand < 38.9102286227)){ myKinErg = 35.6;} 
  else if((myRand >= 38.9102286227) && (myRand < 39.154052652)){ myKinErg = 35.7;} 
  else if((myRand >= 39.154052652) && (myRand < 39.3975974939)){ myKinErg = 35.8;} 
  else if((myRand >= 39.3975974939) && (myRand < 39.6408631484)){ myKinErg = 35.9;} 
  else if((myRand >= 39.6408631484) && (myRand < 39.8837796592)){ myKinErg = 36.0;} 
  else if((myRand >= 39.8837796592) && (myRand < 40.126309862)){ myKinErg = 36.1;} 
  else if((myRand >= 40.126309862) && (myRand < 40.3684537567)){ myKinErg = 36.2;} 
  else if((myRand >= 40.3684537567) && (myRand < 40.6102113434)){ myKinErg = 36.3;} 
  else if((myRand >= 40.6102113434) && (myRand < 40.851582622)){ myKinErg = 36.4;} 
  else if((myRand >= 40.851582622) && (myRand < 41.0925675926)){ myKinErg = 36.5;} 
  else if((myRand >= 41.0925675926) && (myRand < 41.3331662552)){ myKinErg = 36.6;} 
  else if((myRand >= 41.3331662552) && (myRand < 41.5733786097)){ myKinErg = 36.7;} 
  else if((myRand >= 41.5733786097) && (myRand < 41.8132046562)){ myKinErg = 36.8;} 
  else if((myRand >= 41.8132046562) && (myRand < 42.0526443947)){ myKinErg = 36.9;} 
  else if((myRand >= 42.0526443947) && (myRand < 42.2916978251)){ myKinErg = 37.0;} 
  else if((myRand >= 42.2916978251) && (myRand < 42.5303649475)){ myKinErg = 37.1;} 
  else if((myRand >= 42.5303649475) && (myRand < 42.7686457618)){ myKinErg = 37.2;} 
  else if((myRand >= 42.7686457618) && (myRand < 43.0065402681)){ myKinErg = 37.3;} 
  else if((myRand >= 43.0065402681) && (myRand < 43.2440484664)){ myKinErg = 37.4;} 
  else if((myRand >= 43.2440484664) && (myRand < 43.4811703566)){ myKinErg = 37.5;} 
  else if((myRand >= 43.4811703566) && (myRand < 43.7179059388)){ myKinErg = 37.6;} 
  else if((myRand >= 43.7179059388) && (myRand < 43.9542552129)){ myKinErg = 37.7;} 
  else if((myRand >= 43.9542552129) && (myRand < 44.1901510224)){ myKinErg = 37.8;} 
  else if((myRand >= 44.1901510224) && (myRand < 44.4255821744)){ myKinErg = 37.9;} 
  else if((myRand >= 44.4255821744) && (myRand < 44.6605486689)){ myKinErg = 38.0;} 
  else if((myRand >= 44.6605486689) && (myRand < 44.895050506)){ myKinErg = 38.1;} 
  else if((myRand >= 44.895050506) && (myRand < 45.1290876856)){ myKinErg = 38.2;} 
  else if((myRand >= 45.1290876856) && (myRand < 45.3626602078)){ myKinErg = 38.3;} 
  else if((myRand >= 45.3626602078) && (myRand < 45.5957680725)){ myKinErg = 38.4;} 
  else if((myRand >= 45.5957680725) && (myRand < 45.8284112797)){ myKinErg = 38.5;} 
  else if((myRand >= 45.8284112797) && (myRand < 46.0605898295)){ myKinErg = 38.6;} 
  else if((myRand >= 46.0605898295) && (myRand < 46.2923037218)){ myKinErg = 38.7;} 
  else if((myRand >= 46.2923037218) && (myRand < 46.5235529566)){ myKinErg = 38.8;} 
  else if((myRand >= 46.5235529566) && (myRand < 46.754337534)){ myKinErg = 38.9;} 
  else if((myRand >= 46.754337534) && (myRand < 46.9846574539)){ myKinErg = 39.0;} 
  else if((myRand >= 46.9846574539) && (myRand < 47.2145127164)){ myKinErg = 39.1;} 
  else if((myRand >= 47.2145127164) && (myRand < 47.4439033214)){ myKinErg = 39.2;} 
  else if((myRand >= 47.4439033214) && (myRand < 47.6728292689)){ myKinErg = 39.3;} 
  else if((myRand >= 47.6728292689) && (myRand < 47.901290559)){ myKinErg = 39.4;} 
  else if((myRand >= 47.901290559) && (myRand < 48.1292838316)){ myKinErg = 39.5;} 
  else if((myRand >= 48.1292838316) && (myRand < 48.3567575672)){ myKinErg = 39.6;} 
  else if((myRand >= 48.3567575672) && (myRand < 48.5837117657)){ myKinErg = 39.7;} 
  else if((myRand >= 48.5837117657) && (myRand < 48.8101464271)){ myKinErg = 39.8;} 
  else if((myRand >= 48.8101464271) && (myRand < 49.0360615515)){ myKinErg = 39.9;} 
  else if((myRand >= 49.0360615515) && (myRand < 49.2614571388)){ myKinErg = 40.0;} 
  else if((myRand >= 49.2614571388) && (myRand < 49.486333189)){ myKinErg = 40.1;} 
  else if((myRand >= 49.486333189) && (myRand < 49.7106897022)){ myKinErg = 40.2;} 
  else if((myRand >= 49.7106897022) && (myRand < 49.9345266783)){ myKinErg = 40.3;} 
  else if((myRand >= 49.9345266783) && (myRand < 50.1578441173)){ myKinErg = 40.4;} 
  else if((myRand >= 50.1578441173) && (myRand < 50.3806420193)){ myKinErg = 40.5;} 
  else if((myRand >= 50.3806420193) && (myRand < 50.6029203842)){ myKinErg = 40.6;} 
  else if((myRand >= 50.6029203842) && (myRand < 50.824679212)){ myKinErg = 40.7;} 
  else if((myRand >= 50.824679212) && (myRand < 51.0459185028)){ myKinErg = 40.8;} 
  else if((myRand >= 51.0459185028) && (myRand < 51.2666382565)){ myKinErg = 40.9;} 
  else if((myRand >= 51.2666382565) && (myRand < 51.4868384731)){ myKinErg = 41.0;} 
  else if((myRand >= 51.4868384731) && (myRand < 51.7065191527)){ myKinErg = 41.1;} 
  else if((myRand >= 51.7065191527) && (myRand < 51.9256802952)){ myKinErg = 41.2;} 
  else if((myRand >= 51.9256802952) && (myRand < 52.1443123463)){ myKinErg = 41.3;} 
  else if((myRand >= 52.1443123463) && (myRand < 52.3623888479)){ myKinErg = 41.4;} 
  else if((myRand >= 52.3623888479) && (myRand < 52.5799097999)){ myKinErg = 41.5;} 
  else if((myRand >= 52.5799097999) && (myRand < 52.7968752025)){ myKinErg = 41.6;} 
  else if((myRand >= 52.7968752025) && (myRand < 53.0132850556)){ myKinErg = 41.7;} 
  else if((myRand >= 53.0132850556) && (myRand < 53.2291393592)){ myKinErg = 41.8;} 
  else if((myRand >= 53.2291393592) && (myRand < 53.4444381132)){ myKinErg = 41.9;} 
  else if((myRand >= 53.4444381132) && (myRand < 53.6591813178)){ myKinErg = 42.0;} 
  else if((myRand >= 53.6591813178) && (myRand < 53.8733689728)){ myKinErg = 42.1;} 
  else if((myRand >= 53.8733689728) && (myRand < 54.0870010784)){ myKinErg = 42.2;} 
  else if((myRand >= 54.0870010784) && (myRand < 54.3000776344)){ myKinErg = 42.3;} 
  else if((myRand >= 54.3000776344) && (myRand < 54.5125986409)){ myKinErg = 42.4;} 
  else if((myRand >= 54.5125986409) && (myRand < 54.7245640979)){ myKinErg = 42.5;} 
  else if((myRand >= 54.7245640979) && (myRand < 54.9359740054)){ myKinErg = 42.6;} 
  else if((myRand >= 54.9359740054) && (myRand < 55.1468283634)){ myKinErg = 42.7;} 
  else if((myRand >= 55.1468283634) && (myRand < 55.3571271719)){ myKinErg = 42.8;} 
  else if((myRand >= 55.3571271719) && (myRand < 55.5668704309)){ myKinErg = 42.9;} 
  else if((myRand >= 55.5668704309) && (myRand < 55.7760581404)){ myKinErg = 43.0;} 
  else if((myRand >= 55.7760581404) && (myRand < 55.9846804227)){ myKinErg = 43.1;} 
  else if((myRand >= 55.9846804227) && (myRand < 56.1927261118)){ myKinErg = 43.2;} 
  else if((myRand >= 56.1927261118) && (myRand < 56.4001952077)){ myKinErg = 43.3;} 
  else if((myRand >= 56.4001952077) && (myRand < 56.6070877104)){ myKinErg = 43.4;} 
  else if((myRand >= 56.6070877104) && (myRand < 56.8134036199)){ myKinErg = 43.5;} 
  else if((myRand >= 56.8134036199) && (myRand < 57.0191429361)){ myKinErg = 43.6;} 
  else if((myRand >= 57.0191429361) && (myRand < 57.2243056592)){ myKinErg = 43.7;} 
  else if((myRand >= 57.2243056592) && (myRand < 57.4288917891)){ myKinErg = 43.8;} 
  else if((myRand >= 57.4288917891) && (myRand < 57.6329013258)){ myKinErg = 43.9;} 
  else if((myRand >= 57.6329013258) && (myRand < 57.8363342692)){ myKinErg = 44.0;} 
  else if((myRand >= 57.8363342692) && (myRand < 58.0391906195)){ myKinErg = 44.1;} 
  else if((myRand >= 58.0391906195) && (myRand < 58.2414703765)){ myKinErg = 44.2;} 
  else if((myRand >= 58.2414703765) && (myRand < 58.4431735404)){ myKinErg = 44.3;} 
  else if((myRand >= 58.4431735404) && (myRand < 58.644300111)){ myKinErg = 44.4;} 
  else if((myRand >= 58.644300111) && (myRand < 58.8448500884)){ myKinErg = 44.5;} 
  else if((myRand >= 58.8448500884) && (myRand < 59.0448234726)){ myKinErg = 44.6;} 
  else if((myRand >= 59.0448234726) && (myRand < 59.2442202637)){ myKinErg = 44.7;} 
  else if((myRand >= 59.2442202637) && (myRand < 59.4430404615)){ myKinErg = 44.8;} 
  else if((myRand >= 59.4430404615) && (myRand < 59.6412777895)){ myKinErg = 44.9;} 
  else if((myRand >= 59.6412777895) && (myRand < 59.8389292047)){ myKinErg = 45.0;} 
  else if((myRand >= 59.8389292047) && (myRand < 60.0359947069)){ myKinErg = 45.1;} 
  else if((myRand >= 60.0359947069) && (myRand < 60.2324742962)){ myKinErg = 45.2;} 
  else if((myRand >= 60.2324742962) && (myRand < 60.4283679726)){ myKinErg = 45.3;} 
  else if((myRand >= 60.4283679726) && (myRand < 60.6236757361)){ myKinErg = 45.4;} 
  else if((myRand >= 60.6236757361) && (myRand < 60.8183975866)){ myKinErg = 45.5;} 
  else if((myRand >= 60.8183975866) && (myRand < 61.0125335242)){ myKinErg = 45.6;} 
  else if((myRand >= 61.0125335242) && (myRand < 61.2060835489)){ myKinErg = 45.7;} 
  else if((myRand >= 61.2060835489) && (myRand < 61.3990476607)){ myKinErg = 45.8;} 
  else if((myRand >= 61.3990476607) && (myRand < 61.5914258596)){ myKinErg = 45.9;} 
  else if((myRand >= 61.5914258596) && (myRand < 61.7832181455)){ myKinErg = 46.0;} 
  else if((myRand >= 61.7832181455) && (myRand < 61.9744245185)){ myKinErg = 46.1;} 
  else if((myRand >= 61.9744245185) && (myRand < 62.1650449786)){ myKinErg = 46.2;} 
  else if((myRand >= 62.1650449786) && (myRand < 62.3550795258)){ myKinErg = 46.3;} 
  else if((myRand >= 62.3550795258) && (myRand < 62.5445281601)){ myKinErg = 46.4;} 
  else if((myRand >= 62.5445281601) && (myRand < 62.7333908814)){ myKinErg = 46.5;} 
  else if((myRand >= 62.7333908814) && (myRand < 62.9216676898)){ myKinErg = 46.6;} 
  else if((myRand >= 62.9216676898) && (myRand < 63.1093583559)){ myKinErg = 46.7;} 
  else if((myRand >= 63.1093583559) && (myRand < 63.2964628476)){ myKinErg = 46.8;} 
  else if((myRand >= 63.2964628476) && (myRand < 63.4829811649)){ myKinErg = 46.9;} 
  else if((myRand >= 63.4829811649) && (myRand < 63.6689133078)){ myKinErg = 47.0;} 
  else if((myRand >= 63.6689133078) && (myRand < 63.8542592764)){ myKinErg = 47.1;} 
  else if((myRand >= 63.8542592764) && (myRand < 64.0390190706)){ myKinErg = 47.2;} 
  else if((myRand >= 64.0390190706) && (myRand < 64.2231926904)){ myKinErg = 47.3;} 
  else if((myRand >= 64.2231926904) && (myRand < 64.4067801359)){ myKinErg = 47.4;} 
  else if((myRand >= 64.4067801359) && (myRand < 64.5897814069)){ myKinErg = 47.5;} 
  else if((myRand >= 64.5897814069) && (myRand < 64.7721965037)){ myKinErg = 47.6;} 
  else if((myRand >= 64.7721965037) && (myRand < 64.954025426)){ myKinErg = 47.7;} 
  else if((myRand >= 64.954025426) && (myRand < 65.135268174)){ myKinErg = 47.8;} 
  else if((myRand >= 65.135268174) && (myRand < 65.3159247475)){ myKinErg = 47.9;} 
  else if((myRand >= 65.3159247475) && (myRand < 65.4959951468)){ myKinErg = 48.0;} 
  else if((myRand >= 65.4959951468) && (myRand < 65.6754793716)){ myKinErg = 48.1;} 
  else if((myRand >= 65.6754793716) && (myRand < 65.8543774221)){ myKinErg = 48.2;} 
  else if((myRand >= 65.8543774221) && (myRand < 66.0326892982)){ myKinErg = 48.3;} 
  else if((myRand >= 66.0326892982) && (myRand < 66.2104155411)){ myKinErg = 48.4;} 
  else if((myRand >= 66.2104155411) && (myRand < 66.3875622387)){ myKinErg = 48.5;} 
  else if((myRand >= 66.3875622387) && (myRand < 66.5641293912)){ myKinErg = 48.6;} 
  else if((myRand >= 66.5641293912) && (myRand < 66.7401169984)){ myKinErg = 48.7;} 
  else if((myRand >= 66.7401169984) && (myRand < 66.9155250603)){ myKinErg = 48.8;} 
  else if((myRand >= 66.9155250603) && (myRand < 67.0903535771)){ myKinErg = 48.9;} 
  else if((myRand >= 67.0903535771) && (myRand < 67.2646025486)){ myKinErg = 49.0;} 
  else if((myRand >= 67.2646025486) && (myRand < 67.4382719749)){ myKinErg = 49.1;} 
  else if((myRand >= 67.4382719749) && (myRand < 67.6113618559)){ myKinErg = 49.2;} 
  else if((myRand >= 67.6113618559) && (myRand < 67.7838721917)){ myKinErg = 49.3;} 
  else if((myRand >= 67.7838721917) && (myRand < 67.9558029823)){ myKinErg = 49.4;} 
  else if((myRand >= 67.9558029823) && (myRand < 68.1271542276)){ myKinErg = 49.5;} 
  else if((myRand >= 68.1271542276) && (myRand < 68.2979259278)){ myKinErg = 49.6;} 
  else if((myRand >= 68.2979259278) && (myRand < 68.4681180827)){ myKinErg = 49.7;} 
  else if((myRand >= 68.4681180827) && (myRand < 68.6377306923)){ myKinErg = 49.8;} 
  else if((myRand >= 68.6377306923) && (myRand < 68.8067637568)){ myKinErg = 49.9;} 
  else if((myRand >= 68.8067637568) && (myRand < 68.975217276)){ myKinErg = 50.0;} 
  else if((myRand >= 68.975217276) && (myRand < 69.1430912499)){ myKinErg = 50.1;} 
  else if((myRand >= 69.1430912499) && (myRand < 69.3103890421)){ myKinErg = 50.2;} 
  else if((myRand >= 69.3103890421) && (myRand < 69.4771190611)){ myKinErg = 50.3;} 
  else if((myRand >= 69.4771190611) && (myRand < 69.6432813069)){ myKinErg = 50.4;} 
  else if((myRand >= 69.6432813069) && (myRand < 69.8088757795)){ myKinErg = 50.5;} 
  else if((myRand >= 69.8088757795) && (myRand < 69.973902479)){ myKinErg = 50.6;} 
  else if((myRand >= 69.973902479) && (myRand < 70.1383614052)){ myKinErg = 50.7;} 
  else if((myRand >= 70.1383614052) && (myRand < 70.3022525583)){ myKinErg = 50.8;} 
  else if((myRand >= 70.3022525583) && (myRand < 70.4655759382)){ myKinErg = 50.9;} 
  else if((myRand >= 70.4655759382) && (myRand < 70.6283315449)){ myKinErg = 51.0;} 
  else if((myRand >= 70.6283315449) && (myRand < 70.7905193784)){ myKinErg = 51.1;} 
  else if((myRand >= 70.7905193784) && (myRand < 70.9521394387)){ myKinErg = 51.2;} 
  else if((myRand >= 70.9521394387) && (myRand < 71.1131917258)){ myKinErg = 51.3;} 
  else if((myRand >= 71.1131917258) && (myRand < 71.2736762398)){ myKinErg = 51.4;} 
  else if((myRand >= 71.2736762398) && (myRand < 71.4335929806)){ myKinErg = 51.5;} 
  else if((myRand >= 71.4335929806) && (myRand < 71.5929419481)){ myKinErg = 51.6;} 
  else if((myRand >= 71.5929419481) && (myRand < 71.7517231425)){ myKinErg = 51.7;} 
  else if((myRand >= 71.7517231425) && (myRand < 71.9099365637)){ myKinErg = 51.8;} 
  else if((myRand >= 71.9099365637) && (myRand < 72.0675822118)){ myKinErg = 51.9;} 
  else if((myRand >= 72.0675822118) && (myRand < 72.2246676865)){ myKinErg = 52.0;} 
  else if((myRand >= 72.2246676865) && (myRand < 72.3812009045)){ myKinErg = 52.1;} 
  else if((myRand >= 72.3812009045) && (myRand < 72.5371818658)){ myKinErg = 52.2;} 
  else if((myRand >= 72.5371818658) && (myRand < 72.6926105704)){ myKinErg = 52.3;} 
  else if((myRand >= 72.6926105704) && (myRand < 72.8474870182)){ myKinErg = 52.4;} 
  else if((myRand >= 72.8474870182) && (myRand < 73.0018112094)){ myKinErg = 52.5;} 
  else if((myRand >= 73.0018112094) && (myRand < 73.1555831438)){ myKinErg = 52.6;} 
  else if((myRand >= 73.1555831438) && (myRand < 73.3088028215)){ myKinErg = 52.7;} 
  else if((myRand >= 73.3088028215) && (myRand < 73.4614702424)){ myKinErg = 52.8;} 
  else if((myRand >= 73.4614702424) && (myRand < 73.6135854067)){ myKinErg = 52.9;} 
  else if((myRand >= 73.6135854067) && (myRand < 73.7651483142)){ myKinErg = 53.0;} 
  else if((myRand >= 73.7651483142) && (myRand < 73.916158965)){ myKinErg = 53.1;} 
  else if((myRand >= 73.916158965) && (myRand < 74.0666173591)){ myKinErg = 53.2;} 
  else if((myRand >= 74.0666173591) && (myRand < 74.2165234965)){ myKinErg = 53.3;} 
  else if((myRand >= 74.2165234965) && (myRand < 74.3658773771)){ myKinErg = 53.4;} 
  else if((myRand >= 74.3658773771) && (myRand < 74.5146790011)){ myKinErg = 53.5;} 
  else if((myRand >= 74.5146790011) && (myRand < 74.6629283683)){ myKinErg = 53.6;} 
  else if((myRand >= 74.6629283683) && (myRand < 74.8106254788)){ myKinErg = 53.7;} 
  else if((myRand >= 74.8106254788) && (myRand < 74.9577829263)){ myKinErg = 53.8;} 
  else if((myRand >= 74.9577829263) && (myRand < 75.1044062668)){ myKinErg = 53.9;} 
  else if((myRand >= 75.1044062668) && (myRand < 75.2504955005)){ myKinErg = 54.0;} 
  else if((myRand >= 75.2504955005) && (myRand < 75.3960506272)){ myKinErg = 54.1;} 
  else if((myRand >= 75.3960506272) && (myRand < 75.5410716469)){ myKinErg = 54.2;} 
  else if((myRand >= 75.5410716469) && (myRand < 75.6855585598)){ myKinErg = 54.3;} 
  else if((myRand >= 75.6855585598) && (myRand < 75.8295113657)){ myKinErg = 54.4;} 
  else if((myRand >= 75.8295113657) && (myRand < 75.9729300647)){ myKinErg = 54.5;} 
  else if((myRand >= 75.9729300647) && (myRand < 76.1158146567)){ myKinErg = 54.6;} 
  else if((myRand >= 76.1158146567) && (myRand < 76.2581651418)){ myKinErg = 54.7;} 
  else if((myRand >= 76.2581651418) && (myRand < 76.39998152)){ myKinErg = 54.8;} 
  else if((myRand >= 76.39998152) && (myRand < 76.5412637912)){ myKinErg = 54.9;} 
  else if((myRand >= 76.5412637912) && (myRand < 76.6820119555)){ myKinErg = 55.0;} 
  else if((myRand >= 76.6820119555) && (myRand < 76.8222260129)){ myKinErg = 55.1;} 
  else if((myRand >= 76.8222260129) && (myRand < 76.9619059634)){ myKinErg = 55.2;} 
  else if((myRand >= 76.9619059634) && (myRand < 77.1046129213)){ myKinErg = 55.3;} 
  else if((myRand >= 77.1046129213) && (myRand < 77.2468044056)){ myKinErg = 55.4;} 
  else if((myRand >= 77.2468044056) && (myRand < 77.3885220703)){ myKinErg = 55.5;} 
  else if((myRand >= 77.3885220703) && (myRand < 77.5298447146)){ myKinErg = 55.6;} 
  else if((myRand >= 77.5298447146) && (myRand < 77.670908237)){ myKinErg = 55.7;} 
  else if((myRand >= 77.670908237) && (myRand < 77.8119273798)){ myKinErg = 55.8;} 
  else if((myRand >= 77.8119273798) && (myRand < 77.9532656044)){ myKinErg = 55.9;} 
  else if((myRand >= 77.9532656044) && (myRand < 78.0955059916)){ myKinErg = 56.0;} 
  else if((myRand >= 78.0955059916) && (myRand < 78.2394697631)){ myKinErg = 56.1;} 
  else if((myRand >= 78.2394697631) && (myRand < 78.3864348296)){ myKinErg = 56.2;} 
  else if((myRand >= 78.3864348296) && (myRand < 78.53788956)){ myKinErg = 56.3;} 
  else if((myRand >= 78.53788956) && (myRand < 78.6960382948)){ myKinErg = 56.4;} 
  else if((myRand >= 78.6960382948) && (myRand < 78.8630322501)){ myKinErg = 56.5;} 
  else if((myRand >= 78.8630322501) && (myRand < 79.0415386027)){ myKinErg = 56.6;} 
  else if((myRand >= 79.0415386027) && (myRand < 79.2337716847)){ myKinErg = 56.7;} 
  else if((myRand >= 79.2337716847) && (myRand < 79.4416394333)){ myKinErg = 56.8;} 
  else if((myRand >= 79.4416394333) && (myRand < 79.6660287554)){ myKinErg = 56.9;} 
  else if((myRand >= 79.6660287554) && (myRand < 79.906647839)){ myKinErg = 57.0;} 
  else if((myRand >= 79.906647839) && (myRand < 80.1615929182)){ myKinErg = 57.1;} 
  else if((myRand >= 80.1615929182) && (myRand < 80.4277724601)){ myKinErg = 57.2;} 
  else if((myRand >= 80.4277724601) && (myRand < 80.700283882)){ myKinErg = 57.3;} 
  else if((myRand >= 80.700283882) && (myRand < 80.9742226022)){ myKinErg = 57.4;} 
  else if((myRand >= 80.9742226022) && (myRand < 81.2435228958)){ myKinErg = 57.5;} 
  else if((myRand >= 81.2435228958) && (myRand < 81.5034535442)){ myKinErg = 57.6;} 
  else if((myRand >= 81.5034535442) && (myRand < 81.749688075)){ myKinErg = 57.7;} 
  else if((myRand >= 81.749688075) && (myRand < 81.9796259227)){ myKinErg = 57.8;} 
  else if((myRand >= 81.9796259227) && (myRand < 82.1920117623)){ myKinErg = 57.9;} 
  else if((myRand >= 82.1920117623) && (myRand < 82.3870864437)){ myKinErg = 58.0;} 
  else if((myRand >= 82.3870864437) && (myRand < 82.5661158034)){ myKinErg = 58.1;} 
  else if((myRand >= 82.5661158034) && (myRand < 82.7313098195)){ myKinErg = 58.2;} 
  else if((myRand >= 82.7313098195) && (myRand < 82.8849226048)){ myKinErg = 58.3;} 
  else if((myRand >= 82.8849226048) && (myRand < 83.029648414)){ myKinErg = 58.4;} 
  else if((myRand >= 83.029648414) && (myRand < 83.1525518738)){ myKinErg = 58.5;} 
  else if((myRand >= 83.1525518738) && (myRand < 83.2749621108)){ myKinErg = 58.6;} 
  else if((myRand >= 83.2749621108) && (myRand < 83.396879125)){ myKinErg = 58.7;} 
  else if((myRand >= 83.396879125) && (myRand < 83.6279248046)){ myKinErg = 58.8;} 
  else if((myRand >= 83.6279248046) && (myRand < 83.8886181112)){ myKinErg = 58.9;} 
  else if((myRand >= 83.8886181112) && (myRand < 84.1806463433)){ myKinErg = 59.0;} 
  else if((myRand >= 84.1806463433) && (myRand < 84.5035953824)){ myKinErg = 59.1;} 
  else if((myRand >= 84.5035953824) && (myRand < 84.8542755915)){ myKinErg = 59.2;} 
  else if((myRand >= 84.8542755915) && (myRand < 85.22686816)){ myKinErg = 59.3;} 
  else if((myRand >= 85.22686816) && (myRand < 85.6128249916)){ myKinErg = 59.4;} 
  else if((myRand >= 85.6128249916) && (myRand < 86.0027496222)){ myKinErg = 59.5;} 
  else if((myRand >= 86.0027496222) && (myRand < 86.3856571515)){ myKinErg = 59.6;} 
  else if((myRand >= 86.3856571515) && (myRand < 86.7526756205)){ myKinErg = 59.7;} 
  else if((myRand >= 86.7526756205) && (myRand < 87.0951321238)){ myKinErg = 59.8;} 
  else if((myRand >= 87.0951321238) && (myRand < 87.4081432499)){ myKinErg = 59.9;} 
  else if((myRand >= 87.4081432499) && (myRand < 87.6888830929)){ myKinErg = 60.0;} 
  else if((myRand >= 87.6888830929) && (myRand < 87.9374583256)){ myKinErg = 60.1;} 
  else if((myRand >= 87.9374583256) && (myRand < 88.1561076693)){ myKinErg = 60.2;} 
  else if((myRand >= 88.1561076693) && (myRand < 88.3485245716)){ myKinErg = 60.3;} 
  else if((myRand >= 88.3485245716) && (myRand < 88.5191333379)){ myKinErg = 60.4;} 
  else if((myRand >= 88.5191333379) && (myRand < 88.6726432771)){ myKinErg = 60.5;} 
  else if((myRand >= 88.6726432771) && (myRand < 88.8131089676)){ myKinErg = 60.6;} 
  else if((myRand >= 88.8131089676) && (myRand < 88.9443448907)){ myKinErg = 60.7;} 
  else if((myRand >= 88.9443448907) && (myRand < 89.0689670654)){ myKinErg = 60.8;} 
  else if((myRand >= 89.0689670654) && (myRand < 89.1893120521)){ myKinErg = 60.9;} 
  else if((myRand >= 89.1893120521) && (myRand < 89.3067278734)){ myKinErg = 61.0;} 
  else if((myRand >= 89.3067278734) && (myRand < 89.4222685887)){ myKinErg = 61.1;} 
  else if((myRand >= 89.4222685887) && (myRand < 89.5365147702)){ myKinErg = 61.2;} 
  else if((myRand >= 89.5365147702) && (myRand < 89.6498543822)){ myKinErg = 61.3;} 
  else if((myRand >= 89.6498543822) && (myRand < 89.7624951041)){ myKinErg = 61.4;} 
  else if((myRand >= 89.7624951041) && (myRand < 89.8745545557)){ myKinErg = 61.5;} 
  else if((myRand >= 89.8745545557) && (myRand < 89.9860945208)){ myKinErg = 61.6;} 
  else if((myRand >= 89.9860945208) && (myRand < 90.0971445448)){ myKinErg = 61.7;} 
  else if((myRand >= 90.0971445448) && (myRand < 90.2077200264)){ myKinErg = 61.8;} 
  else if((myRand >= 90.2077200264) && (myRand < 90.317827126)){ myKinErg = 61.9;} 
  else if((myRand >= 90.317827126) && (myRand < 90.4243474728)){ myKinErg = 62.0;} 
  else if((myRand >= 90.4243474728) && (myRand < 90.5304177579)){ myKinErg = 62.1;} 
  else if((myRand >= 90.5304177579) && (myRand < 90.6360379812)){ myKinErg = 62.2;} 
  else if((myRand >= 90.6360379812) && (myRand < 90.7412081427)){ myKinErg = 62.3;} 
  else if((myRand >= 90.7412081427) && (myRand < 90.8459282424)){ myKinErg = 62.4;} 
  else if((myRand >= 90.8459282424) && (myRand < 90.9501982804)){ myKinErg = 62.5;} 
  else if((myRand >= 90.9501982804) && (myRand < 91.0540182565)){ myKinErg = 62.6;} 
  else if((myRand >= 91.0540182565) && (myRand < 91.1574035027)){ myKinErg = 62.7;} 
  else if((myRand >= 91.1574035027) && (myRand < 91.2603601517)){ myKinErg = 62.8;} 
  else if((myRand >= 91.2603601517) && (myRand < 91.3628882034)){ myKinErg = 62.9;} 
  else if((myRand >= 91.3628882034) && (myRand < 91.4649876578)){ myKinErg = 63.0;} 
  else if((myRand >= 91.4649876578) && (myRand < 91.566658515)){ myKinErg = 63.1;} 
  else if((myRand >= 91.566658515) && (myRand < 91.6679007749)){ myKinErg = 63.2;} 
  else if((myRand >= 91.6679007749) && (myRand < 91.7687144376)){ myKinErg = 63.3;} 
  else if((myRand >= 91.7687144376) && (myRand < 91.869099503)){ myKinErg = 63.4;} 
  else if((myRand >= 91.869099503) && (myRand < 91.9690559712)){ myKinErg = 63.5;} 
  else if((myRand >= 91.9690559712) && (myRand < 92.0685838421)){ myKinErg = 63.6;} 
  else if((myRand >= 92.0685838421) && (myRand < 92.1676831157)){ myKinErg = 63.7;} 
  else if((myRand >= 92.1676831157) && (myRand < 92.2663537921)){ myKinErg = 63.8;} 
  else if((myRand >= 92.2663537921) && (myRand < 92.3645958712)){ myKinErg = 63.9;} 
  else if((myRand >= 92.3645958712) && (myRand < 92.4624093531)){ myKinErg = 64.0;} 
  else if((myRand >= 92.4624093531) && (myRand < 92.5597942377)){ myKinErg = 64.1;} 
  else if((myRand >= 92.5597942377) && (myRand < 92.6567505251)){ myKinErg = 64.2;} 
  else if((myRand >= 92.6567505251) && (myRand < 92.7532782152)){ myKinErg = 64.3;} 
  else if((myRand >= 92.7532782152) && (myRand < 92.8493773081)){ myKinErg = 64.4;} 
  else if((myRand >= 92.8493773081) && (myRand < 92.9450671375)){ myKinErg = 64.5;} 
  else if((myRand >= 92.9450671375) && (myRand < 93.040349422)){ myKinErg = 64.6;} 
  else if((myRand >= 93.040349422) && (myRand < 93.1352241616)){ myKinErg = 64.7;} 
  else if((myRand >= 93.1352241616) && (myRand < 93.2296913563)){ myKinErg = 64.8;} 
  else if((myRand >= 93.2296913563) && (myRand < 93.3237510061)){ myKinErg = 64.9;} 
  else if((myRand >= 93.3237510061) && (myRand < 93.417403111)){ myKinErg = 65.0;} 
  else if((myRand >= 93.417403111) && (myRand < 93.510647671)){ myKinErg = 65.1;} 
  else if((myRand >= 93.510647671) && (myRand < 93.6034846861)){ myKinErg = 65.2;} 
  else if((myRand >= 93.6034846861) && (myRand < 93.7008449082)){ myKinErg = 65.3;} 
  else if((myRand >= 93.7008449082) && (myRand < 93.7977991747)){ myKinErg = 65.4;} 
  else if((myRand >= 93.7977991747) && (myRand < 93.8943738042)){ myKinErg = 65.5;} 
  else if((myRand >= 93.8943738042) && (myRand < 93.9906097283)){ myKinErg = 65.6;} 
  else if((myRand >= 93.9906097283) && (myRand < 94.0865932369)){ myKinErg = 65.7;} 
  else if((myRand >= 94.0865932369) && (myRand < 94.1824534091)){ myKinErg = 65.8;} 
  else if((myRand >= 94.1824534091) && (myRand < 94.2784194077)){ myKinErg = 65.9;} 
  else if((myRand >= 94.2784194077) && (myRand < 94.3748333891)){ myKinErg = 66.0;} 
  else if((myRand >= 94.3748333891) && (myRand < 94.4722032296)){ myKinErg = 66.1;} 
  else if((myRand >= 94.4722032296) && (myRand < 94.5712788741)){ myKinErg = 66.2;} 
  else if((myRand >= 94.5712788741) && (myRand < 94.6729997383)){ myKinErg = 66.3;} 
  else if((myRand >= 94.6729997383) && (myRand < 94.7786784736)){ myKinErg = 66.4;} 
  else if((myRand >= 94.7786784736) && (myRand < 94.8896509257)){ myKinErg = 66.5;} 
  else if((myRand >= 94.8896509257) && (myRand < 95.007691961)){ myKinErg = 66.6;} 
  else if((myRand >= 95.007691961) && (myRand < 95.1342869837)){ myKinErg = 66.7;} 
  else if((myRand >= 95.1342869837) && (myRand < 95.2709518745)){ myKinErg = 66.8;} 
  else if((myRand >= 95.2709518745) && (myRand < 95.4186322816)){ myKinErg = 66.9;} 
  else if((myRand >= 95.4186322816) && (myRand < 95.5776913055)){ myKinErg = 67.0;} 
  else if((myRand >= 95.5776913055) && (myRand < 95.7475915435)){ myKinErg = 67.1;} 
  else if((myRand >= 95.7475915435) && (myRand < 95.9269356463)){ myKinErg = 67.2;} 
  else if((myRand >= 95.9269356463) && (myRand < 96.1132273611)){ myKinErg = 67.3;} 
  else if((myRand >= 96.1132273611) && (myRand < 96.3035670907)){ myKinErg = 67.4;} 
  else if((myRand >= 96.3035670907) && (myRand < 96.4940755872)){ myKinErg = 67.5;} 
  else if((myRand >= 96.4940755872) && (myRand < 96.6814046362)){ myKinErg = 67.6;} 
  else if((myRand >= 96.6814046362) && (myRand < 96.8619475593)){ myKinErg = 67.7;} 
  else if((myRand >= 96.8619475593) && (myRand < 97.0331799815)){ myKinErg = 67.8;} 
  else if((myRand >= 97.0331799815) && (myRand < 97.1931784786)){ myKinErg = 67.9;} 
  else if((myRand >= 97.1931784786) && (myRand < 97.3411047144)){ myKinErg = 68.0;} 
  else if((myRand >= 97.3411047144) && (myRand < 97.4769667352)){ myKinErg = 68.1;} 
  else if((myRand >= 97.4769667352) && (myRand < 97.6015607601)){ myKinErg = 68.2;} 
  else if((myRand >= 97.6015607601) && (myRand < 97.7161002783)){ myKinErg = 68.3;} 
  else if((myRand >= 97.7161002783) && (myRand < 97.7964342672)){ myKinErg = 68.4;} 
  else if((myRand >= 97.7964342672) && (myRand < 97.8764009529)){ myKinErg = 68.5;} 
  else if((myRand >= 97.8764009529) && (myRand < 97.9560003354)){ myKinErg = 68.6;} 
  else if((myRand >= 97.9560003354) && (myRand < 98.0352324145)){ myKinErg = 68.7;} 
  else if((myRand >= 98.0352324145) && (myRand < 98.1140971904)){ myKinErg = 68.8;} 
  else if((myRand >= 98.1140971904) && (myRand < 98.2098666162)){ myKinErg = 68.9;} 
  else if((myRand >= 98.2098666162) && (myRand < 98.3071965056)){ myKinErg = 69.0;} 
  else if((myRand >= 98.3071965056) && (myRand < 98.4056396459)){ myKinErg = 69.1;} 
  else if((myRand >= 98.4056396459) && (myRand < 98.504638804)){ myKinErg = 69.2;} 
  else if((myRand >= 98.504638804) && (myRand < 98.6034414466)){ myKinErg = 69.3;} 
  else if((myRand >= 98.6034414466) && (myRand < 98.7013983978)){ myKinErg = 69.4;} 
  else if((myRand >= 98.7013983978) && (myRand < 98.7978689142)){ myKinErg = 69.5;} 
  else if((myRand >= 98.7978689142) && (myRand < 98.8922668145)){ myKinErg = 69.6;} 
  else if((myRand >= 98.8922668145) && (myRand < 98.9842222367)){ myKinErg = 69.7;} 
  else if((myRand >= 98.9842222367) && (myRand < 99.073508183)){ myKinErg = 69.8;} 
  else if((myRand >= 99.073508183) && (myRand < 99.160069645)){ myKinErg = 69.9;} 
  else if((myRand >= 99.160069645) && (myRand < 99.2439856721)){ myKinErg = 70.0;} 
  else if((myRand >= 99.2439856721) && (myRand < 99.3254670056)){ myKinErg = 70.1;} 
  else if((myRand >= 99.3254670056) && (myRand < 99.404786497)){ myKinErg = 70.2;} 
  else if((myRand >= 99.404786497) && (myRand < 99.4822305778)){ myKinErg = 70.3;} 
  else if((myRand >= 99.4822305778) && (myRand < 99.5581375322)){ myKinErg = 70.4;} 
  else if((myRand >= 99.5581375322) && (myRand < 99.6327775851)){ myKinErg = 70.5;} 
  else if((myRand >= 99.6327775851) && (myRand < 99.7063825333)){ myKinErg = 70.6;} 
  else if((myRand >= 99.7063825333) && (myRand < 99.7791721737)){ myKinErg = 70.7;} 
  else if((myRand >= 99.7791721737) && (myRand < 99.8512867114)){ myKinErg = 70.8;} 
  else if((myRand >= 99.8512867114) && (myRand < 99.9228367872)){ myKinErg = 70.9;} 
  else if((myRand >= 99.9228367872) && (myRand < 99.9939065239)){ myKinErg = 71.0;} 
  else if((myRand >= 99.9939065239) && (myRand < 100.064580254)){ myKinErg = 71.1;} 
  else if((myRand >= 100.064580254) && (myRand < 100.13490567)){ myKinErg = 71.2;} 
  else if((myRand >= 100.13490567) && (myRand < 100.204882771)){ myKinErg = 71.3;} 
  else if((myRand >= 100.204882771) && (myRand < 100.274511557)){ myKinErg = 71.4;} 
  else if((myRand >= 100.274511557) && (myRand < 100.343792029)){ myKinErg = 71.5;} 
  else if((myRand >= 100.343792029) && (myRand < 100.412737526)){ myKinErg = 71.6;} 
  else if((myRand >= 100.412737526) && (myRand < 100.481352865)){ myKinErg = 71.7;} 
  else if((myRand >= 100.481352865) && (myRand < 100.549638046)){ myKinErg = 71.8;} 
  else if((myRand >= 100.549638046) && (myRand < 100.617593069)){ myKinErg = 71.9;} 
  else if((myRand >= 100.617593069) && (myRand < 100.685217934)){ myKinErg = 72.0;} 
  else if((myRand >= 100.685217934) && (myRand < 100.75251264)){ myKinErg = 72.1;} 
  else if((myRand >= 100.75251264) && (myRand < 100.819477189)){ myKinErg = 72.2;} 
  else if((myRand >= 100.819477189) && (myRand < 100.886111579)){ myKinErg = 72.3;} 
  else if((myRand >= 100.886111579) && (myRand < 100.952415812)){ myKinErg = 72.4;} 
  else if((myRand >= 100.952415812) && (myRand < 101.018389886)){ myKinErg = 72.5;} 
  else if((myRand >= 101.018389886) && (myRand < 101.084033803)){ myKinErg = 72.6;} 
  else if((myRand >= 101.084033803) && (myRand < 101.149347561)){ myKinErg = 72.7;} 
  else if((myRand >= 101.149347561) && (myRand < 101.214331161)){ myKinErg = 72.8;} 
  else if((myRand >= 101.214331161) && (myRand < 101.278984604)){ myKinErg = 72.9;} 
  else if((myRand >= 101.278984604) && (myRand < 101.343307888)){ myKinErg = 73.0;} 
  else if((myRand >= 101.343307888) && (myRand < 101.407301014)){ myKinErg = 73.1;} 
  else if((myRand >= 101.407301014) && (myRand < 101.470963982)){ myKinErg = 73.2;} 
  else if((myRand >= 101.470963982) && (myRand < 101.534296792)){ myKinErg = 73.3;} 
  else if((myRand >= 101.534296792) && (myRand < 101.597315682)){ myKinErg = 73.4;} 
  else if((myRand >= 101.597315682) && (myRand < 101.660021711)){ myKinErg = 73.5;} 
  else if((myRand >= 101.660021711) && (myRand < 101.722414878)){ myKinErg = 73.6;} 
  else if((myRand >= 101.722414878) && (myRand < 101.784495185)){ myKinErg = 73.7;} 
  else if((myRand >= 101.784495185) && (myRand < 101.846262631)){ myKinErg = 73.8;} 
  else if((myRand >= 101.846262631) && (myRand < 101.907717216)){ myKinErg = 73.9;} 
  else if((myRand >= 101.907717216) && (myRand < 101.96885894)){ myKinErg = 74.0;} 
  else if((myRand >= 101.96885894) && (myRand < 102.029687802)){ myKinErg = 74.1;} 
  else if((myRand >= 102.029687802) && (myRand < 102.090203804)){ myKinErg = 74.2;} 
  else if((myRand >= 102.090203804) && (myRand < 102.150406945)){ myKinErg = 74.3;} 
  else if((myRand >= 102.150406945) && (myRand < 102.210297224)){ myKinErg = 74.4;} 
  else if((myRand >= 102.210297224) && (myRand < 102.269874643)){ myKinErg = 74.5;} 
  else if((myRand >= 102.269874643) && (myRand < 102.3291392)){ myKinErg = 74.6;} 
  else if((myRand >= 102.3291392) && (myRand < 102.388090897)){ myKinErg = 74.7;} 
  else if((myRand >= 102.388090897) && (myRand < 102.446729732)){ myKinErg = 74.8;} 
  else if((myRand >= 102.446729732) && (myRand < 102.505055707)){ myKinErg = 74.9;} 
  else if((myRand >= 102.505055707) && (myRand < 102.56306882)){ myKinErg = 75.0;} 
  else if((myRand >= 102.56306882) && (myRand < 102.62077142)){ myKinErg = 75.1;} 
  else if((myRand >= 102.62077142) && (myRand < 102.678177586)){ myKinErg = 75.2;} 
  else if((myRand >= 102.678177586) && (myRand < 102.735287321)){ myKinErg = 75.3;} 
  else if((myRand >= 102.735287321) && (myRand < 102.792100622)){ myKinErg = 75.4;} 
  else if((myRand >= 102.792100622) && (myRand < 102.848617492)){ myKinErg = 75.5;} 
  else if((myRand >= 102.848617492) && (myRand < 102.904837928)){ myKinErg = 75.6;} 
  else if((myRand >= 102.904837928) && (myRand < 102.960761932)){ myKinErg = 75.7;} 
  else if((myRand >= 102.960761932) && (myRand < 103.016389504)){ myKinErg = 75.8;} 
  else if((myRand >= 103.016389504) && (myRand < 103.071720643)){ myKinErg = 75.9;} 
  else if((myRand >= 103.071720643) && (myRand < 103.126755349)){ myKinErg = 76.0;} 
  else if((myRand >= 103.126755349) && (myRand < 103.181493623)){ myKinErg = 76.1;} 
  else if((myRand >= 103.181493623) && (myRand < 103.235935464)){ myKinErg = 76.2;} 
  else if((myRand >= 103.235935464) && (myRand < 103.290080873)){ myKinErg = 76.3;} 
  else if((myRand >= 103.290080873) && (myRand < 103.343929849)){ myKinErg = 76.4;} 
  else if((myRand >= 103.343929849) && (myRand < 103.397482393)){ myKinErg = 76.5;} 
  else if((myRand >= 103.397482393) && (myRand < 103.450738504)){ myKinErg = 76.6;} 
  else if((myRand >= 103.450738504) && (myRand < 103.503698183)){ myKinErg = 76.7;} 
  else if((myRand >= 103.503698183) && (myRand < 103.556361429)){ myKinErg = 76.8;} 
  else if((myRand >= 103.556361429) && (myRand < 103.608733643)){ myKinErg = 76.9;} 
  else if((myRand >= 103.608733643) && (myRand < 103.660824988)){ myKinErg = 77.0;} 
  else if((myRand >= 103.660824988) && (myRand < 103.712635465)){ myKinErg = 77.1;} 
  else if((myRand >= 103.712635465) && (myRand < 103.764165075)){ myKinErg = 77.2;} 
  else if((myRand >= 103.764165075) && (myRand < 103.815413816)){ myKinErg = 77.3;} 
  else if((myRand >= 103.815413816) && (myRand < 103.86638169)){ myKinErg = 77.4;} 
  else if((myRand >= 103.86638169) && (myRand < 103.917068695)){ myKinErg = 77.5;} 
  else if((myRand >= 103.917068695) && (myRand < 103.967474832)){ myKinErg = 77.6;} 
  else if((myRand >= 103.967474832) && (myRand < 104.017600102)){ myKinErg = 77.7;} 
  else if((myRand >= 104.017600102) && (myRand < 104.067444503)){ myKinErg = 77.8;} 
  else if((myRand >= 104.067444503) && (myRand < 104.117008036)){ myKinErg = 77.9;} 
  else if((myRand >= 104.117008036) && (myRand < 104.166290701)){ myKinErg = 78.0;} 
  else if((myRand >= 104.166290701) && (myRand < 104.215292499)){ myKinErg = 78.1;} 
  else if((myRand >= 104.215292499) && (myRand < 104.264013428)){ myKinErg = 78.2;} 
  else if((myRand >= 104.264013428) && (myRand < 104.312453489)){ myKinErg = 78.3;} 
  else if((myRand >= 104.312453489) && (myRand < 104.360612682)){ myKinErg = 78.4;} 
  else if((myRand >= 104.360612682) && (myRand < 104.408491008)){ myKinErg = 78.5;} 
  else if((myRand >= 104.408491008) && (myRand < 104.456088465)){ myKinErg = 78.6;} 
  else if((myRand >= 104.456088465) && (myRand < 104.503413162)){ myKinErg = 78.7;} 
  else if((myRand >= 104.503413162) && (myRand < 104.550471707)){ myKinErg = 78.8;} 
  else if((myRand >= 104.550471707) && (myRand < 104.5972641)){ myKinErg = 78.9;} 
  else if((myRand >= 104.5972641) && (myRand < 104.643790339)){ myKinErg = 79.0;} 
  else if((myRand >= 104.643790339) && (myRand < 104.690050426)){ myKinErg = 79.1;} 
  else if((myRand >= 104.690050426) && (myRand < 104.736044361)){ myKinErg = 79.2;} 
  else if((myRand >= 104.736044361) && (myRand < 104.781772143)){ myKinErg = 79.3;} 
  else if((myRand >= 104.781772143) && (myRand < 104.827233772)){ myKinErg = 79.4;} 
  else if((myRand >= 104.827233772) && (myRand < 104.872429249)){ myKinErg = 79.5;} 
  else if((myRand >= 104.872429249) && (myRand < 104.917358573)){ myKinErg = 79.6;} 
  else if((myRand >= 104.917358573) && (myRand < 104.962021744)){ myKinErg = 79.7;} 
  else if((myRand >= 104.962021744) && (myRand < 105.006418763)){ myKinErg = 79.8;} 
  else if((myRand >= 105.006418763) && (myRand < 105.050549629)){ myKinErg = 79.9;} 
  else if((myRand >= 105.050549629) && (myRand < 105.094414342)){ myKinErg = 80.0;} 
  else if((myRand >= 105.094414342) && (myRand < 105.138012903)){ myKinErg = 80.1;} 
  else if((myRand >= 105.138012903) && (myRand < 105.181345311)){ myKinErg = 80.2;} 
  else if((myRand >= 105.181345311) && (myRand < 105.224411567)){ myKinErg = 80.3;} 
  else if((myRand >= 105.224411567) && (myRand < 105.26721167)){ myKinErg = 80.4;} 
  else if((myRand >= 105.26721167) && (myRand < 105.309756108)){ myKinErg = 80.5;} 
  else if((myRand >= 105.309756108) && (myRand < 105.352048281)){ myKinErg = 80.6;} 
  else if((myRand >= 105.352048281) && (myRand < 105.394088191)){ myKinErg = 80.7;} 
  else if((myRand >= 105.394088191) && (myRand < 105.435875836)){ myKinErg = 80.8;} 
  else if((myRand >= 105.435875836) && (myRand < 105.477411218)){ myKinErg = 80.9;} 
  else if((myRand >= 105.477411218) && (myRand < 105.518694336)){ myKinErg = 81.0;} 
  else if((myRand >= 105.518694336) && (myRand < 105.559725189)){ myKinErg = 81.1;} 
  else if((myRand >= 105.559725189) && (myRand < 105.600503779)){ myKinErg = 81.2;} 
  else if((myRand >= 105.600503779) && (myRand < 105.641030104)){ myKinErg = 81.3;} 
  else if((myRand >= 105.641030104) && (myRand < 105.681304166)){ myKinErg = 81.4;} 
  else if((myRand >= 105.681304166) && (myRand < 105.721325963)){ myKinErg = 81.5;} 
  else if((myRand >= 105.721325963) && (myRand < 105.761095496)){ myKinErg = 81.6;} 
  else if((myRand >= 105.761095496) && (myRand < 105.800612766)){ myKinErg = 81.7;} 
  else if((myRand >= 105.800612766) && (myRand < 105.839877771)){ myKinErg = 81.8;} 
  else if((myRand >= 105.839877771) && (myRand < 105.878890513)){ myKinErg = 81.9;} 
  else if((myRand >= 105.878890513) && (myRand < 105.91765099)){ myKinErg = 82.0;} 
  else if((myRand >= 105.91765099) && (myRand < 105.956159203)){ myKinErg = 82.1;} 
  else if((myRand >= 105.956159203) && (myRand < 105.994415153)){ myKinErg = 82.2;} 
  else if((myRand >= 105.994415153) && (myRand < 106.032431393)){ myKinErg = 82.3;} 
  else if((myRand >= 106.032431393) && (myRand < 106.070208459)){ myKinErg = 82.4;} 
  else if((myRand >= 106.070208459) && (myRand < 106.107746351)){ myKinErg = 82.5;} 
  else if((myRand >= 106.107746351) && (myRand < 106.145045068)){ myKinErg = 82.6;} 
  else if((myRand >= 106.145045068) && (myRand < 106.18210461)){ myKinErg = 82.7;} 
  else if((myRand >= 106.18210461) && (myRand < 106.218924978)){ myKinErg = 82.8;} 
  else if((myRand >= 106.218924978) && (myRand < 106.255506172)){ myKinErg = 82.9;} 
  else if((myRand >= 106.255506172) && (myRand < 106.291848191)){ myKinErg = 83.0;} 
  else if((myRand >= 106.291848191) && (myRand < 106.327951035)){ myKinErg = 83.1;} 
  else if((myRand >= 106.327951035) && (myRand < 106.363814706)){ myKinErg = 83.2;} 
  else if((myRand >= 106.363814706) && (myRand < 106.399439201)){ myKinErg = 83.3;} 
  else if((myRand >= 106.399439201) && (myRand < 106.434824522)){ myKinErg = 83.4;} 
  else if((myRand >= 106.434824522) && (myRand < 106.469970669)){ myKinErg = 83.5;} 
  else if((myRand >= 106.469970669) && (myRand < 106.504877641)){ myKinErg = 83.6;} 
  else if((myRand >= 106.504877641) && (myRand < 106.539545438)){ myKinErg = 83.7;} 
  else if((myRand >= 106.539545438) && (myRand < 106.573974061)){ myKinErg = 83.8;} 
  else if((myRand >= 106.573974061) && (myRand < 106.60816351)){ myKinErg = 83.9;} 
  else if((myRand >= 106.60816351) && (myRand < 106.642115796)){ myKinErg = 84.0;} 
  else if((myRand >= 106.642115796) && (myRand < 106.675841229)){ myKinErg = 84.1;} 
  else if((myRand >= 106.675841229) && (myRand < 106.709339809)){ myKinErg = 84.2;} 
  else if((myRand >= 106.709339809) && (myRand < 106.742611537)){ myKinErg = 84.3;} 
  else if((myRand >= 106.742611537) && (myRand < 106.775656412)){ myKinErg = 84.4;} 
  else if((myRand >= 106.775656412) && (myRand < 106.808474434)){ myKinErg = 84.5;} 
  else if((myRand >= 106.808474434) && (myRand < 106.841065604)){ myKinErg = 84.6;} 
  else if((myRand >= 106.841065604) && (myRand < 106.873429921)){ myKinErg = 84.7;} 
  else if((myRand >= 106.873429921) && (myRand < 106.905567385)){ myKinErg = 84.8;} 
  else if((myRand >= 106.905567385) && (myRand < 106.937477996)){ myKinErg = 84.9;} 
  else if((myRand >= 106.937477996) && (myRand < 106.969161755)){ myKinErg = 85.0;} 
  else if((myRand >= 106.969161755) && (myRand < 107.000618661)){ myKinErg = 85.1;} 
  else if((myRand >= 107.000618661) && (myRand < 107.031848715)){ myKinErg = 85.2;} 
  else if((myRand >= 107.031848715) && (myRand < 107.062851915)){ myKinErg = 85.3;} 
  else if((myRand >= 107.062851915) && (myRand < 107.093628263)){ myKinErg = 85.4;} 
  else if((myRand >= 107.093628263) && (myRand < 107.124177759)){ myKinErg = 85.5;} 
  else if((myRand >= 107.124177759) && (myRand < 107.154500401)){ myKinErg = 85.6;} 
  else if((myRand >= 107.154500401) && (myRand < 107.184596191)){ myKinErg = 85.7;} 
  else if((myRand >= 107.184596191) && (myRand < 107.214469385)){ myKinErg = 85.8;} 
  else if((myRand >= 107.214469385) && (myRand < 107.244127314)){ myKinErg = 85.9;} 
  else if((myRand >= 107.244127314) && (myRand < 107.273569978)){ myKinErg = 86.0;} 
  else if((myRand >= 107.273569978) && (myRand < 107.302797377)){ myKinErg = 86.1;} 
  else if((myRand >= 107.302797377) && (myRand < 107.331809512)){ myKinErg = 86.2;} 
  else if((myRand >= 107.331809512) && (myRand < 107.360606381)){ myKinErg = 86.3;} 
  else if((myRand >= 107.360606381) && (myRand < 107.389187986)){ myKinErg = 86.4;} 
  else if((myRand >= 107.389187986) && (myRand < 107.417554325)){ myKinErg = 86.5;} 
  else if((myRand >= 107.417554325) && (myRand < 107.4457054)){ myKinErg = 86.6;} 
  else if((myRand >= 107.4457054) && (myRand < 107.473641209)){ myKinErg = 86.7;} 
  else if((myRand >= 107.473641209) && (myRand < 107.501361754)){ myKinErg = 86.8;} 
  else if((myRand >= 107.501361754) && (myRand < 107.528867034)){ myKinErg = 86.9;} 
  else if((myRand >= 107.528867034) && (myRand < 107.556157049)){ myKinErg = 87.0;} 
  else if((myRand >= 107.556157049) && (myRand < 107.583231799)){ myKinErg = 87.1;} 
  else if((myRand >= 107.583231799) && (myRand < 107.610091284)){ myKinErg = 87.2;} 
  else if((myRand >= 107.610091284) && (myRand < 107.636735505)){ myKinErg = 87.3;} 
  else if((myRand >= 107.636735505) && (myRand < 107.66316446)){ myKinErg = 87.4;} 
  else if((myRand >= 107.66316446) && (myRand < 107.68937815)){ myKinErg = 87.5;} 
  else if((myRand >= 107.68937815) && (myRand < 107.715382798)){ myKinErg = 87.6;} 
  else if((myRand >= 107.715382798) && (myRand < 107.74118307)){ myKinErg = 87.7;} 
  else if((myRand >= 107.74118307) && (myRand < 107.766778965)){ myKinErg = 87.8;} 
  else if((myRand >= 107.766778965) && (myRand < 107.792170484)){ myKinErg = 87.9;} 
  else if((myRand >= 107.792170484) && (myRand < 107.817357627)){ myKinErg = 88.0;} 
  else if((myRand >= 107.817357627) && (myRand < 107.842340394)){ myKinErg = 88.1;} 
  else if((myRand >= 107.842340394) && (myRand < 107.867118785)){ myKinErg = 88.2;} 
  else if((myRand >= 107.867118785) && (myRand < 107.8916928)){ myKinErg = 88.3;} 
  else if((myRand >= 107.8916928) && (myRand < 107.916062438)){ myKinErg = 88.4;} 
  else if((myRand >= 107.916062438) && (myRand < 107.940227701)){ myKinErg = 88.5;} 
  else if((myRand >= 107.940227701) && (myRand < 107.964188587)){ myKinErg = 88.6;} 
  else if((myRand >= 107.964188587) && (myRand < 107.987945097)){ myKinErg = 88.7;} 
  else if((myRand >= 107.987945097) && (myRand < 108.011497231)){ myKinErg = 88.8;} 
  else if((myRand >= 108.011497231) && (myRand < 108.034844989)){ myKinErg = 88.9;} 
  else if((myRand >= 108.034844989) && (myRand < 108.057988371)){ myKinErg = 89.0;} 
  else if((myRand >= 108.057988371) && (myRand < 108.080927376)){ myKinErg = 89.1;} 
  else if((myRand >= 108.080927376) && (myRand < 108.103662006)){ myKinErg = 89.2;} 
  else if((myRand >= 108.103662006) && (myRand < 108.126192259)){ myKinErg = 89.3;} 
  else if((myRand >= 108.126192259) && (myRand < 108.148526066)){ myKinErg = 89.4;} 
  else if((myRand >= 108.148526066) && (myRand < 108.170665722)){ myKinErg = 89.5;} 
  else if((myRand >= 108.170665722) && (myRand < 108.192611228)){ myKinErg = 89.6;} 
  else if((myRand >= 108.192611228) && (myRand < 108.214362582)){ myKinErg = 89.7;} 
  else if((myRand >= 108.214362582) && (myRand < 108.235919786)){ myKinErg = 89.8;} 
  else if((myRand >= 108.235919786) && (myRand < 108.257282839)){ myKinErg = 89.9;} 
  else if((myRand >= 108.257282839) && (myRand < 108.278451741)){ myKinErg = 90.0;} 
  else if((myRand >= 108.278451741) && (myRand < 108.299426492)){ myKinErg = 90.1;} 
  else if((myRand >= 108.299426492) && (myRand < 108.320207092)){ myKinErg = 90.2;} 
  else if((myRand >= 108.320207092) && (myRand < 108.340793541)){ myKinErg = 90.3;} 
  else if((myRand >= 108.340793541) && (myRand < 108.36118584)){ myKinErg = 90.4;} 
  else if((myRand >= 108.36118584) && (myRand < 108.381383988)){ myKinErg = 90.5;} 
  else if((myRand >= 108.381383988) && (myRand < 108.401387985)){ myKinErg = 90.6;} 
  else if((myRand >= 108.401387985) && (myRand < 108.421197831)){ myKinErg = 90.7;} 
  else if((myRand >= 108.421197831) && (myRand < 108.440813526)){ myKinErg = 90.8;} 
  else if((myRand >= 108.440813526) && (myRand < 108.46023507)){ myKinErg = 90.9;} 
  else if((myRand >= 108.46023507) && (myRand < 108.479462464)){ myKinErg = 91.0;} 
  else if((myRand >= 108.479462464) && (myRand < 108.498495707)){ myKinErg = 91.1;} 
  else if((myRand >= 108.498495707) && (myRand < 108.5173442)){ myKinErg = 91.2;} 
  else if((myRand >= 108.5173442) && (myRand < 108.53600814)){ myKinErg = 91.3;} 
  else if((myRand >= 108.53600814) && (myRand < 108.554487526)){ myKinErg = 91.4;} 
  else if((myRand >= 108.554487526) && (myRand < 108.572782358)){ myKinErg = 91.5;} 
  else if((myRand >= 108.572782358) && (myRand < 108.590892637)){ myKinErg = 91.6;} 
  else if((myRand >= 108.590892637) && (myRand < 108.608818362)){ myKinErg = 91.7;} 
  else if((myRand >= 108.608818362) && (myRand < 108.626559534)){ myKinErg = 91.8;} 
  else if((myRand >= 108.626559534) && (myRand < 108.644116152)){ myKinErg = 91.9;} 
  else if((myRand >= 108.644116152) && (myRand < 108.661488217)){ myKinErg = 92.0;} 
  else if((myRand >= 108.661488217) && (myRand < 108.678675728)){ myKinErg = 92.1;} 
  else if((myRand >= 108.678675728) && (myRand < 108.695678685)){ myKinErg = 92.2;} 
  else if((myRand >= 108.695678685) && (myRand < 108.712497089)){ myKinErg = 92.3;} 
  else if((myRand >= 108.712497089) && (myRand < 108.729130939)){ myKinErg = 92.4;} 
  else if((myRand >= 108.729130939) && (myRand < 108.745580236)){ myKinErg = 92.5;} 
  else if((myRand >= 108.745580236) && (myRand < 108.761844979)){ myKinErg = 92.6;} 
  else if((myRand >= 108.761844979) && (myRand < 108.777925168)){ myKinErg = 92.7;} 
  else if((myRand >= 108.777925168) && (myRand < 108.793820804)){ myKinErg = 92.8;} 
  else if((myRand >= 108.793820804) && (myRand < 108.80953354)){ myKinErg = 92.9;} 
  else if((myRand >= 108.80953354) && (myRand < 108.825070726)){ myKinErg = 93.0;} 
  else if((myRand >= 108.825070726) && (myRand < 108.840432364)){ myKinErg = 93.1;} 
  else if((myRand >= 108.840432364) && (myRand < 108.855618451)){ myKinErg = 93.2;} 
  else if((myRand >= 108.855618451) && (myRand < 108.870628989)){ myKinErg = 93.3;} 
  else if((myRand >= 108.870628989) && (myRand < 108.885463978)){ myKinErg = 93.4;} 
  else if((myRand >= 108.885463978) && (myRand < 108.900123417)){ myKinErg = 93.5;} 
  else if((myRand >= 108.900123417) && (myRand < 108.914607307)){ myKinErg = 93.6;} 
  else if((myRand >= 108.914607307) && (myRand < 108.928915647)){ myKinErg = 93.7;} 
  else if((myRand >= 108.928915647) && (myRand < 108.943048438)){ myKinErg = 93.8;} 
  else if((myRand >= 108.943048438) && (myRand < 108.957005679)){ myKinErg = 93.9;} 
  else if((myRand >= 108.957005679) && (myRand < 108.970787371)){ myKinErg = 94.0;} 
  else if((myRand >= 108.970787371) && (myRand < 108.984393513)){ myKinErg = 94.1;} 
  else if((myRand >= 108.984393513) && (myRand < 108.997824106)){ myKinErg = 94.2;} 
  else if((myRand >= 108.997824106) && (myRand < 109.011079149)){ myKinErg = 94.3;} 
  else if((myRand >= 109.011079149) && (myRand < 109.024158643)){ myKinErg = 94.4;} 
  else if((myRand >= 109.024158643) && (myRand < 109.037062587)){ myKinErg = 94.5;} 
  else if((myRand >= 109.037062587) && (myRand < 109.049790982)){ myKinErg = 94.6;} 
  else if((myRand >= 109.049790982) && (myRand < 109.062347102)){ myKinErg = 94.7;} 
  else if((myRand >= 109.062347102) && (myRand < 109.074736118)){ myKinErg = 94.8;} 
  else if((myRand >= 109.074736118) && (myRand < 109.086958029)){ myKinErg = 94.9;} 
  else if((myRand >= 109.086958029) && (myRand < 109.099012835)){ myKinErg = 95.0;} 
  else if((myRand >= 109.099012835) && (myRand < 109.110900537)){ myKinErg = 95.1;} 
  else if((myRand >= 109.110900537) && (myRand < 109.122621135)){ myKinErg = 95.2;} 
  else if((myRand >= 109.122621135) && (myRand < 109.134174628)){ myKinErg = 95.3;} 
  else if((myRand >= 109.134174628) && (myRand < 109.145561016)){ myKinErg = 95.4;} 
  else if((myRand >= 109.145561016) && (myRand < 109.1567803)){ myKinErg = 95.5;} 
  else if((myRand >= 109.1567803) && (myRand < 109.16783248)){ myKinErg = 95.6;} 
  else if((myRand >= 109.16783248) && (myRand < 109.178717555)){ myKinErg = 95.7;} 
  else if((myRand >= 109.178717555) && (myRand < 109.189435525)){ myKinErg = 95.8;} 
  else if((myRand >= 109.189435525) && (myRand < 109.199986391)){ myKinErg = 95.9;} 
  else if((myRand >= 109.199986391) && (myRand < 109.210370153)){ myKinErg = 96.0;} 
  else if((myRand >= 109.210370153) && (myRand < 109.22058681)){ myKinErg = 96.1;} 
  else if((myRand >= 109.22058681) && (myRand < 109.230636362)){ myKinErg = 96.2;} 
  else if((myRand >= 109.230636362) && (myRand < 109.24051881)){ myKinErg = 96.3;} 
  else if((myRand >= 109.24051881) && (myRand < 109.250234154)){ myKinErg = 96.4;} 
  else if((myRand >= 109.250234154) && (myRand < 109.25978708)){ myKinErg = 96.5;} 
  else if((myRand >= 109.25978708) && (myRand < 109.269180819)){ myKinErg = 96.6;} 
  else if((myRand >= 109.269180819) && (myRand < 109.278415374)){ myKinErg = 96.7;} 
  else if((myRand >= 109.278415374) && (myRand < 109.287490742)){ myKinErg = 96.8;} 
  else if((myRand >= 109.287490742) && (myRand < 109.296406925)){ myKinErg = 96.9;} 
  else if((myRand >= 109.296406925) && (myRand < 109.305163922)){ myKinErg = 97.0;} 
  else if((myRand >= 109.305163922) && (myRand < 109.313761733)){ myKinErg = 97.1;} 
  else if((myRand >= 109.313761733) && (myRand < 109.322200359)){ myKinErg = 97.2;} 
  else if((myRand >= 109.322200359) && (myRand < 109.330479799)){ myKinErg = 97.3;} 
  else if((myRand >= 109.330479799) && (myRand < 109.338600053)){ myKinErg = 97.4;} 
  else if((myRand >= 109.338600053) && (myRand < 109.346561121)){ myKinErg = 97.5;} 
  else if((myRand >= 109.346561121) && (myRand < 109.354363004)){ myKinErg = 97.6;} 
  else if((myRand >= 109.354363004) && (myRand < 109.362005701)){ myKinErg = 97.7;} 
  else if((myRand >= 109.362005701) && (myRand < 109.369489212)){ myKinErg = 97.8;} 
  else if((myRand >= 109.369489212) && (myRand < 109.376813538)){ myKinErg = 97.9;} 
  else if((myRand >= 109.376813538) && (myRand < 109.383978677)){ myKinErg = 98.0;} 
  else if((myRand >= 109.383978677) && (myRand < 109.390984632)){ myKinErg = 98.1;} 
  else if((myRand >= 109.390984632) && (myRand < 109.3978314)){ myKinErg = 98.2;} 
  else if((myRand >= 109.3978314) && (myRand < 109.404524892)){ myKinErg = 98.3;} 
  else if((myRand >= 109.404524892) && (myRand < 109.411066622)){ myKinErg = 98.4;} 
  else if((myRand >= 109.411066622) && (myRand < 109.41745659)){ myKinErg = 98.5;} 
  else if((myRand >= 109.41745659) && (myRand < 109.423694797)){ myKinErg = 98.6;} 
  else if((myRand >= 109.423694797) && (myRand < 109.429781243)){ myKinErg = 98.7;} 
  else if((myRand >= 109.429781243) && (myRand < 109.435715927)){ myKinErg = 98.8;} 
  else if((myRand >= 109.435715927) && (myRand < 109.441498849)){ myKinErg = 98.9;} 
  else if((myRand >= 109.441498849) && (myRand < 109.44713001)){ myKinErg = 99.0;} 
  else if((myRand >= 109.44713001) && (myRand < 109.452609409)){ myKinErg = 99.1;} 
  else if((myRand >= 109.452609409) && (myRand < 109.457937047)){ myKinErg = 99.2;} 
  else if((myRand >= 109.457937047) && (myRand < 109.463112924)){ myKinErg = 99.3;} 
  else if((myRand >= 109.463112924) && (myRand < 109.468137038)){ myKinErg = 99.4;} 
  else if((myRand >= 109.468137038) && (myRand < 109.473009391)){ myKinErg = 99.5;} 
  else if((myRand >= 109.473009391) && (myRand < 109.477729983)){ myKinErg = 99.6;} 
  else if(myRand >= 109.477729983){ myKinErg = 0.0;} 

  // Quelle
  fParticleGun->SetParticleEnergy(myKinErg*keV);
  
  // Quadrat, welches dann Überprüft, ob Zahl innerhalb Kreisscheibe ist
  //G4double r = 0.5, x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r);
  //while((x0*x0)+(y0*y0)>r){ x0 = (2*r*G4UniformRand())-r, y0 = (2*r*G4UniformRand()-r); }
  //G4double z_pos = ((0.05*G4UniformRand())-0.025);
  G4double z0 = 46.2; 

  // Zufallszahl mit Phi und R
  G4double radius_quelle = 1;
  G4double r_position = radius_quelle *sqrt(G4UniformRand());
  G4double phi = CLHEP::twopi*G4UniformRand();
  G4double x0 = (r_position * sin(phi))  ;
  G4double y0 = (r_position * cos(phi)) ;

  G4double dasoderdas = G4UniformRand(), grenze = 0.0; // grenze Zufallsstreuung, 1-grenze Festgelegt
  //G4cout << dasoderdas << G4endl;
  if(dasoderdas < grenze){
    // Zufallszahl und jeder Winkel ist möglich
    G4double ges_wink = 3.369, b = 1.685, a = -0.84; 
    //G4double ges_wink = 3.055*deg, b = 1.528, a = -0.764; 
    G4double ax = (a*x0)+b, ay = (a*y0)+b;
    G4double alphax = ax - (ges_wink*G4UniformRand()), alphay = ay - (ges_wink*G4UniformRand());
    //G4cout << "1:  " << alphax << " " << alphay << G4endl; 
    G4ThreeVector direction = (G4ThreeVector(std::tan(alphax*deg), std::tan(alphay*deg), -1));
    //G4cout << x0 << " " << alpx << ", " << y0 << " " << alpy << G4endl;
    //G4double alpx = G4UniformRand()*2*alphax-alphax, alpy = G4UniformRand()*2*alphay-alphay
    fParticleGun->SetParticlePosition(G4ThreeVector(x0*mm,y0*mm,z0*mm)); 
    fParticleGun->SetParticleMomentumDirection(direction);    
    fParticleGun->GeneratePrimaryVertex(anEvent); 
  }
  else if(dasoderdas >= grenze){
    // Feste Richtung 
    G4double b = 0, a = 0.8425;
    //G4double b = 0, a = 2.292;
    G4double alphax = (a*x0)+b, alphay = (a*y0)+b; 
    //G4cout << "2:  " << alphax << " " << alphay << G4endl;
    G4ThreeVector direction = (G4ThreeVector(std::tan(alphax*deg), std::tan(alphay*deg), -1));
    //G4cout << x0 << " " << alpx << ", " << y0 << " " << alpy << G4endl;
    //G4double alpx = G4UniformRand()*2*alphax-alphax, alpy = G4UniformRand()*2*alphay-alphay
    fParticleGun->SetParticlePosition(G4ThreeVector(x0*mm,y0*mm,z0*mm)); 
    fParticleGun->SetParticleMomentumDirection(direction);    
    fParticleGun->GeneratePrimaryVertex(anEvent);
  }
}




