#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os, sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress
import matplotlib.cm as cm

def fitten(funk, x, y):
	params, covar = curve_fit(funk, x, y)
	uparams = unumpy.uarray(params, np.sqrt(np.diag(covar)))
	return params, covar, uparams

def efunkt(x, a, b, c):
	return a*np.exp(-b*x) + c 

# Messdaten
x1, y1, z1, val1 = np.loadtxt('../9_tdk_nach_taddei_solid_water/taddei_bild_fit_24_c_gerade_solid-build/cyl_tdk_nach_taddei_solidwater_200000000_1.out', delimiter=',', unpack=True)

z = []
val = []
parameter = 3
leng = int(len(val1)/3)
for i in range(leng, 2*leng):
	z = z + [2*x1[i]+2]
	val = [val1[i]/100] + val 
	#val = [val1[i]/100000] + val

print(z)
print(val)
# Daten laden
x_taddei_simu, y_taddei_simu = np.loadtxt('taddei_simu_gefiltert_von_hand.txt', unpack=True)
x_taddei_mess, y_taddei_mess = np.loadtxt('taddei_mess_gefiltert_von_hand.txt', unpack=True)
legende_tdk = ['Simulation mit Geant4', 'Simulation von Taddei et al.', 'Messwerte von Taddei et al.']

y_taddei_simu = y_taddei_simu/100
y_taddei_mess = y_taddei_mess/100

# Plotgroesse
fig_size = plt.rcParams["figure.figsize"]
#print(fig_size)
#fig_size[0] = 8*0.8
#fig_size[1] = 6*0.85
#plt.rcParams["figure.figsize"] = fig_size

# Fit
par_simu, covar_simu, upar_simu = fitten(efunkt, x_taddei_simu, y_taddei_simu)
par_mess, covar_mess, upar_mess = fitten(efunkt, x_taddei_mess, y_taddei_mess)
par_simu_geant, covar_simu_geant, upar_simu_geant = fitten(efunkt, z, val)

# PLOTS
col = ['b', 'g', 'r']
strich = ['-', '--', 'o']
fig, (ax1, ax2) = plt.subplots(2, sharex=True, gridspec_kw = {'height_ratios':[2, 1]})



x = np.linspace(0,30)
norm_wert = 0
norm_funk = (efunkt(z[0], *par_simu)/efunkt(norm_wert, *par_simu)*100)
#print(norm_funk)

ax1.plot(z, val/val[0]*norm_funk, 'rx', markersize=8)
ax1.plot(x, efunkt(x, *par_simu_geant)/efunkt(norm_wert, *par_simu_geant)*100, str(col[0])+str(strich[0]), linewidth=3, label=str(legende_tdk[0]))
ax1.plot(x, efunkt(x, *par_simu)/efunkt(norm_wert, *par_simu)*100, str(col[1])+str(strich[1]), linewidth=3, label=str(legende_tdk[1]))
ax1.plot(x_taddei_mess, efunkt(x_taddei_mess, *par_mess)/efunkt(norm_wert, *par_mess)*100, str(col[2])+str(strich[2]), markersize=8, label=str(legende_tdk[2]))

ax1.tick_params(axis='both', right='on', labelleft='off', labelright='on')
ax1.yaxis.set_label_position("right")
ax1.set_ylabel('prozentuale Dosis   /   %')

legend = ax1.legend(markerscale = 1, fancybox=True)
legend.get_frame().set_edgecolor('white')

######################################################################

legende_feh = ['Taddei et al.', 'Geant4']

ax2.plot(x_taddei_mess, ((efunkt(x_taddei_mess, *par_mess)/efunkt(norm_wert, *par_mess))-(efunkt(x_taddei_mess, *par_simu))/efunkt(norm_wert, *par_simu))/(efunkt(x_taddei_mess, *par_mess)/efunkt(norm_wert, *par_mess))*100, str(col[1])+'x', markersize=8, markeredgewidth=2, label=str(legende_feh[0]))
ax2.plot(x_taddei_mess, ((efunkt(x_taddei_mess, *par_mess)/efunkt(norm_wert, *par_mess))-(efunkt(x_taddei_mess, *par_simu_geant))/efunkt(norm_wert, *par_simu_geant))/(efunkt(x_taddei_mess, *par_mess)/efunkt(norm_wert, *par_mess))*100, str(col[0])+'x'	, markersize=8, markeredgewidth=2, label=str(legende_feh[1]))

#legend = ax2.legend(loc=2, markerscale = 1.2, fancybox=True)
#legend.get_frame().set_edgecolor('gray')

#ax = plt.axes()
#box = dict(boxstyle="round", fc="w", alpha=0.5)
#parastr = '    f(x) = A $-$ B$\cdot$exp($-$C$\cdot$x) \n\nA = '+str(uparams[0]/value_cyl[i][1])+'\nB = '+str(uparams[1]/value_cyl[i][1])+'\nC = '+str(uparams[2])+' mm$ ^{-1}$'
#ax.text(0.5, 0.04, parastr, horizontalalignment='left', verticalalignment='bottom', transform=ax.transAxes, fontsize=12, bbox=box) 

#plt.rcParams.update({'font.size': 12})
plt.ylabel('proz. Abw.  /  %')
plt.xlabel('Tiefe des Wasserphantoms  /  mm')

fig.subplots_adjust(hspace=0)
plt.setp([a.get_xticklabels() for a in fig.axes[:-1]], visible=False)
plt.savefig("plot_tdk_geschatzt_mit_lit.pdf")





