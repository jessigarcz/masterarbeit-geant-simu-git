import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
import scipy.interpolate as sp
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress
from PIL import Image
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.signal import medfilt
from plot_farben import *

### Graphen ###
x_dirk_mit, y_dirk_mit = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_dirk_mit.txt', unpack=True)
x_oraya_mit, y_oraya_mit = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_oraya_mit.txt', unpack=True)
x_taddei_mac_ori, y_taddei_mac_ori = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mac_ori.txt', unpack=True)
x_taddei_mac_se_ori, y_taddei_mac_se_ori = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mac_ori.txt', unpack=True)
x_taddei_mac, y_taddei_mac = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mac.txt', unpack=True)
x_taddei_mit_bild_ori, y_taddei_mit_bild_ori = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mit_bild_ori.txt', unpack=True)
x_taddei_mit_bild, y_taddei_mit_bild = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mit_bild.txt', unpack=True)
x_taddei_mit_data_ori, y_taddei_mit_data_ori = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mit_data_ori.txt', unpack=True)
x_taddei_mit_data_se_ori, y_taddei_mit_data_se_ori = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mit_data_ori.txt', unpack=True)
x_taddei_mit_data, y_taddei_mit_data = np.loadtxt('../2_spektren_gefittet/spektra_fitten/spektren_taddei_mit_data.txt', unpack=True)


### 1: ohne, 2: mit, 3: mac, 4: nur taddei bild alle drei, 5: taddei bild und data 
num = 3

#if(num == 1):
# Plots ohne
	

if(num == 2):
	# Plots mit
	plot_name = 'mit'
	label_deu = ['Taddei Bilddaten', 'Taddei (ori. Daten)', 'Oraya', 'Oraya Patent']

	plt.plot(x_taddei_mit_bild, y_taddei_mit_bild, str(strich[0])+str(marker[0]), markevery=30, color=str(farbe[0]), linewidth=2, label=label_deu[0])
	plt.plot(x_taddei_mit_data, y_taddei_mit_data, str(strich[0])+str(marker[1]), markevery=31, color=str(farbe[1]), linewidth=2, label=label_deu[0])
	plt.plot(x_oraya_mit, y_oraya_mit, str(strich[0])+str(marker[2]), color=str(farbe[2]), markevery=32, linewidth=2, label=label_deu[0])
	plt.plot(x_dirk_mit, y_dirk_mit, str(strich[0])+str(marker[3]), color=str(farbe[3]), markevery=33, linewidth=2, label=label_deu[0])
	
if(num == 3):
	# Plots mac
	plot_name = 'mac'
	label_deu = ['Taddei Bilddaten', 'Taddei (ori. Daten)']

	plt.plot(x_taddei_mac, y_taddei_mac, str(strich[0])+str(marker[0]), markevery=30, color=str(farbe[0]), linewidth=2, label=label_deu[0])
	plt.plot(x_taddei_mac_se_ori, y_taddei_mac_se_ori, str(strich[0])+str(marker[1]), markevery=31, color=str(farbe[1]), linewidth=2, label=label_deu[0])



#plt.fill_between(x_taddei_ohne, y_taddei_ohne*1.5, color=str(farbe[0]), alpha = 0.1) 

plt.xlabel('Energie  /  keV')
plt.ylabel('Intensität  /  Photonen/cm$^{2}$')

plt.rcParams.update({'font.size': 12})
legend = plt.legend(markerscale = 1.5, fancybox=True)
legend.get_frame().set_edgecolor('gray')
plt.savefig('spektren_'+str(plot_name)+'.pdf')


#plt.figure(2)





