import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage
import scipy.interpolate as sp
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress
from PIL import Image
from scipy.interpolate import interp1d
from scipy.signal import savgol_filter
from scipy.signal import medfilt

def min_array(array):
	wert_min = 0
	for i in range(0,len(array)):
		if(array[i] < wert_min):
			wert_min = array[i] 
	return wert_min

def max_array(array):
	wert_max = 0
	for i in range(0,len(array)):
		#print(array[i])
		if(array[i] > wert_max):
			wert_max = array[i] 
	return wert_max

def max_array_zwischen(array_x, array_y, mini, maxi):
	wert_max = 0
	for i in range(0, len(array_y)):
		if((array_x[i] <= maxi) and (array_x[i] >= mini)):
			#print(array_y[i])
			if(array_y[i] > wert_max):
				wert_max = array_y[i] 
	#print(wert_max)
	return wert_max

def norm_array(array, wert):
	for i in range(0,len(array)):	
		if(array[i] != 0):
			array[i] = array[i]/wert
		#print(array[i])
	return array

def rahmen(array, numrows, numcols, xmin, xmax, ymin, ymax):
	# Rahmen ermitteln
	white = [255, 255, 255]
	black = [0, 0, 0]
	blackpixel_x = []
	blackpixel_y = []

	# Schwarze Pixel finden
	for x in range(0, numrows):
		#print(x/numrows*100)
		for y in range(0, numcols):
			y = numcols-y-1
			#print(array[x][y])
			if((array[x][y][0] == black[0]) and (array[x][y][1] == black[1]) and (array[x][y][2] == black[2])):
				#print('yes')
				#blackpixel = np.insert(blackpixel, len(blackpixel), y)
				#blackpixel = np.append(blackpixel, [y])
				#blackpixel = np.concatenate((blackpixel, np.array([x,y])), axis = 0) #newpixel)
				blackpixel_x = blackpixel_x + [x]
				blackpixel_y = blackpixel_y + [y]				

	#print(blackpixel_x)

	# Häufige Schwarzwerte rausfiltern
	# X-Werte Liste
	maxvalue_x = 0
	maxanzahl_x = 0
	maxvalue_y = 0
	maxanzahl_y = 0

	for i in range(0, int(len(blackpixel_x))):
		pixel_x = blackpixel_x[i]
		count_x = blackpixel_x.count(pixel_x)
		#(blackpixel_x[i])
		if(count_x > maxanzahl_x):
			maxanzahl_x = count_x
			maxvalue_x = pixel_x
			maxpos_x = []
			#print(maxpos_x)
		if(count_x == maxanzahl_x):
			maxanzahl_x = count_x
			maxvalue_x = pixel_x
			maxpos_x = maxpos_x + [blackpixel_y[i]]
			#print(maxpos_x)

	for i in range(0, int(len(blackpixel_y))):
		pixel_y = blackpixel_y[i]
		count_y = blackpixel_y.count(pixel_y)
		if(count_y > maxanzahl_y):
			maxanzahl_y = count_y
			maxvalue_y = pixel_y
			maxpos_y = []
			#print(maxpos_y)
		if(count_y == maxanzahl_y):
			maxanzahl_y = count_y
			maxvalue_y = pixel_y
			maxpos_y = maxpos_y + [blackpixel_x[i]]
			#print(maxpos_y)

	print(maxanzahl_x)
	print(maxanzahl_y)

	pixel_u_l = [maxvalue_y, min(maxpos_y)]
	pixel_o_l = [maxvalue_y, maxvalue_x]
	pixel_u_r = [max(maxpos_x), min(maxpos_y)]
	pixel_o_r = [max(maxpos_x), maxvalue_x]
	plot_x_len = pixel_o_r[0] - pixel_o_l[0]
	plot_y_len = pixel_u_l[1] - pixel_o_l[1]

	print(pixel_u_l)
	print(pixel_o_l)
	print(pixel_u_r)


	faktor_x = (xmax-xmin)/plot_x_len
	faktor_y = (ymax-ymin)/plot_y_len
	return faktor_x, faktor_y, pixel_o_l, pixel_u_l

def coloured_pixel(array, numcols, numrows, faktor_x, faktor_y, col, pixel_o_l, pixel_u_l):
	# Coloured Pixel finden
	colpixel_x = []
	colpixel_y = []
	for x in range(0, numcols):
		for y in range(0, numrows):
			#y = numrows-1-y 
			#print(y)
			if((array[y][x][0] == col[0]) and (array[y][x][1] == col[1]) and (array[y][x][2] == col[2])):
				colpixel_y = colpixel_y + [((y - pixel_o_l[1]) * faktor_y)]
				#print(((y - pixel_o_l[1]) * faktor_y))
				colpixel_x = colpixel_x + [((x - pixel_u_l[0]) * faktor_x)]
	
	# Reduzieren in y-Ebene plus 0 und 100 hinzufügen und testen
	colpixelred_x = [0]
	colpixelred_y = [0]
	for i in range(0, len(colpixel_y)):
		#print('Anzahl')
		anzahl = colpixel_x.count(colpixel_x[i])
		#print(anzahl)
		if(anzahl > 1 and colpixel_x[i] > 0): ##colpixel_x[i] <= 0 and colpixel_x[i] >= 100):
			ind_min = i 
			ind_max = i+anzahl
			ind = round((ind_max-ind_min)/2)+ind_min
			#print(colpixel_x[ind])
			#print(colpixel_y[ind])
			#print()
			colpixelred_x = colpixelred_x + [colpixel_x[ind]]
			colpixelred_y = colpixelred_y + [colpixel_y[ind]-colpixel_y[0]]
			for j in range(i,ind_max):
				colpixel_x[j] = 0
				colpixel_y[j] = 0
		if(anzahl == 1):  ## and colpixel_x[i] < 0 and colpixel_x[i] >= 100):
			colpixelred_x = colpixelred_x + [colpixel_x[i]]
			colpixelred_y = colpixelred_y + [colpixel_y[i]-colpixel_y[0]]
			#print(colpixel_x[i])
			#print(colpixel_y[i])
	#print(colpixelred_x)
	#print(colpixelred_y)
	#print('Colourpixel fertig.')

	# Reduzieren in x-Ebene
	colpixelredfinal_x = [0]
	colpixelredfinal_y = [0]	
	for i in range(1, len(colpixelred_y)):
		anzahl = colpixelred_y.count(colpixelred_y[i])
		if(anzahl > 1 and colpixelred_y[i] > 0): 
			ind_min = i 
			ind_max = i+anzahl
			ind = round((ind_max-ind_min)/2)+ind_min
			colpixelredfinal_x = colpixelredfinal_x + [colpixelred_x[ind]]
			colpixelredfinal_y = colpixelredfinal_y + [colpixelred_y[ind]]
			for j in range(ind_min,ind_max):
				colpixelred_x[j] = 0
				colpixelred_y[j] = 0
		if(anzahl == 1): 
			colpixelredfinal_x = colpixelredfinal_x + [colpixelred_x[i]]
			colpixelredfinal_y = colpixelredfinal_y + [colpixelred_y[i]]	


	# Start bei 0
	# Gibt es eine negative Zahl?
	min_wert = min_array(colpixel_y)
	if(min_wert == abs(min_wert)): # Positve Zahl
		for i in range(0, len(colpixel_y)):
			colpixel_y[i] = colpixel_y[i] - min_wert
	else:
		for i in range(0, len(colpixel_y)):
			colpixel_y[i] = colpixel_y[i] + min_wert

	colpixelredfinal_x = colpixelredfinal_x + [100]
	colpixelredfinal_y = colpixelredfinal_y + [0]	
	return colpixelredfinal_x, colpixelredfinal_y

def graph_aus_bild(graph, col, min_x, max_x, min_y, max_y):
	image = Image.open(graph).convert("RGB")
	array = np.array(np.asarray(image))
	#img = Image.fromarray(array)
	#img.save("test_output.png")
	numrows = len(array) # Breite
	# print(numrows)
	numcols = len(array[0]) # Höhe
	# print(numcols)
	faktor_x, faktor_y, pixel_o_l, pixel_u_l = rahmen(array, numrows, numcols, min_x, max_x, min_y, max_y)
	pixelred_x_fil, pixelred_y_fil = coloured_pixel(array, numcols, numrows, faktor_x, faktor_y, col, pixel_o_l, pixel_u_l)
	return pixelred_x_fil, pixelred_y_fil, 

def werte_ausgeben(name, xwerte, ywerte):
	fobj_out = open(name, "w")
	for i in range(0,len(xwerte)):
		fobj_out.write(str(xwerte[i]) + "	" + str(ywerte[i]) + "\n")
	fobj_out.close()
	#print('Werte ausgegeben.')



### Farben der Graphen ### 
blue = [0, 0, 255]
red = [255, 0, 0]
green = [0, 255, 0]

### Graphen ohne Filterung ###
x_taddei_ohne, y_taddei_ohne = graph_aus_bild("../daten/graph_taddei.png", blue, 0, 100, 0, 1)

### Graphen mit Filterung ###
x_taddei_mit, y_taddei_mit = graph_aus_bild("../daten/graph_taddei.png", red, 0, 100, 0, 1)

### Graphen Macula ###
x_taddei_mac, y_taddei_mac = graph_aus_bild("../daten/graph_taddei.png", green, 0, 100, 0, 1)


### Originalwerte ausgeben ###
werte_ausgeben("taddei_ohne.txt", x_taddei_ohne, y_taddei_ohne)
werte_ausgeben("taddei_mit.txt", x_taddei_mit, y_taddei_mit)
werte_ausgeben("taddei_mac.txt", x_taddei_mac, y_taddei_mac)


###### Werte neu normieren #########
y_taddei_ohne = norm_array(y_taddei_ohne, max_array_zwischen(x_taddei_ohne, y_taddei_ohne, 15, 30))
y_taddei_mit = norm_array(y_taddei_mit, max_array_zwischen(x_taddei_mit, y_taddei_mit, 20, 45))
y_taddei_mac = norm_array(y_taddei_mac, max_array_zwischen(x_taddei_mit, y_taddei_mit, 25, 50))

### Normierte Werte ausgeben ###
werte_ausgeben("taddei_brems_ohne.txt", x_taddei_ohne, y_taddei_ohne)
werte_ausgeben("taddei_brems_mit.txt", x_taddei_mit, y_taddei_mit)
werte_ausgeben("taddei_brems_mac.txt", x_taddei_mac, y_taddei_mac)



