#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os, sys
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from scipy.optimize import curve_fit
import scipy.constants.constants as const
from uncertainties import ufloat
from uncertainties import unumpy
from scipy.stats import linregress


farbe = ['royalblue', 'mediumpurple', 'seagreen', 'orchid', 'firebrick', 'darkorange' ]
strich = ['-', '--', '-.', ':']
marker= ['o', 'v', 's', '^', 'p', '<', 'd', '>']