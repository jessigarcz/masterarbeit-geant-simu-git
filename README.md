# Masterarbeit (Abgabe Januar 2018)

In diesem Projekt befinden sich einige Simulations- und Auswertungsdateien meiner Masterarbeit.
Simuliert wird mit Geant4. 
Ausgewertet wird mit python.


## Thema der Masterarbeit:

- `Masterarbeit`: die Simulationen beziehen sich auf das IRay System, welches Patienten mit AMD bestrahlt. 
Ziel der Masterarbeit war es ein Festkörperphantom für die Qualitätssicherung zu entwickeln. 
Die Eigenschaften des Phantoms, wie Größe, Form, Material, wurden mittels Simulationen bestimmt. 
- `Info`: das Git-Repository ist nicht vollständig. Es diente lediglich zur weiteren Datensicherung und um Erfahrungen mit git zu machen. 